#include "CxAODReader_VHcc/MVATree_VHcc.h"
#include "EventLoop/Worker.h"

MVATree_VHcc::MVATree_VHcc(bool persistent, bool readMVA, std::string analysisType, EL::Worker* wk, std::vector<std::string> variations, bool nominalOnly) :
        MVATree(persistent, readMVA, wk, variations, nominalOnly),
        m_analysisType(analysisType)
{
  SetBranches();
}


void MVATree_VHcc::AddBranch(TString name, Int_t* address)
{
  for (std::pair<std::string, TTree*> tree : m_treeMap) {
    tree.second -> Branch(name, address);
  }
  m_reader.AddVariable(name, address);
}

void MVATree_VHcc::AddBranch(TString name, Float_t* address)
{
  for (std::pair<std::string, TTree*> tree : m_treeMap) {
    tree.second -> Branch(name, address);
  }
  m_reader.AddVariable(name, address);
}

void MVATree_VHcc::AddBranch(TString name, ULong64_t* address)
{
  for (std::pair<std::string, TTree*> tree : m_treeMap) {
    tree.second -> Branch(name, address);
  }
  m_reader.AddVariable(name, address);
}

// void MVATree_VHcc::AddBranch(TString name, std::string* address)
// {
//   for (std::pair<std::string, TTree*> tree : m_treeMap) {
//     tree.second -> Branch(name, address);
//   }
// }

void MVATree_VHcc::SetBranches()
{
  // prepare MVA reader
  m_reader.SetSplitVar(&EventNumber);
  m_reader.AddReader("reader", 2);
  if(m_analysisType == "0lep"){
     m_reader.AddReader("0lep_2jet", 2);
     m_reader.AddReader("0lep_3jet", 2);
  }
  if(m_analysisType == "1lep"){
    m_reader.AddReader("1lep_2jet", 2);
    m_reader.AddReader("1lep_3jet", 2);
    m_reader.AddReader("1lep_2jet_WZ", 2);
    m_reader.AddReader("1lep_3jet_WZ", 2);
  }

  AddBranch("sample", &sample);

  AddBranch("EventWeight", &EventWeight);
  AddBranch("EventNumber", &EventNumber);
  AddBranch("ChannelNumber", &ChannelNumber);

  // variables to use Paul's MVA code
  AddBranch("isOdd", &isOdd);
  AddBranch("weight", &weight);

  AddBranch("nJ", &nJ);
  AddBranch("nTags", &nTags);
  AddBranch("nSigJet", &nSigJet);
  AddBranch("nForwardJet", &nForwardJet);
  AddBranch("mBB", &mBB);
  AddBranch("dRBB", &dRBB);
  AddBranch("dPhiBB", &dPhiBB);
  AddBranch("dEtaBB", &dEtaBB);
  AddBranch("sumPt", &sumPt);
  AddBranch("pTB1", &pTB1);
  AddBranch("pTB2", &pTB2);
  AddBranch("pTBB", &pTBB);
  AddBranch("pTBBoverMET", &pTBBoverMET);
  AddBranch("etaB1", &etaB1);
  AddBranch("etaB2", &etaB2);
  AddBranch("MET", &MET);
  AddBranch("METSig", &METSig);
  AddBranch("METSig_soft", &METSig_soft);
  AddBranch("METSig_hard", &METSig_hard);
  AddBranch("METOverSqrtSumET", &METOverSqrtSumET);
  AddBranch("METOverSqrtHT", &METOverSqrtHT);
  AddBranch("METDirectional", &METDirectional);
  AddBranch("METRho", &METRho);
  AddBranch("METVarL", &METVarL);
  AddBranch("METVarL_soft", &METVarL_soft);
  AddBranch("METVarL_hard", &METVarL_hard);
  AddBranch("METVarT", &METVarT);
  AddBranch("MPT", &MPT);
  AddBranch("HT", &HT);
  AddBranch("METHT", &METHT);
  AddBranch("MV1cB1", &MV1cB1);
  AddBranch("MV1cB2", &MV1cB2);
  AddBranch("MV1cJ3", &MV1cJ3);
  AddBranch("pTJ3", &pTJ3);
  AddBranch("etaJ3", &etaJ3);
  AddBranch("dRB1J3", &dRB1J3);
  AddBranch("dRB2J3", &dRB2J3);
  AddBranch("mBBJ", &mBBJ);
  
  AddBranch("dPhiVBB", &dPhiVBB);
  AddBranch("dPhiMETMPT", &dPhiMETMPT);
  AddBranch("dPhiMETdijet", &dPhiMETdijet);
  AddBranch("mindPhi", &mindPhi);
  
  AddBranch("BDT", &BDT);

  if (m_analysisType == "0lep") {
    AddBranch("MEff", &MEff);
    AddBranch("MEff3", &MEff3);
  }
  else if (m_analysisType == "1lep") {
    AddBranch("dPhiLBmin", &dPhiLBmin);
    AddBranch("Mtop", &Mtop);
    AddBranch("dYWH", &dYWH);
    AddBranch("dEtaWH", &dEtaWH);
    AddBranch("dPhiLMET", &dPhiLMET);
    AddBranch("pTL", &pTL);
    AddBranch("etaL", &etaL);
    AddBranch("mTW", &mTW);
    AddBranch("pTV", &pTV);
  }
  else if (m_analysisType == "2lep") {
    AddBranch("dEtaVBB", &dEtaVBB);
    AddBranch("mLL", &mLL);
    AddBranch("pTV", &pTV);
  }
  
  // book MVA reader
  TString xmlFile = getenv("WorkDir_DIR");
  if (m_analysisType == "2lep" && m_readMVA) {
    // Run 1 files can be found in /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run1Paper/MVATrainingFiles/
    xmlFile += "/data/CxAODReader_VHbb/TMVAClassification_BDT_LiverpoolBmham_8TeV_llbb_2tag2jet_ptv0_120_ZllH125_0of2_root5.34.05_v3.0.xml";
    m_reader.BookReader("reader", xmlFile);
  }else if(m_analysisType == "0lep" && m_readMVA){
     //TString xmlFile_2jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_BDT_AcSinica_8TeV_vvbb_2tag2jet_ptv120_ZvvHandWlvHandZllH125_qq_and_gg_0of2_root5.34.11_v7.xml";
     TString xmlFile_2jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_BDT_0lep2tag2jet.weights.xml";
     m_reader.BookReader("0lep_2jet", xmlFile_2jet);

     //TString xmlFile_3jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_BDT_AcSinica_8TeV_vvbb_2tag3jet_ptv120_ZvvHandWlvHandZllH125_qq_and_gg_0of2_root5.34.11_v7.xml";
     TString xmlFile_3jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_BDT_0lep2tag3jet.weights.xml";
     m_reader.BookReader("0lep_3jet", xmlFile_3jet);
  }else if(m_analysisType == "1lep" && m_readMVA){

    TString xmlFile_2jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep2jet_0of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
//    TString xmlFile_2jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep2jet_0of2_0905_BDT_AdaBoost.weights.xml";
    m_reader.BookReader("1lep_2jet", xmlFile_2jet);
    TString xmlFile_3jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep3jet_0of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
//    TString xmlFile_3jet = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep3jet_0of2_0905_BDT_AdaBoost.weights.xml";
    m_reader.BookReader("1lep_3jet", xmlFile_3jet);

    TString xmlFile_2jet_WZ = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep2jet_0of2_WZ_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
    m_reader.BookReader("1lep_2jet_WZ", xmlFile_2jet_WZ);
    TString xmlFile_3jet_WZ = xmlFile+"/data/CxAODReader_VHbb/TMVAClassification_1lep3jet_0of2_WZ_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
    m_reader.BookReader("1lep_3jet_WZ", xmlFile_3jet_WZ);

  }
}

float MVATree_VHcc::getBinnedMV1c(float MV1c) {
  const int nbins = 5;
  float xbins[nbins+1] = {0, 0.4050, 0.7028, 0.8353, 0.9237, 1.0};
  float xbins_mv2c20[nbins+1] = {-1, -0.5911, -0.0436, 0.4496, 0.7535, 1.0};

  if (m_analysisType == "0lep"){
     if (MV1c >= 1) MV1c = 1-1e-5;
     if (MV1c <= -1) MV1c = -1+1e-5;// lowset edge for MV2c20

     for(int i=0; i<nbins; i++) {
	if ( MV1c >= xbins_mv2c20[i] && MV1c < xbins_mv2c20[i+1] ) {
	   return (xbins[i] + xbins[i+1])/2;
	}
     }

     return 0;
  }

  if (MV1c >= 1) MV1c = 1-1e-5;
  if (MV1c <= 0) MV1c = 1e-5;

  for(int i=0; i<nbins; i++) {
    if ( MV1c >= xbins[i] && MV1c < xbins[i+1] ) {
      return (xbins[i] + xbins[i+1])/2;
    }
  }

  return 0;
}


void MVATree_VHcc::TransformVars() {
  MV1cB1 = getBinnedMV1c(MV1cB1);
  MV1cB2 = getBinnedMV1c(MV1cB2);
  MV1cJ3 = getBinnedMV1c(MV1cJ3);
  mBB = mBB / 1e3;
  MET = MET / 1e3;
  pTB1 = pTB1  / 1e3;
  pTB2 = pTB2  / 1e3;
  pTBB = pTBB  / 1e3;
  MEff = MEff / 1e3;
  MPT = MPT / 1e3;
  MEff3 = MEff3 / 1e3;
  pTJ3 = pTJ3 / 1e3;
  mBBJ = mBBJ / 1e3;
}

void MVATree_VHcc::Fill() {
  StaticSetBranches(m_analysisType, this);
  MVATree::Fill();
}

void MVATree_VHcc::Reset()
{

  sample = "Unknown";

  EventWeight = 0;
  EventNumber = -1;
  ChannelNumber = -1;

  weight = 0;
  isOdd = -1;

  nJ     = -1;
  nSigJet    = -1;
  nForwardJet= -1;
  nTags  = -1;
  nTaus  = -1;
  dRBB   = -1;
  dPhiBB = -1;
  dEtaBB = -1;
  dPhiVBB = -1;
  dPhiMETMPT = -1;
  dPhiMETdijet = -1;
  mindPhi = -1;
  dEtaVBB = -1;
  dPhiLBmin = -1;
  pTV    = -1;
  mBB    = -1;
  MEff   = -1;
  MEff3  = -1;
  HT     = -1;
  sumPt   = -1;
  pTB1   = -1;
  pTB2   = -1;
  pTBB   = -1;
  yBB    = -1;
  pTBBoverMET   = -1;
  pTBBMETAsym   = -1;
  etaB1   = -1;
  etaB2   = -1;
  mTW    = -1;
  MPT    = -1;
  METHT  = -1;
  MET    = -1;
  METSig = -1;
  METSig_soft = -1;
  METSig_hard = -1;
  METOverSqrtSumET = -1;
  METOverSqrtHT = -1;
  METDirectional = -1;
  METRho = -1;
  METVarL = -1;
  METVarL_soft = -1;
  METVarL_hard = -1;
  METVarT = -1;
  mLL    = -1;
  MV1cB1 = -1;
  MV1cB2 = -1;
  MV1cJ3 = -1;
  pTJ3   = -1;
  etaJ3   = -1;
  dRB1J3   = -1;
  dRB2J3   = -1;
  mindRBJ3 = -1;
  mBBJ   = -1;
  pTL    = -1;
  etaL   = -1;
  Tau21  = -1;
  Tau32  = -1;
  mVH    = -1;
  mVH1   = -1;
  mVH2   = -1;
  Mtop   = -1;
  dYWH   = -1;
  dEtaWH   = -1;
  pTVHnorm = -1;
  dPhiLMET = -1;

  phiB1 = -99.;
  phiB2 = -99.;
  phiJ3 = -99.;
  mB1 = -99.;
  mB2 = -99.;
  mJ3 = -99.;
  
  costheta = -1;

  BDT    = -1;
  BDT_WZ = -1;
  BDTCharles = -1;
  
  for (auto &bdt:BDTs)
  {
    bdt.second = -99.;
  }
  
  StaticReset();
}

void MVATree_VHcc::ReadMVA() {

  if (!m_readMVA) return;
  if (m_analysisType == "0lep"){
     //TransformVars();
     //if (nJ==2){
     //   BDT = m_reader.EvaluateMVA("0lep_2jet");
     //}else{
     //   BDT = m_reader.EvaluateMVA("0lep_3jet");
     //}
  }else if (m_analysisType == "1lep"){
    if (nJ==2){
      BDT = m_reader.EvaluateMVA("1lep_2jet");
      BDT_WZ = m_reader.EvaluateMVA("1lep_3jet_WZ");
    }else{
      BDT = m_reader.EvaluateMVA("1lep_3jet");
      BDT_WZ = m_reader.EvaluateMVA("1lep_3jet_WZ");
    }
  }else{
     BDT = m_reader.EvaluateMVA("reader");
  }

}
