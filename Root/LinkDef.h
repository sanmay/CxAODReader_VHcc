#include <CxAODReader_VHcc/AnalysisReader_VHcc.h>
//#include <CxAODReader_VHcc/AnalysisReader_VHbb0Lep.h>
#include <CxAODReader_VHcc/AnalysisReader_VHcc1Lep.h>
//#include <CxAODReader_VHcc/AnalysisReader_VHbb2Lep.h>
//#include <CxAODReader_VHcc/AnalysisReader_VBFHbb1Ph.h>
//#include <CxAODReader_VHcc/AnalysisReader_VGamma.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class AnalysisReader_VHcc+;
//#pragma link C++ class AnalysisReader_VHbb0Lep+;
#pragma link C++ class AnalysisReader_VHcc1Lep+;
//#pragma link C++ class AnalysisReader_VHbb2Lep+;
//#pragma link C++ class AnalysisReader_VBFHbb1Ph+;
//#pragma link C++ class AnalysisReader_VGamma+;
#endif
