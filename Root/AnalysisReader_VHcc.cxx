#include <type_traits>
#include "NNLOReweighter/NNLOReweighter.h"

#include <EventLoop/Worker.h>

#include "TSystem.h"

#include "CxAODReader/EasyTree.h"
#include "CxAODReader/OSTree.h"
#include "KinematicFit/KinematicFit.h"
//#include "CxAODReader_VHcc/MVATree_VHcc.h"
//#include "CxAODReader_VHcc/MVATree_VBFHbb.h"
//#include "CxAODReader_VHcc/MVATree_BoostedVHcc.h"
//#include "CxAODTools_VHbb/VHbb0lepEvtSelection.h"
#include "CxAODTools/ITMVAApplicationTool.h"
#include "CxAODReader/AnalysisReader.h"

#include "CxAODReader_VHcc/AnalysisReader_VHcc.h"
//#include "CxAODReader_VHcc/AnalysisReader_VHcc0Lep.h"
#include "CxAODReader_VHcc/AnalysisReader_VHcc1Lep.h"
//#include "CxAODReader_VHcc/AnalysisReader_VHcc2Lep.h"
//#include "CxAODReader_VHcc/AnalysisReader_VBFHbb1Ph.h"
//#include "CxAODReader_VHcc/AnalysisReader_VGamma.h"

//#include "CxAODTools_VHbb/VHbb2lepEvtSelection.h"
#include "NNLOReweighter/NNLOReweighter.h"

//#include "CxAODTools/JMSCorrection_lvqq.h"//combMass
#include "JetUncertainties/JetUncertaintiesTool.h"//combMass

#define length(array) (sizeof(array) / sizeof(*(array)))

ClassImp(AnalysisReader_VHcc)

AnalysisReader_VHcc::AnalysisReader_VHcc () :
  AnalysisReader(),
  m_analysisType(""),
  m_analysisStrategy("SimpleMerge500"),
  m_doTruthTagging(false),
  m_doOnlyInputs(false),
  m_doReduceFillHistos(false),
  m_jetCorrType("OneMu"),
  m_fatJetCorrType("XbbMu"),
  m_tagStrategy("AllSignalJets"),
  m_analysisContext(nullptr),
  m_mva(nullptr),
  m_mvaToolName("TMVAApplicationTool"),
  // boosted 0 lep, I'll try to reduce (Camilla)
  m_use2DbTagCut(false),
  m_doFillHistograms(false),
  m_doMergeCR(false),
  m_doCutflow(false),
  m_doResolution(false),
  m_doBinnedSyst(false),
  m_doSherpaSyst(false),
  m_histFastSvc(),
  m_fatJetHistos(),
  m_model(Model::undefined)
{
  m_csCorrections.clear();
  m_csVariations.clear();
}

AnalysisReader_VHcc::~AnalysisReader_VHcc ()
{
}

EL::StatusCode AnalysisReader_VHcc::initializeNNLORW (int DSID)
{

//  std::cout << "DSID " << DSID << std::endl;

  m_myNNLO = new NNLOReweighter( DSID );  // e.g. 410000, 410001...
  m_myNNLO->Init();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VHcc::initializeSelection ()
{

  // reset physics metadata
  m_physicsMeta = PhysicsMetadata();
  std::string model = m_config->get<std::string>("modelType");
  if     ( model == "AZh" ) m_model = Model::AZh;
  else if ( model == "HVT" ) m_model = Model::HVT;
  else if ( model == "SM" )  m_model = Model::SM;
  else if ( model == "MVA" ) m_model = Model::MVA;
  else m_model = Model::undefined;

  m_config->getif<string>("analysisType", m_analysisType);
  m_config->getif<string>("analysisStrategy", m_analysisStrategy);

  // Select analysis strategy according to model
  if ( m_model == Model::SM || m_model == Model::MVA  ) m_analysisStrategy = "Resolved";

  // For VH-reso analyses default is to merge ptv bins
  if ( m_model == Model::AZh || m_model == Model::HVT ) m_doMergePtVBins = true;

  // For 2D cut only use merged
  m_config->getif<bool>("use2DbTagCut", m_use2DbTagCut);
  if (m_use2DbTagCut){
    if(m_analysisStrategy != "Merged"){
      Error("initializeSelection()","Running with 2DbTagCut. Only possible for merged analysis, but %s is chosen. Exiting!",m_analysisStrategy.c_str());
      return EL::StatusCode::FAILURE;
    }
    Warning("initializeSelection()","Running with 2DbTagCut. b-tagging for calo jets disabled!!");
  }

  Info("initializeSelection()", "Initialize analysis '%s %s'.", m_analysisType.c_str(), m_analysisStrategy.c_str());


  //Trees
  //------
  // TODO move to base class?
  bool writeMVATree = false;
  bool readMVA      = false;
  std::string MVAxmlFileName = "";
  m_config->getif<bool>("writeMVATree", writeMVATree);
  m_config->getif<bool>("readMVA", readMVA);
  m_config->getif<std::string>("MVAxmlFileName", MVAxmlFileName);
  bool writeEasyTree = false;
  m_config->getif<bool>("writeEasyTree", writeEasyTree);

  if (writeEasyTree && writeMVATree) writeMVATree = false;

//  if ( m_analysisType == "0lep" && m_analysisStrategy != "Resolved" ) {
//    m_boostedtree = new MVATree_BoostedVHcc(writeMVATree, readMVA, m_analysisType, wk(), m_variations, false);
//  }


  // initialize EasyTree
  m_etree = new EasyTree(writeEasyTree, m_analysisType, wk(), m_variations, false);

  bool writeOSTree = false;
  m_config->getif<bool>("writeOSTree", writeOSTree);
  m_OStree = new OSTree(writeOSTree, readMVA, m_analysisType, wk(), {"physics"}, false);

  // Fill reduced set of histograms
  m_config->getif<bool>("doReduceFillHistos", m_doReduceFillHistos);

  // Fill only histograms for fit inputs
  m_config->getif<bool>("doOnlyInputs", m_doOnlyInputs);

  // Run bootstrap
  m_config->getif<bool>("doBootstrap", m_doBootstrap);

  // do truth tagging?
  m_config->getif<bool>("doTruthTagging", m_doTruthTagging);

  m_config->getif<bool>("fillCr", m_doFillCR);

  m_config->getif<string>("jetCorrType",m_jetCorrType);

  m_config->getif<string>("fatjetCorrType",m_fatJetCorrType);
  if(m_CxAODTag=="CxAODTag26" && m_fatJetCorrType=="xbb") {
    m_fatJetCorrType="XbbMu"; //there was a different convention for what is now xbb
  }

  m_config->getif<std::string>("tagStrategy", m_tagStrategy);   // AllSignalJets,Leading2SignalJets,LeadingSignalJets

  m_config->getif<std::vector<std::string> >("csCorrections", m_csCorrections);
  m_config->getif<std::vector<std::string> >("csVariations", m_csVariations);

  m_config->getif<bool>("doMergeJetBins", m_doMergeJetBins);
  m_config->getif<bool>("doMergePtVBins", m_doMergePtVBins);
  m_config->getif<bool>("doMergeCR", m_doMergeCR);
  m_config->getif<bool>("doResolution", m_doResolution);
  m_config->getif<bool>("doBinnedSyst", m_doBinnedSyst);
  m_config->getif<bool>("doSherpaSyst", m_doSherpaSyst);

  return EL::StatusCode::SUCCESS;
} // initializeSelection

EL::StatusCode AnalysisReader_VHcc::initializeCorrsAndSysts ()
{
  //m_doTruthTagging is retrieved BEFORE m_isMC, so we need to put it after
  //This isn't the best sytlistically, but I'm not sure where else to put this cross-check
  m_doTruthTagging&=m_isMC;

  if (!m_isMC) return EL::StatusCode::SUCCESS;

  std::string comEnergy    = m_config->get<std::string>("COMEnergy");

  if ((comEnergy != "8TeV") || (comEnergy != "7TeV")) comEnergy = "13TeV";
  TString csname;
  std::string debugname;

  if (m_analysisType == "0lep") { csname = comEnergy + "_ZeroLepton"; debugname = comEnergy + "_ZeroLepton"; }

  if (m_analysisType == "1lep") { csname = comEnergy + "_OneLepton"; debugname =  comEnergy + "_OneLepton"; }

  if (m_analysisType == "2lep") { csname = comEnergy + "_TwoLepton"; debugname =  comEnergy + "_TwoLepton"; }

  if (m_analysisType == "vbfa" || m_analysisType == "vgamma") { csname = comEnergy + "_TwoLepton"; debugname =  comEnergy + "_TwoLepton"; } // fake config for vbfa analysis. Is it possible to skip it?

  Info("initializeCorrsAndSysts()", "Initializing CorrsAndSysts for %s", debugname.c_str());

  m_corrsAndSysts = new CorrsAndSysts(csname);

  return EL::StatusCode::SUCCESS;
} // initializeCorrsAndSysts


EL::StatusCode AnalysisReader_VHcc::initializeVariations (){
  EL_CHECK("AnalysisReader_VHcc::initializeVariations()", AnalysisReader::initializeVariations());

  string analysisType = "";
  bool nominalOnly = false;
  m_config->getif<string>("analysisType", analysisType);
  // TODO member n_nominalOnly would be useful
  m_config->getif<bool>("nominalOnly", nominalOnly);

  // trigger syst
  // -------------
  // TODO move triggerSystList to config?
  m_triggerSystList.clear();
  std::vector<std::string> triggerSystList;
  m_config->getif<std::vector<std::string> > ("triggerSystList",triggerSystList);
  if (!nominalOnly) {
    std::string finalTriggerSyst;
    for (auto triggerSyst : triggerSystList){
      //up
      finalTriggerSyst=triggerSyst+"__1up";
      Info("AnalysisReader_VHcc::initializeVariations()", finalTriggerSyst.c_str());
      m_triggerSystList.push_back(finalTriggerSyst);
      //down
      finalTriggerSyst=triggerSyst+"__1down";
      Info("AnalysisReader_VHcc::initializeVariations()", finalTriggerSyst.c_str());
      m_triggerSystList.push_back(finalTriggerSyst);
    }
  }

  //PU systematics - temporary fix to run PU syst in v26
  //---------------
  if(m_isMC && m_CxAODTag=="CxAODTag26" && !m_config->get<bool>("nominalOnly")){
    std::vector<std::string> variations;
    m_config->getif<std::vector<std::string> >("variations", variations);
    for (std::string varNameIn : variations) {
      if (varNameIn.find("PRW_DATASF") == 0){
	//make sure to not pushback twice the same syst
	bool alreadyPresent = false;
	for (std::string varName : m_variations) {
	  if (varName.find(varNameIn) == 0) alreadyPresent = true;
	}
	if(!alreadyPresent) {
	  m_variations.push_back("PRW_DATASF__1up");
	  m_variations.push_back("PRW_DATASF__1down");
	}
      }
    }
  }

  return EL::StatusCode::SUCCESS;
} //initializeVariations

EL::StatusCode AnalysisReader_VHcc::initializeTools ()
{
  EL_CHECK("AnalysisReader_VHcc::initializeTools()", AnalysisReader::initializeTools());

  // KinematicFitter
  // -----------------
  m_KF=new KF::KinematicFit();
  std::string datadir_KF=gSystem->Getenv("WorkDir_DIR");
  datadir_KF+="/data/KinematicFit/";
  m_KF->Init(datadir_KF,"2012",m_isMC);

  //combMass begin

  if ( m_doCombMass ) {

    /*
    if ( m_CxAODTag=="CxAODTag26" ) {
      //Fatjet syst init for JetUncertainties-00-09-51
      /*
      m_jetMassCorr = new JMSCorrection_lvqq("comb_mass_calc");
      m_jetMassCorr->msg().setLevel( this->msg().level() );
      if ( m_jetMassCorr->initializeTool("comb_mass_calc").isFailure() ) {
	ATH_MSG_FATAL("Couldn't initialize the JMS Calibration. Aborting");
	return StatusCode::FAILURE;
	}*/
      m_jesProviderHbb = new JetUncertaintiesTool("JESProviderHbb_HbbTagging_CombMass_medium");
      m_jesProviderHbb->msg().setLevel(MSG::ERROR);
      TOOL_CHECK("initializeTools()", m_jesProviderHbb->setProperty("JetDefinition", "AntiKt10LCTopoTrimmedPtFrac5SmallR20" ));
      TOOL_CHECK("initializeTools()", m_jesProviderHbb->setProperty("MCType", "MC15c"));
      TOOL_CHECK("initializeTools()", m_jesProviderHbb->setProperty("ConfigFile", "UJ_2015/ICHEP2016/HbbTagging_CombMass_medium.config"));
      TOOL_CHECK("initializeTools()", m_jesProviderHbb->initialize());

      bool nominalOnly(false);
      m_config->getif<bool>("nominalOnly", nominalOnly);
      if (!nominalOnly){
	std::vector<std::string> variations;
	m_config->getif<std::vector<std::string> >("variations", variations);
	CP::SystematicSet jes_sysSet = m_jesProviderHbb->recommendedSystematics();
	std::vector<std::string> pulls = {"__1down", "__1up"};
	const std::set<std::string> sysNames = jes_sysSet.getBaseNames();
	for (auto pull : pulls) {
	  for (auto sysName: sysNames) {
	    std::string sysPulled = sysName + pull;
	    m_combMassSyst.push_back(sysPulled);
	  }
	}
	for (auto var: variations) {
	  for (auto pull : pulls) {
	    std::string sysPulled = var + pull;
	    if (std::find(m_variations.begin(), m_variations.end(), sysPulled) == m_variations.end() &&
		std::find(m_combMassSyst.begin(), m_combMassSyst.end(), sysPulled) != m_combMassSyst.end() &&
		m_isMC){
	      m_variations.push_back(sysPulled);
	    }
	  }
	}
      }

      for(auto thisCombMassSyst : m_combMassSyst){
	std::cout<<thisCombMassSyst<<endl;
      }

    if(m_CxAODTag=="CxAODTag28"){
      //fatjet syst is not reculculated for Tag28
    }

  }

  //combMass end

  std::string ffin_el =  m_config->get<std::string>("fakerate_file_el");
  std::string ffin_mu =  m_config->get<std::string>("fakerate_file_mu");

  m_FakeFactor_el = new FakeFactor(*m_config,0);
  m_FakeFactor_mu = new FakeFactor(*m_config,1);
  EL_CHECK("AnalysisReader::initializeTools()", m_FakeFactor_el->initialize(ffin_el));
  EL_CHECK("AnalysisReader::initializeTools()", m_FakeFactor_mu->initialize(ffin_mu));

  if (!m_analysisContext) {
    m_analysisContext=new AnalysisContext;
  }
  m_analysisContext->setWorker(wk());
  m_analysisContext->setConfigStore(m_config);

  m_mva.reset();
  ToolKitManager::createAndSet( m_mvaToolName, m_analysisContext, m_mva );
  m_mva->initialize(m_analysisContext);

  return EL::StatusCode::SUCCESS;
} // initializeTools

EL::StatusCode AnalysisReader_VHcc::finalizeTools ()
{

  delete m_KF;

  EL_CHECK("AnalysisReader_VHcc::finalizeTools()", AnalysisReader::finalizeTools());

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_VHcc::applyTTbarNNLORW(bool isNominal) {
  float nnlo_ttbarPt = 0;
  float nnlo_topPt = 0;
  truthVariablesNNLO(nnlo_ttbarPt, nnlo_topPt);
  return NNLORW(nnlo_ttbarPt, nnlo_topPt, isNominal);
}



EL::StatusCode AnalysisReader_VHcc::NNLORW(float ttbarPt, float topPt, bool isNominal)
{
  bool corrNNLORWApplied = false;
  if (m_csCorrections.size() != 0) // apply corrections
  {
    for (decltype(m_csCorrections.size()) i = 0; i < m_csCorrections.size(); i++)
    {
      if(m_csCorrections[i] == "NNLORW")
      {
	corrNNLORWApplied = true;
	if(m_histNameSvc->get_sample() == "ttbar")
	{
	  m_weight = m_weight * m_myNNLO->GetTtbarAndTopPtWeight( ttbarPt, topPt );
	  if (m_myNNLO->GetTtbarAndTopPtWeight( ttbarPt, topPt ) == 0)
	    Warning("NNLORW()","SF = 0: not physical value!");

	}
      }
    }
  }
  float weightUp = 1;
  if (isNominal){
    for (decltype(m_csVariations.size()) i = 0; i < m_csVariations.size(); i++)
      {
	if(m_csVariations.at(i) == "NNLORW")
	  {
	    if(m_histNameSvc->get_sample() == "ttbar")
	      {
		weightUp = m_myNNLO->GetTtbarAndTopPtWeight( ttbarPt, topPt );
	      }
	    if (corrNNLORWApplied){
	      if (weightUp != 0)
		m_weightSysts.push_back({ (std::string)m_csVariations.at(i) + "__1down",   static_cast<float>(1/weightUp) });
	      else if (weightUp == 0)
		m_weightSysts.push_back({ (std::string)m_csVariations.at(i) + "__1down",   static_cast<float>(1.) });

	    }
	    else {
	      m_weightSysts.push_back({ (std::string)m_csVariations.at(i) + "__1up",   static_cast<float>(weightUp) });
	    }
	  }
      }
  }

  return EL::StatusCode::SUCCESS;
}

void AnalysisReader_VHcc::truthVariablesNNLO(float &truthPt,
					     float &topPt)

{
  truthPt=0;
  topPt=0;

  std::string samplename       =  m_histNameSvc->get_sample();

  if(samplename == "ttbar") // average pT of top,antitop truth pair
    // we need 2 truth-level quantities: average top pt (to get SysTopPt) and pT(ttbar) (to get SysTTbarPt)
    // use avgTopPt for SysTopPt and truthPt for SysTTbarPt
  {
    TLorentzVector p4_ttbar(0,0,0,0);
    TLorentzVector p4_top(0,0,0,0);
    for (auto *part : *m_truthParts)
    {
      if(part->absPdgId()==6)
      {
	p4_ttbar = p4_ttbar + part->p4();
      }
      if((part->pdgId())==6)
      {
	p4_top = part->p4(); // It's assumed there's only one top in a ttbar sample - true for DSID 410000 but should be checked before applying the reweighting to any other sample
      }
    }
    truthPt = p4_ttbar.Pt();
    topPt = p4_top.Pt();
  }
}

void AnalysisReader_VHcc::truthVariablesCS(float &truthPt,
					   float &avgTopPt)

{
  truthPt=0;
  avgTopPt=0;

  std::string samplename       =  m_histNameSvc->get_sample();

  if(samplename == "ttbar") // average pT of top,antitop truth pair
    // we need 2 truth-level quantities: average top pt (to get SysTopPt) and pT(ttbar) (to get SysTTbarPt)
    // use avgTopPt for SysTopPt and truthPt for SysTTbarPt
    {
      TLorentzVector p4_ttbar(0,0,0,0);
      for (auto *part : *m_truthParts)
	{
	  if(fabs(part->pdgId())==6)
	    {
	      p4_ttbar = p4_ttbar + part->p4();
	      avgTopPt += (part->pt()/2.0);
	      //truthPt += (part->pt()/2.0);
	    }
	}
      truthPt = p4_ttbar.Pt();
    }

  if(samplename =="Z" || samplename == "W" || (samplename == "Zv22") || (samplename == "Wv22")) // take truth pT of truth V candidate built from truth leptons
    {
      TLorentzVector l1,l2,V;
      for (auto *part : *m_truthParts)
	{
	  if(part->pdgId()==11 || part->pdgId()==12 || part->pdgId()==13 || part->pdgId()==14 || part->pdgId()==15 || part->pdgId()==16) l1 = part->p4();
	  if(part->pdgId()==-11 || part->pdgId()==-12 || part->pdgId()==-13 || part->pdgId()==-14 || part->pdgId()==-15 || part->pdgId()==-16) l2 = part->p4();
	}
      V = l1 + l2;
      truthPt = V.Pt();
    }

  if(samplename == "WW" || samplename == "ZZ") // cannot select the leptonic one -> take average pT
    {
      for (auto *part : *m_truthParts)
	{
	  if(fabs(part->pdgId())==23 || fabs(part->pdgId())==24) truthPt += (part->pt()/2.0);
	}
    }

  if(samplename == "WZ") // can discriminate between leptonic and hadronic V -> take leptonic V truth pT
    {
      int pdgId = -1;
      if(m_mcChannel==361083) pdgId=24; // WlvZqq
      if(m_mcChannel==361084) pdgId=23; // WqqZll
      if(m_mcChannel==361085) pdgId=23; // WqqZvv
      for (auto *part : *m_truthParts)
	{
	  if(fabs(part->pdgId())==pdgId) truthPt = (part->pt());
	}
    }

  if(samplename == "qqZllH125" || samplename == "qqZvvH125" || samplename == "qqWlvH125" || samplename == "ggZllH125" || samplename == "ggZvvH125")
    {
      int pdgId = 23;
      if(samplename == "qqWlvH125") pdgId = 24;
      for (auto *part : *m_truthParts)
	{
	  if(fabs(part->pdgId())==pdgId && part->status()==62) truthPt = (part->pt());
	}
    }
} // truthVariablesCS

EL::StatusCode AnalysisReader_VHcc::applyCS (float VpT,
                                             float MET,
                                             float nJet,
                                             float nTag,
                                             const TLorentzVector& jet1,
                                             const TLorentzVector& jet2) {
  // TODO: Check quantities (truth, reco, missing, ...) --> not well defined yet
  TLorentzVector dijet = jet1 + jet2;
  float cs_dr       = fabs(jet1.DeltaR(jet2));
  float cs_dphi     = fabs(jet1.DeltaPhi(jet2));
  float cs_mbb      = dijet.M();
  float cs_ptb1     = jet1.Pt();
  float cs_ptb2     = jet2.Pt();
  float cs_truthPt  = -1;
  float cs_avgTopPt = -1;
  truthVariablesCS(cs_truthPt, cs_avgTopPt);
  return applyCS(VpT, cs_mbb, cs_truthPt, cs_dphi, cs_dr, cs_ptb1, cs_ptb2, MET, cs_avgTopPt, nJet, nTag);
}

EL::StatusCode AnalysisReader_VHcc::applyCS (float VpT,
                                   float Mbb,
                                   float truthPt,
                                   float DeltaPhi,
                                   float DeltaR,
                                   float pTB1,
                                   float pTB2,
                                   float MET,
                                   float avgTopPt,
                                   int   njet,
                                   int   ntag)
{
  std::string detailSampleNameClean = m_xSectionProvider->getSampleDetailName(m_mcChannel);
  std::string sampleNameClean       =  m_histNameSvc->get_sample();
  sampleCS    sample("temp");
  sample.setup_sample(sampleNameClean, detailSampleNameClean);
  std::string flav = "";

  if ((sampleNameClean == "Z") || (sampleNameClean == "W") || (sampleNameClean == "Zv22") || (sampleNameClean == "Wv22"))
  {
    int flav0, flav1 = -1;
    m_histNameSvc->get_eventFlavour(flav0,flav1);

    if(flav0 < 0 || flav1 < 0){
      Error("applyCS()","Failed to retrieve event flavour! Exiting!");
      return EL::StatusCode::FAILURE;
    }
    else if (flav0 == 5 || flav1 == 5) flav = "b";
    else if (flav0 == 4 || flav1 == 4){
      flav = "c";
      if(flav0 == flav1)  flav = "cc";
    }
    else if (flav0 < 4 && flav1 < 4) flav = "l";
  }
  TString sampleName                 = sample.get_sampleName() + flav;
  TString detailsampleName           = sample.get_detailsampleName();
  CAS::EventType evtType             = m_corrsAndSysts->GetEventType(sampleName);
  CAS::DetailEventType detailevtType = m_corrsAndSysts->GetDetailEventType(detailsampleName);
  double weightUp                    = 1;
  double weightDo                    = 1;

  if (m_csCorrections.size() != 0) // apply corrections
  {
    for (decltype(m_csCorrections.size()) i = 0; i < m_csCorrections.size(); i++)
    {
      if(m_csCorrections[i] == "VpTEFTCorrectionfit_kHdvR1Lambda1ca1")
	{
	  m_weight = m_weight * m_corrsAndSysts->Get_VpTEFTCorrection_kHdvR1Lambda1ca1(evtType, truthPt);
	}
      else if(m_csCorrections[i] == "VpTEFTCorrectionfit_kSM1HdvR1Lambda1ca1")
	{
	  m_weight = m_weight * m_corrsAndSysts->Get_VpTEFTCorrection_kSM1HdvR1Lambda1ca1(evtType, truthPt);
	}
      else if(m_csCorrections[i] == "VHNLOEWK")
      {
        m_weight = m_weight * m_corrsAndSysts->Get_HiggsNLOEWKCorrection(evtType, truthPt);
      }
      else if(m_csCorrections[i] == "NNLORW")
	{
// No need to do anything, as applied elsewhere
	}
      else{
	Error("applyCS()", "Cannot retrieve correction from CorrsAndSysts package %s. Exiting.", m_csCorrections[i].c_str());
	return EL::StatusCode::FAILURE;
      }
    }
  }

  bool nominalOnly = false;
  m_config->getif<bool>("nominalOnly", nominalOnly);
  if (!nominalOnly && m_csVariations.size() != 0) // apply variations
  {
    for (decltype(m_csVariations.size()) i = 0; i < m_csVariations.size(); i++)
    {

      TString s_csVariations(m_csVariations[i]);

      CAS::Systematic sysUp;
      CAS::SysVar     varUp = CAS::Up;
      CAS::SysVar     varDo = CAS::Do;
      CAS::SysBin     bin   = CAS::Any;

      m_corrsAndSysts->GetSystFromName(s_csVariations, sysUp);
      CAS::Systematic sysDo = sysUp;

      if(s_csVariations == "SysTopPt") // pass averate top+antitop pT as truthPt variable
	{
	  weightUp = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, avgTopPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysUp, varUp, bin);
	  weightDo = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, avgTopPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysDo, varDo, bin);
	}
      else // use truthPt variable (which for SysTTbarPt is the pT of the ttbar pair))
	{
	  weightUp = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, truthPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysUp, varUp, bin);
	  weightDo = m_corrsAndSysts->Get_SystematicWeight(evtType, VpT, Mbb, truthPt, DeltaPhi, DeltaR, pTB1, pTB2, MET, njet, ntag, detailevtType, sysDo, varDo, bin);
	}

      if (s_csVariations.BeginsWith("Sys")) s_csVariations.Remove(0, 3);  // remove "Sys" from name to avoid duplicating it

      std::string samplename_tmp       =  m_histNameSvc->get_sample();

      // Fill the histograms only if at least one side has a non-0 variation
      if(fabs(weightUp - 1) > 1.e-6 || fabs(weightDo - 1) > 1.e-6) {
        m_weightSysts.push_back({ (std::string)s_csVariations + "__1up",   static_cast<float>(weightUp) });
        m_weightSysts.push_back({ (std::string)s_csVariations + "__1down", static_cast<float>(weightDo) });
      }
    }
  }
  return EL::StatusCode::SUCCESS;
} // applyCS

EL::StatusCode AnalysisReader_VHcc::fill_bTaggerHists (const xAOD::Jet *jet)
{
  // fills histograms per jet and jet flavour for a list of b-taggers
  std::string flav = "Data";

  if (m_isMC) {
    int label = Props::HadronConeExclTruthLabelID.get(jet);

    flav = "L";

    if (label == 4) flav = "C";
    else if (label == 5) flav = "B";
  }

  float  MV2c10   = Props::MV2c10.get(jet);
  double BVar     = BTagProps::tagWeight.get(jet);

  if (m_isMC) {
    double BEff = BTagProps::eff.get(jet);
    m_histSvc->BookFillHist("eff_"   + flav, 110, -0.1, 1.1, BEff,  m_weight);
    m_histSvc->BookFillHist("pT_eff_"  + flav, 400,   0, 400, 110, 0.0, 1.1, jet->pt() / 1e3, BEff,  m_weight);
    m_histSvc->BookFillHist("eta_eff_" + flav, 100,  -5,   5, 110, 0.0, 1.1, jet->eta(), BEff,     m_weight);
  }
  m_histSvc->BookFillHist("btag_weight_"   + flav, 110, -1.1, 1.1, BVar,   m_weight);
  m_histSvc->BookFillHist("MV2c10_"   + flav, 110, -1.1, 1.1, MV2c10,   m_weight);

  return EL::StatusCode::SUCCESS;
} // fill_bTaggerHists

EL::StatusCode AnalysisReader_VHcc::fill_TLV(std::string name, const TLorentzVector &vec, double weight , bool isE , bool isMu) {
//     if ((vec.Pt() > 14e6) && (vec.E() > 14e6)) return EL::StatusCode::SUCCESS;
    m_histSvc->BookFillHist(name + "Pt",  600, 0, 3000,  vec.Pt() / 1e3, weight);
    m_histSvc->BookFillHist(name + "Eta", 100, -5., 5., vec.Eta(), weight);
    m_histSvc->BookFillHist(name + "Phi", 100, -TMath::Pi(), TMath::Pi(), vec.Phi(), weight);
    m_histSvc->BookFillHist(name + "M",   600, 0, 3000, vec.M() / 1e3, weight);
    if(isE){
    m_histSvc->BookFillHist("El_" + name + "Pt",  600, 0, 3000,  vec.Pt() / 1e3, weight);
    m_histSvc->BookFillHist("El_" + name + "Eta", 100, -5., 5., vec.Eta(), weight);
    m_histSvc->BookFillHist("El_" + name + "Phi", 100, -TMath::Pi(), TMath::Pi(), vec.Phi(), weight);
    m_histSvc->BookFillHist("El_" + name + "M",   600, 0, 3000, vec.M() / 1e3, weight);
    }
    if(isMu){
    m_histSvc->BookFillHist("Mu_" + name + "Pt",  600, 0, 3000,  vec.Pt() / 1e3, weight);
    m_histSvc->BookFillHist("Mu_" + name + "Eta", 100, -5., 5., vec.Eta(), weight);
    m_histSvc->BookFillHist("Mu_" + name + "Phi", 100, -TMath::Pi(), TMath::Pi(), vec.Phi(), weight);
    m_histSvc->BookFillHist("Mu_" + name + "M",   600, 0, 3000, vec.M() / 1e3, weight);
    }

//     m_histSvc->BookFillHist(name + "E",   600, 0, 3000, vec.E() / 1e3, weight);
    return EL::StatusCode::SUCCESS;
}

/*
EL::StatusCode AnalysisReader_VHcc::fill_VBFInclCutFlow (std::string label)
{
  std::string dir = "CutFlow/Nominal/";

  static std::string cuts[18] = {
    "All", "J4Pt", "J5Pt", "2btag", "Ptbb"
  };

  m_histSvc->BookFillCutHist(dir + "Cuts", length(cuts), cuts, label, m_weight);
  m_histSvc->BookFillCutHist(dir + "CutsNoWeight", length(cuts), cuts, label, 1.);

  return EL::StatusCode::SUCCESS;
} // fill_VBFInclCutFlow
*/

float AnalysisReader_VHcc::compute_JVTSF(const std::vector<const xAOD::Jet*>& signalJets)
{

  float SF = 1.0;

  //this seems not correct following the email exchange "Applying JVT scale factors"
  //on the hn-atlas-jet-etmiss-wg (JetEtMissWG) mailing list (started 16.1.17)
  //need to wait for correct recommendations, until then don't use SFs
  //for (auto jet : signalJets) {
  //
  //  SF *= Props::JvtSF.get(jet);
  //
  //}
  if(m_CxAODTag>="CxAODTag28")
    SF *= Props::JvtSF.get(m_eventInfo);

  return SF;
}


void AnalysisReader_VHcc::compute_ctagging (float b_fraction, float discriminant_cut)
{

  ////// Jan 17 2018 ///////////////////////////////////////////////////////////////////////////////
  // c-tagging functionality will be added to CxAOD btagging tool at some point,
  // for optimisation studies its simpler to have it defined in the reader itself
  ////////////////////////////////////////////////////////////////////////////////////////////////////

  if (m_jets) {
    auto previous_ja = m_bTagTool->getJetAuthor();

    m_bTagTool->setJetAuthor(m_jetReader->getContainerName());

    for (auto jet : *m_jets) {

      float dl1_pb = Props::DL1_pb.get(jet);
      float dl1_pc = Props::DL1_pc.get(jet);
      float dl1_pu = Props::DL1_pu.get(jet);

      float discriminant = log( dl1_pc/(b_fraction*dl1_pb+(1.-b_fraction)*dl1_pu) );

      BTagProps::tagWeight.set(jet, discriminant);

      if(discriminant > discriminant_cut){
        BTagProps::isTagged.set(jet, true);
      }else{
         BTagProps::isTagged.set(jet, false);
      }

      //efficiency extraction not possible until CxAOD btagging tool update
      //if (m_isMC) BTagProps::eff.set(jet, m_bTagTool->getEfficiency(*jet));

    }
    m_bTagTool->setJetAuthor(previous_ja);
  }

} // compute_ctagging


void AnalysisReader_VHcc::compute_btagging ()
{
  if (m_trackJets) {
    auto previous_ja = m_bTagTool->getJetAuthor();
    auto previous_ts = m_bTagTool->getTaggingScheme();

    m_bTagTool->setJetAuthor(m_trackJetReader->getContainerName());
    m_bTagTool->setTaggingScheme("FixedCut");

    for (auto jet : *m_trackJets) {
      BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
    }

    m_bTagTool->setJetAuthor(previous_ja);
    m_bTagTool->setTaggingScheme(previous_ts);
  }

  if (m_jets) {
    auto previous_ja = m_bTagTool->getJetAuthor();
    if(!m_use2DbTagCut) m_bTagTool->setJetAuthor(m_jetReader->getContainerName());

    for (auto jet : *m_jets) {
      if(!m_use2DbTagCut) BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      else BTagProps::isTagged.set(jet, false);

      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));

      if (m_isMC && !m_use2DbTagCut) BTagProps::eff.set(jet, m_bTagTool->getEfficiency(*jet));
      else BTagProps::eff.set(jet, -999.);
    }
    m_bTagTool->setJetAuthor(previous_ja);
  }

  if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      Props::nTrackJets.set(fatjet, 0);
      Props::nBTags.set(fatjet, 0);
      if (m_isMC) Props::nTrueBJets.set(fatjet, 0);
      else Props::nTrueBJets.set(fatjet, -1);
    }
  }

} // compute_btagging

void AnalysisReader_VHcc::compute_TRF_tagging (const std::vector<const xAOD::Jet*>& signalJets)
{
  bool ttag_track_jets=false;
  if (m_trackJets) {
    m_bTagTool->setJetAuthor(m_trackJetReader->getContainerName());
    if(ttag_track_jets)    m_bTagTool->truth_tag_jets(m_eventInfo->eventNumber(),*m_trackJets,m_config);
    else{
      for (auto jet : *m_trackJets) {
	BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
	BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
      }
    }
  }

  if (m_jets) {//we assume signal jets are taken from the larger jet container
    m_bTagTool->setJetAuthor(m_jetReader->getContainerName());
    m_bTagTool->truth_tag_jets(m_eventInfo->eventNumber(),signalJets,m_config);
  }

  if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      Props::nTrackJets.set(fatjet, 0);
      Props::nBTags.set(fatjet, 0);
      if (m_isMC) Props::nTrueBJets.set(fatjet, 0);
      else Props::nTrueBJets.set(fatjet, -1);
    }
  }

} // compute_TRF_tagging

void AnalysisReader_VHcc::compute_fatjetTags (const std::vector<const xAOD::Jet*> &trackJets, const std::vector<const xAOD::Jet*> &fatJets,
                                              std::vector<const xAOD::Jet*> *trackJetsInLeadFJ=nullptr, std::vector<const xAOD::Jet*> *trackJetsNotInLeadFJ=nullptr)
{
  for (unsigned int i=0; i < fatJets.size(); ++i) {
    const xAOD::Jet* fatjet = fatJets.at(i);
    using FatJetType = typename std::remove_pointer<decltype(fatjet)>::type;
    static FatJetType::ConstAccessor < vector < ElementLink < DataVector < xAOD::IParticle >> >>
      GhostAccessor("GhostAntiKt2TrackJet");

    if (Props::isFatJet.get(fatjet)) {
      // determine number of b-tags in a fat jet
      //  number of tracks will be the track jets with closest DR with the ghost matched track jets
      std::vector<const xAOD::Jet*> trackJetsInFatJet;
      std::vector<const xAOD::Jet*> trackJetsNotInFatJet;

      // find the track jet that match to a ghost-associated jet
      for (auto trackJet : trackJets) {
        auto minDR                     = 100.0;
        const xAOD::Jet *minDRTrackJet = nullptr;

        // loop over the ghost associated track jets
        for (auto gTrackParticle : GhostAccessor(*fatjet)) {
          if (!gTrackParticle.isValid()) continue;
          const auto &temp = **gTrackParticle;
          auto gTrackJet   = dynamic_cast<const xAOD::Jet*>(&temp);
          auto DR          = trackJet->p4().DeltaR(gTrackJet->p4());

	  if (minDR > DR) {
            minDR         = DR;
            minDRTrackJet = gTrackJet;
          }
        }

        //Check if trackjet is matched
        if (minDRTrackJet && (minDR < 0.1)) {
          trackJetsInFatJet.push_back(trackJet);
          // If leading FJ store associated trackjets
          if (i == 0 && trackJetsInLeadFJ != nullptr){
            if(trackJetsInLeadFJ->size() < 2) trackJetsInLeadFJ->push_back(trackJet);
          }
        }else{
          trackJetsNotInFatJet.push_back(trackJet);
          if(i==0 && trackJetsNotInLeadFJ != nullptr){
            trackJetsNotInLeadFJ->push_back(trackJet);
          }
        }
      }//end loop on track jets
      Props::nTrackJets.set(fatjet, trackJetsInFatJet.size());

      //count number of b-tagged track jets inside the fat jet
      auto nTags = 0;
      for (auto trackJet : trackJetsInFatJet){
	if (BTagProps::isTagged.get(trackJet))
          ++nTags;
      }
      Props::nBTagsAll.set(fatjet, nTags);

      //Count the number of B-Tagged jets outside the fat jet (TopCR)
      int nAddTags = 0;
      for(const xAOD::Jet* trackjet : trackJetsNotInFatJet){
        if( BTagProps::isTagged.get(trackjet) ){ nAddTags++; }
      }
      Props::nAddBTags.set(fatjet, nAddTags);

      //define number of b-tags based on the number of b-tagged (sub)leading track jets
      nTags = 0; //reuse
      if (trackJetsInFatJet.size() >= 1 ){
	if ( BTagProps::isTagged.get(trackJetsInFatJet.at(0))){
          nTags++;
        }
        if (trackJetsInFatJet.size() >= 2 &&
            BTagProps::isTagged.get(trackJetsInFatJet.at(1))) {
          nTags++;
        }
      }
      Props::nBTags.set(fatjet, nTags);

      // finished b-tagging criteria
      // determine number of true b-jets in a fat jet with simple DR
      auto nBJets = 0;
      if (m_isMC) {
        for (auto trackJet : trackJetsInFatJet) {
          auto label = Props::HadronConeExclTruthLabelID.get(trackJet);
          if (label == 5) ++nBJets;
        }
      }
      else nBJets = -1;  // Data
      Props::nTrueBJets.set(fatjet, nBJets);

      // finished true b-jet association criteria
    }
  }//end loop on fat jets
}

void AnalysisReader_VHcc::tagjet_selection (
  std::vector<const xAOD::Jet*>  signalJets,
  std::vector<const xAOD::Jet*>  forwardJets,
  std::vector<const xAOD::Jet*> &selectedJets,
  int                           &tagcatExcl)
{
  string tagAlgorithm;

  m_config->getif<std::string>("tagAlgorithm", tagAlgorithm); // FlavLabel,FlavTag

  selectedJets.clear();
  tagcatExcl = -1;

  /////////////////////////////////////////////////////////////
  // **B-Tagging Selection**
  bool Lead1BTag        = false;
  bool Lead2BTag        = false;
  int  Ind1BTag         = -1;
  int  Ind2BTag         = -1;
  int  nbtag            = 0;
  int  jetidx           = 0;
  int  nSignalJet       = signalJets.size();
  int  nForwardJet      = forwardJets.size();
  const xAOD::Jet *Jet1 = nullptr;
  const xAOD::Jet *Jet2 = nullptr;
  const xAOD::Jet *Jet3 = nullptr;

  //check if the leading signal jets are b-tagged
  //----------------------------------------------
  if (m_isMC && (tagAlgorithm == "FlavLabel")){ // use truth label to select b-jets (MC only!)
    if (signalJets.size() > 0 && Props::HadronConeExclTruthLabelID.get(signalJets.at(0)) == 5) Lead1BTag = true;
    if (signalJets.size() > 1 && Props::HadronConeExclTruthLabelID.get(signalJets.at(1)) == 5) Lead2BTag = true;
  }
  else{ // if (tagAlgorithm == "FlavTag") use b-tagging to select b-jets (the only option for data)
    if (signalJets.size() > 0 && BTagProps::isTagged.get(signalJets.at(0)) == 1) Lead1BTag = true;
    if (signalJets.size() > 1 && BTagProps::isTagged.get(signalJets.at(1)) == 1) Lead2BTag = true;
  }

  //count number of b-tagged jets
  //find indices of leading ones
  //-----------------------------
  for (const xAOD::Jet *jet : signalJets){

    bool isTagged = BTagProps::isTagged.get(jet);

    //if using truth label (MC only!)
    if (m_isMC && (tagAlgorithm == "FlavLabel")) isTagged = (Props::HadronConeExclTruthLabelID.get(jet) == 5);

    if (isTagged){

      nbtag++;

      if (Ind1BTag < 0) Ind1BTag = jetidx;
      else if (Ind2BTag < 0) Ind2BTag = jetidx;
    }
    jetidx++;
  }

  /////////////////////////////////////////////////////////////
  // **Tag-Category Definition**
  if ((m_tagStrategy == "AllSignalJets") || (m_tagStrategy == "LeadingSignalJets")){

    tagcatExcl = nbtag;

    //0tag
    //----
    // decreasing pT ordering
    if (nbtag == 0) {
      if (nSignalJet >= 1) Jet1 = signalJets.at(0);
      if (nSignalJet >= 2) {
	Jet2 = signalJets.at(1);
	if (nSignalJet >= 3) Jet3 = signalJets.at(2);
	else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
      }
    }

    //1 tag
    //-----
    // all sig jets: first leading jet, even if not b-tagged
    if (nbtag == 1 && m_tagStrategy == "AllSignalJets"){

      // first is always the leading, whether it's b-tagged or not
      if (nSignalJet >= 1)
         Jet1 = signalJets.at(0);

      // second is either the sub-leading (if the 1st is tagged) or the b-tagged
      if (nSignalJet >= 2) {
         if ( Lead1BTag || Lead2BTag )
            Jet2 = signalJets.at(1);
         else
            Jet2 = signalJets.at(Ind1BTag);


      // third is the third sub-leading, unless the third is b-tagged (in that case it is in position 2 in the vector)
      if (nSignalJet >= 3) {

         if ( Ind1BTag >= 2 )
            Jet3 = signalJets.at(1);
         else
            Jet3 = signalJets.at(2);

      } else if (nForwardJet >= 1) {
          Jet3 = forwardJets.at(0);
      }
       }
    } else if (nbtag == 1 && m_tagStrategy == "LeadingSignalJets"){
      Jet1 = signalJets.at(Ind1BTag);      // * leading b-tagged signal jet
      if ((m_tagStrategy == "LeadingSignalJets") && !Lead1BTag) tagcatExcl = -1;     // * LeadingSignalJets: leading b-tagged jet must be leading jet

      if (nSignalJet >= 2) {
        if (Ind1BTag == 0) Jet2 = signalJets.at(1);
        else Jet2 = signalJets.at(0);

        if (nSignalJet >= 3) {
          if (Ind1BTag < 2) Jet3 = signalJets.at(2);
          else Jet3 = signalJets.at(1);
        }
        else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
      }
    }

    //2 tag
    //-----
    if (nbtag >= 2){

      Jet1 = signalJets.at(Ind1BTag);
      Jet2 = signalJets.at(Ind2BTag);
      if ((m_tagStrategy == "LeadingSignalJets") && !Lead1BTag) tagcatExcl = -1;  // * LeadingSignalJets: leading b-tagged jet must be leading jet

      int jetidx3 = 0;
      if (nSignalJet >= 3){
	for (const xAOD::Jet *jet : signalJets) {
	  if ((jetidx3 != Ind1BTag) && (jetidx3 != Ind2BTag)) {
	    Jet3 = jet;
	    break;
	  }
	  jetidx3++;
	}
      }
      else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
    }
  }

  /////////////////////////////////////////////////////////////
  if (m_tagStrategy == "Leading2SignalJets"){

    if (!Lead1BTag && !Lead2BTag) tagcatExcl = 0;
    if ((Lead1BTag && !Lead2BTag) || (!Lead1BTag && Lead2BTag)) tagcatExcl = 1;
    if (Lead1BTag && Lead2BTag && nbtag>=2) tagcatExcl = nbtag;

    if (nSignalJet >= 1) Jet1 = signalJets.at(0);
    if (nSignalJet >= 2) Jet2 = signalJets.at(1);
    if (nSignalJet >= 3) Jet3 = signalJets.at(2);
    else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
  }
  //fill selected jets
  //------------------
  if (Jet1) selectedJets.push_back(Jet1);
  if (Jet2) selectedJets.push_back(Jet2);
  if (Jet3) selectedJets.push_back(Jet3);

  /////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////
} // tagjet_selection

EL::StatusCode AnalysisReader_VHcc::fill_nJetHistos (const std::vector<const xAOD::Jet*> &jets, const string &jetType) {
  int  nJet    = jets.size();
  bool isSig   = (jetType == "Sig");

  if (!m_doReduceFillHistos) {

    m_histSvc->BookFillHist("N" + jetType + "Jets", 11, -0.5, 10.5, nJet, m_weight);

    for (const xAOD::Jet *jet : jets) {
      m_histSvc->BookFillHist("Pt" + jetType + "Jets", 100,  0, 100, jet->pt() / 1e3, m_weight);
      m_histSvc->BookFillHist("Eta" + jetType + "Jets", 100, -5,   5, jet->eta(),    m_weight);
    }

    // TODO: we may want to allow track-jets to be handled by fill_bTaggerHists - some changed would be needed there
    if (isSig) {
      for (const xAOD::Jet *jet : jets) {
        EL_CHECK("AnalysisReader_VHcc::fill_nJetHistos", fill_bTaggerHists(jet));
      }
    }
  }

  return EL::StatusCode::SUCCESS;
} // fill_nJetHistos

EL::StatusCode AnalysisReader_VHcc::fill_fatJetHistos (const std::vector<const xAOD::Jet*> &fatJets) {

  TLorentzVector HVec = m_etree->exists("HVec") ? (*m_etree)["HVec"] : TLorentzVector();

  if (!m_doReduceFillHistos) {
    for (auto fatjet : fatJets) {
      decltype(m_weight) eventWeight = 1.0;
      auto GeV = 1.0E-3;

      m_histSvc->BookFillHist("nBTag_nTrackJets_FatJet", 5, 0, 5, 10, 0, 10, Props::nBTags.get(fatjet), Props::nTrackJets.get(fatjet),  eventWeight);

      if (m_isMC) {
        m_histSvc->BookFillHist("nBTag_nTrueBJets_FatJet", 5, 0, 5, 6, 0, 6, Props::nBTags.get(fatjet), Props::nTrueBJets.get(fatjet),  eventWeight);
        m_histSvc->BookFillHist("nTrueBJets_nTrackJets_FatJet", 6, 0, 6, 10, 0, 10, Props::nTrueBJets.get(fatjet), Props::nTrackJets.get(fatjet),  eventWeight);
        m_histSvc->BookFillHist("nTrueBJets_eta_FatJet", 6, 0, 6, 30, -3, 3, Props::nTrueBJets.get(fatjet), fatjet->eta(),  eventWeight);
        m_histSvc->BookFillHist("nTrueBJets_pT_FatJet", 6, 0, 6, 90, 100, 1000, Props::nTrueBJets.get(fatjet), fatjet->pt() * GeV,  eventWeight);
      }
      m_histSvc->BookFillHist("eta_nTrackJets_FatJet", 30, -3, 3, 10, 0, 10, fatjet->eta(), Props::nTrackJets.get(fatjet),  eventWeight);
      m_histSvc->BookFillHist("pT_nTrackJets_FatJet", 90, 100, 1000, 10, 0, 10, fatjet->pt() * GeV, Props::nTrackJets.get(fatjet),  eventWeight);
      m_histSvc->BookFillHist("nBTag_eta_FatJet", 5, 0, 5, 30, -3, 3, Props::nBTags.get(fatjet), fatjet->eta(),  eventWeight);
      m_histSvc->BookFillHist("nBTag_pT_FatJet", 5, 0, 5, 90, 100, 1000, Props::nBTags.get(fatjet), fatjet->pt() * GeV,  eventWeight);
    }
  }

  if ( fatJets.size() >= 1 ) {
    if (!m_doReduceFillHistos) {
      m_histSvc->BookFillHist("PtFatJ1", 75, 0,   1500,  HVec.Pt()/1.e3,     m_weight);
      m_histSvc->BookFillHist("EtaFatJ1", 25, -2.5,   2.5,  HVec.Eta(),     m_weight);
      m_histSvc->BookFillHist("NtrkjetsFatJ1", 10, -0.5,   9.5,  Props::nTrackJets.get(fatJets.at(0)),     m_weight);
      m_histSvc->BookFillHist("NbTagsFatJ1", 5, -0.5,   4.5,  Props::nBTags.get(fatJets.at(0)),     m_weight);
      if ( fatJets.size() >= 2 ) {
        TLorentzVector fatj2Vec = m_etree->exists("fatj2Vec") ? (*m_etree)["fatj2Vec"] : TLorentzVector();
        m_histSvc->BookFillHist("PtFatJ2", 150, 0,   1500,  fatj2Vec.Pt()/1.e3,     m_weight);
        m_histSvc->BookFillHist("EtaFatJ2", 60, -3,   3,  fatj2Vec.Eta(),     m_weight);
        m_histSvc->BookFillHist("MFatJ2", 100, 0,  500,  fatj2Vec.M()/1.e3,     m_weight);
        m_histSvc->BookFillHist("NtrkjetsFatJ2", 10, -0.5,   4.5,  Props::nTrackJets.get(fatJets.at(1)),     m_weight);
        m_histSvc->BookFillHist("NbTagsFatJ2", 5, -0.5,   4.5,  Props::nBTags.get(fatJets.at(1)),     m_weight);
      }
    }
  }

  return EL::StatusCode::SUCCESS;
} // fill_fatJetHistos

void AnalysisReader_VHcc::setevent_flavour (const std::vector<const xAOD::Jet*> &selectedJets)
{
  if (m_isMC) {
    int jet0flav = -1;
    int jet1flav = -1;
    int jet2flav = -1;

    if ( selectedJets.size() > 0 ){
      jet0flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(0));
    }
    if ( selectedJets.size() > 1 ){
      jet1flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(1));
    }
    if ( selectedJets.size() > 2 ){
      jet2flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(2));
    }
    m_physicsMeta.b1Flav = jet0flav;
    m_physicsMeta.b2Flav = jet1flav;
    m_physicsMeta.j3Flav = jet2flav;
  }
} // setevent_flavour

void AnalysisReader_VHcc::setevent_flavourGhost(
    const std::vector<const xAOD::Jet *> &selectedJets) {
  //////////////////////////////
  // Get event flavour from the truth particles that are ghost matched to
  // the leading jet of selectedJets
  // Do this by checking the number of matched BHadrons.
  // If the number is >= 2 tag both jets 'b'
  // If the number is == 1 but there is a matched prompt C hadron, tag the
  // jets 'bc'
  // If the number is == 0 and there is no matched prompt C hadron, tag the
  // jets 'bl'
  // (do the corresponding thing for 'cc', 'cl' [and respect 'cb' == 'bc'])
  //
  // setevent_flavour is not suited for this as it is
  // (ahoenle, 2016-05-30)
  //////////////////////////////
  if (m_isMC) {
    int jet0flav = -1;
    int jet1flav = -1;
    if (selectedJets.size() > 0) {
      if (!Props::nGhostMatchedBHadrons.exists(selectedJets.at(0))) {
        // ghost matching not available -> Error
        Error("setevent_flavourGhost()",
              "Ghost Matching not available for "
              "this event. No labeling can be applied.");
        return;
      } else {
        int nGMBHadrons = Props::nGhostMatchedBHadrons.get(selectedJets.at(0));
        int nGMPromptCHadrons =
            Props::nGhostMatchedPromptCHadrons.get(selectedJets.at(0));
        // 1) two or more B hadrons
        if (nGMBHadrons >= 2) {
          jet0flav = 5;
          jet1flav = 5;
        // 2) exactly one B hadron
        } else if (nGMBHadrons == 1) {
          jet0flav = 5;
          if (nGMPromptCHadrons >= 1) {
            jet1flav = 4;
          } else {
            jet1flav = 0; // != 4, != 5 makes light in HistNameSvc.cxx
          }
        // 3) zero or less B hadrons implicit
        //    -> third case two or more C Hadrons
        } else if (nGMPromptCHadrons >= 2) {
          jet0flav = 4;
          jet1flav = 4;
        // 4) exactly one C Hadron
        } else if (nGMPromptCHadrons == 1) {
          jet0flav = 4;
          jet1flav = 0;
        // 5) only light
        } else {
          jet0flav = 0;
          jet1flav = 0;
        }
        m_physicsMeta.b1Flav = jet0flav;
        m_physicsMeta.b2Flav = jet1flav;
	m_physicsMeta.j3Flav = -1;
	if(selectedJets.size() > 1) m_physicsMeta.j3Flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(1)); //this is supposed to be the leading non-matched track jet
      }
    }
  }
} // setevent_flavourGhost


void AnalysisReader_VHcc::setevent_nJets   (const std::vector<const xAOD::Jet*> &signalJets,
                                           const std::vector<const xAOD::Jet*> &forwardJets)
{
  auto nSignalJet  = signalJets.size();
  auto nForwardJet = forwardJets.size();
  auto nJet        = nSignalJet + nForwardJet;

  m_physicsMeta.nJets       = nJet;
  m_physicsMeta.nSigJet     = nSignalJet;
  m_physicsMeta.nForwardJet = nForwardJet;
} // setevent_nJets

std::string AnalysisReader_VHcc::determine_regime () {
  return "resolved";

  // return "merged";
}

EL::StatusCode AnalysisReader_VHcc::fill_jetSelectedHistos () {
  int nJet = m_etree->get<int>("nJets");

  if (!m_doOnlyInputs) {
    // Main histograms
    if(!m_doBlinding) m_histSvc->BookFillHist("mBB", 100, 0, 500,   m_etree->get<float>("mBB") / 1e3, m_weight);
    // Additional histograms
    if (!m_doReduceFillHistos) {
      m_histSvc->BookFillHist("pTB1",   200, 0, 2000,  m_etree->get<float>("pTB1") / 1e3, m_weight);
      m_histSvc->BookFillHist("pTB2",   150, 0, 1500,  m_etree->get<float>("pTB2") / 1e3, m_weight);
      m_histSvc->BookFillHist("EtaB1", 60, -3,   3,  m_etree->get<float>("etaB1"),     m_weight);
      m_histSvc->BookFillHist("EtaB2", 60, -3,   3,  m_etree->get<float>("etaB2"),     m_weight);
      m_histSvc->BookFillHist("dPhiBB", 50, 0, 3.15, std::fabs(m_etree->get<float>("dPhiBB")),     m_weight);
      m_histSvc->BookFillHist("dEtaBB", 100, 0,   5,  m_etree->get<float>("dEtaBB"),     m_weight);
      if (m_etree->exists("MV2c10B1")) m_histSvc->BookFillHist("MV2c10B1",100,-1,1,    m_etree->get<float>("MV2c10B1"),   m_weight);
      if (m_etree->exists("MV2c10B2")) m_histSvc->BookFillHist("MV2c10B2",100,-1,1,    m_etree->get<float>("MV2c10B2"),   m_weight);
      m_histSvc->BookFillHist("dRBB",   100, 0,   5,  m_etree->get<float>("dRBB"),       m_weight);
      m_histSvc->BookFillHist("pTBB",   400, 0, 2000, m_etree->get<float>("pTBB")/ 1e3,  m_weight);

      if (nJet >= 3) {//this should actually be if (selectedJets.size() >= 3)...see compute_jetSelectedQuantities
	m_histSvc->BookFillHist("pTJ3",   100, 0, 500,  m_etree->get<float>("pTJ3") / 1e3,  m_weight);
	m_histSvc->BookFillHist("EtaJ3", 100, -5,   5,  m_etree->get<float>("etaJ3"),       m_weight);
	m_histSvc->BookFillHist("mBBJ",   100, 0, 750,  m_etree->get<float>("mBBJ") / 1e3,  m_weight);
        m_histSvc->BookFillHist("pTBBJ",  600, 0, 3000, m_etree->get<float>("pTBBJ") / 1e3, m_weight);
      }
    }
  }
  return EL::StatusCode::SUCCESS;
} // fill_jetSelectedHistos

EL::StatusCode AnalysisReader_VHcc::compute_jetSelectedQuantities (const std::vector<const xAOD::Jet*> &selectedJets) {

  if (selectedJets.size() >= 1) (*m_etree)["j1Vec"] = selectedJets.at(0)->p4();
  if (selectedJets.size() >= 2) (*m_etree)["j2Vec"] = selectedJets.at(1)->p4();
  if (selectedJets.size() >= 3) (*m_etree)["j3Vec"] = selectedJets.at(2)->p4();

  int nJet = m_physicsMeta.nJets;
  int nSigJet = m_physicsMeta.nSigJet;
  int nFwdJet = m_physicsMeta.nForwardJet;
  int nbJet = m_physicsMeta.nTags;

  TLorentzVector j1Vec = m_etree->exists("j1Vec") ? (*m_etree)["j1Vec"] : TLorentzVector();
  TLorentzVector j2Vec = m_etree->exists("j2Vec") ? (*m_etree)["j2Vec"] : TLorentzVector();
  TLorentzVector j3Vec = m_etree->exists("j3Vec") ? (*m_etree)["j3Vec"] : TLorentzVector();

  //HVec and bbj should be built using the corrected jets (e.g. mu-in-jet)
  TLorentzVector HVec = m_etree->exists("HVec") ? (*m_etree)["HVec"] : TLorentzVector();
  TLorentzVector bbj  = m_etree->exists("bbjVec") ? (*m_etree)["bbjVec"] : TLorentzVector();

  m_etree->SetBranchAndValue<int>("nJets", nJet, -1);
  m_etree->SetBranchAndValue<int>("nSigJets", nSigJet, -1);
  m_etree->SetBranchAndValue<int>("nFwdJets", nFwdJet, -1);
  m_etree->SetBranchAndValue<int>("nbJets", nbJet, -1);
  m_etree->SetBranchAndValue<float>("dRBB", j1Vec.DeltaR(j2Vec), -99);
  m_etree->SetBranchAndValue<float>("dPhiBB", j1Vec.DeltaPhi(j2Vec), -99);
  m_etree->SetBranchAndValue<float>("dEtaBB", fabs(j1Vec.Eta() - j2Vec.Eta()), -99);
  m_etree->SetBranchAndValue<float>("mBB", HVec.M(), -99);
  m_etree->SetBranchAndValue<float>("pTBB", HVec.Pt(), -99); //using mu-in-jet-corr, is it what we want?
  m_etree->SetBranchAndValue<float>("pTB1", j1Vec.Pt(), -99);
  m_etree->SetBranchAndValue<float>("pTB2", j2Vec.Pt(), -99);
  m_etree->SetBranchAndValue<float>("etaB1", j1Vec.Eta(), -99);
  m_etree->SetBranchAndValue<float>("etaB2", j2Vec.Eta(), -99);
  if (selectedJets.size() >= 1) m_etree->SetBranchAndValue<float>("MV2c10B1", Props::MV2c10.get(selectedJets.at(0)), -99);
  if (selectedJets.size() >= 2) m_etree->SetBranchAndValue<float>("MV2c10B2", Props::MV2c10.get(selectedJets.at(1)), -99);

  if(m_isMC){ //those values get overwritten if event ends up in merged regime...
    m_etree->SetBranchAndValue<int>("flavB1", m_physicsMeta.b1Flav, -99);
    m_etree->SetBranchAndValue<int>("flavB2", m_physicsMeta.b2Flav, -99);
    m_etree->SetBranchAndValue<int>("flavJ3", m_physicsMeta.j3Flav, -99);
  }
  if (selectedJets.size() >= 3) {
    m_etree->SetBranchAndValue<float>("pTJ3", j3Vec.Pt(), -99);
    m_etree->SetBranchAndValue<float>("etaJ3", j3Vec.Eta(), -99);
    m_etree->SetBranchAndValue<float>("mBBJ", bbj.M(), -99);
    m_etree->SetBranchAndValue<float>("pTBBJ", bbj.Pt(), -99);
  }

  return EL::StatusCode::SUCCESS;
} // compute_jetSelectedQuantities


void AnalysisReader_VHcc::rescale_jets (double          mBB,
                                        TLorentzVector &j1Vec,
                                        TLorentzVector &j2Vec) {
  double mbb_weight = 125.0 / (mBB * 1e-3);

  j1Vec *= mbb_weight;
  j2Vec *= mbb_weight;
} // rescale_jets

void AnalysisReader_VHcc::rescale_leptons (double          mLL,
					   TLorentzVector &l1Vec,
					   TLorentzVector &l2Vec) {
  double mll_weight = 91.188 / (mLL * 1e-3);

  l1Vec *= mll_weight;
  l2Vec *= mll_weight;
} // rescale_leptons


// function to create the bit mask
unsigned long int AnalysisReader_VHcc::bitmask(const unsigned long int cut, const std::vector<unsigned long int> &excludeCuts) {

  unsigned long int mask = 0;
  unsigned long int bit  = 0;

  const unsigned long int offBit = 0;
  const unsigned long int onBit  = 1;


  for (unsigned long int i=0; i < cut+1; ++i) {
    if   ( std::find(excludeCuts.begin(), excludeCuts.end(), i) != excludeCuts.end() ) {
      // if a cut should be excluded set the corresponding bit to 0
      mask = mask | offBit << bit++;
    } else  {
      // otherwise set the bit to 1
      mask = mask | onBit << bit++;
    }
  }

  return mask;
}

// function to compare a bit flag against a bit mask
// Usage: given a flag, check if it passes all cuts up to and including "cut"
// excluding the cuts in "excludedCuts"
bool AnalysisReader_VHcc::passAllCutsUpTo(const unsigned long int flag, const unsigned long int cut, const std::vector<unsigned long int> &excludeCuts) {

  // Get the bitmask: we want to check all cuts up to "cut" excluding the cuts listed in excludeCuts
  unsigned long int mask = bitmask(cut, excludeCuts);
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;

}

// a function to check specifig cuts
bool AnalysisReader_VHcc::passSpecificCuts(const unsigned long int flag, const std::vector<unsigned long int> &cuts) {
  unsigned long int mask = 0;
  const unsigned long int onBit  = 1;

  // Make the bit mask
  for (auto cut : cuts) mask = mask | (onBit << cut);
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;
}


// a function to update the bit flag
void AnalysisReader_VHcc::updateFlag(unsigned long int &flag, const unsigned long int cutPosition, const unsigned long int passCut) {
  // Put bit passCut (true or false) at position cutPosition
  flag = flag | passCut << cutPosition;
}


void AnalysisReader_VHcc::computeHT(double &HTsmallR, double &HTlargeR,
				     const Lepton &l1, const Lepton &l2,
				     const std::vector<const xAOD::Jet*> &signalJets,
				     const std::vector<const xAOD::Jet*> &forwardJets,
				     const std::vector<const xAOD::Jet*> &fatJets){
  //leptons
    HTsmallR+=l1.vec.Pt()+l2.vec.Pt();
    HTlargeR+=l1.vec.Pt()+l2.vec.Pt();
    //forward jets
    for(unsigned int i = 0; i < forwardJets.size();i++){
      HTsmallR += forwardJets.at(i)->pt();
    }
    //signal jets
    for(unsigned int i = 0; i < signalJets.size();i++){
      HTsmallR += signalJets.at(i)->pt();
    }
    //fat jets
    for(unsigned int i = 0; i < fatJets.size();i++){
      HTlargeR += fatJets.at(i)->pt();
    }
}//computeHT

TLorentzVector AnalysisReader_VHcc::getMETCorrTLV(const xAOD::MissingET *met, std::vector<const TLorentzVector *> removeObjects, std::vector<const TLorentzVector *> addObjects) {
  TLorentzVector metVec;
  double met_x = met->mpx();
  double met_y = met->mpy();
  for (auto jet : removeObjects) {
     met_x += jet->Px();
     met_y += jet->Py();
  }
  for (auto jet : addObjects) {
     met_x -= jet->Px();
     met_y -= jet->Py();
  }
  double met_e = sqrt(met_x * met_x + met_y * met_y);
  metVec.SetPxPyPzE(met_x, met_y, 0, met_e);
  return metVec;
} // getMETCorrTLV

EL::StatusCode AnalysisReader_VHcc::setJetVariables(Jet& j1, Jet& j2, Jet& j3,
					  const std::vector<const xAOD::Jet*> &selectedJets){

  int nJet = selectedJets.size();

  const xAOD::Jet* jet1 = nullptr;
  const xAOD::Jet* jet2 = nullptr;
  const xAOD::Jet* jet3 = nullptr;

  if(nJet>=1) jet1 = selectedJets.at(0);
  if(nJet>=2) jet2 = selectedJets.at(1);
  if(nJet>=3) jet3 = selectedJets.at(2);

  if(nJet >= 1) j1.vec = selectedJets.at(0)->p4();
  if(nJet >= 2) j2.vec = selectedJets.at(1)->p4();
  if(nJet >= 3) j3.vec = selectedJets.at(2)->p4();

  if(nJet >= 1) j1.isTagged = BTagProps::isTagged.get(jet1);
  if(nJet >= 2) j2.isTagged = BTagProps::isTagged.get(jet2);
  if(nJet >= 3) j3.isTagged = BTagProps::isTagged.get(jet3);

  if(nJet >= 1) j1.isSL = (Props::nrMuonInJet.get(jet1) > 0);
  if(nJet >= 2) j2.isSL = (Props::nrMuonInJet.get(jet2) > 0);
  if(nJet >= 3) j3.isSL = (Props::nrMuonInJet.get(jet3) > 0);

  //mu-in-jet-corr
  if(nJet >= 1 && j1.isTagged){
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet1,j1.vec_onemu,false,"OneMu"));
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet1,j1.vec_corr,false,m_jetCorrType));
  }
  else {
    j1.vec_onemu  =  j1.vec;
    j1.vec_corr   =  j1.vec;
  }
  if(nJet >= 2 && j2.isTagged){
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet2,j2.vec_onemu,false,"OneMu"));
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet2,j2.vec_corr,false,m_jetCorrType));
  }
  else {
    j2.vec_onemu  =  j2.vec;
    j2.vec_corr   =  j2.vec;
  }
  if(nJet >= 3 && j3.isTagged){
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet3,j3.vec_onemu,false,"OneMu"));
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(jet3,j3.vec_corr,false,m_jetCorrType));
  }
  else {
    j3.vec_onemu  =  j3.vec;
    j3.vec_corr   =  j3.vec;
  }

  //mBB rescaling
  if(nJet >= 1) j1.vec_resc = j1.vec_corr; // will be set properly later
  if(nJet >= 2) j2.vec_resc = j2.vec_corr; // will be set properly later
  if(nJet >= 3) j3.vec_resc = j3.vec_corr; // won't be rescaled

  if(nJet >= 1) j1.vec_gsc = j1.vec;
  if(nJet >= 2) j2.vec_gsc = j2.vec;
  if(nJet >= 3) j3.vec_gsc = j3.vec;

  if(nJet >= 1) j1.vec_ptreco = j1.vec_corr;
  if(nJet >= 2) j2.vec_ptreco = j2.vec_corr;
  if(nJet >= 3) j3.vec_ptreco = j3.vec_corr;

  if(nJet >= 1) j1.vec_kf = j1.vec_corr;
  if(nJet >= 2) j2.vec_kf = j2.vec_corr;
  if(nJet >= 3) j3.vec_kf = j3.vec_corr;

  if(m_doResolution){
    if(nJet >= 1) j1.resolution  = Props::resolution.get(jet1);
    if(nJet >= 2) j2.resolution  = Props::resolution.get(jet2);
    if(nJet >= 3) j3.resolution  = Props::resolution.get(jet3);
  } else{
    if(nJet >= 1) j1.resolution  = 0.02751455+0.85052821/sqrt(j1.vec.E()*0.001);
    if(nJet >= 2) j2.resolution  = 0.02751455+0.85052821/sqrt(j2.vec.E()*0.001);
    if(nJet >= 3) j3.resolution  = 0.02751455+0.85052821/sqrt(j3.vec.E()*0.001);
  }

  return EL::StatusCode::SUCCESS;

}//setJetVariables

EL::StatusCode AnalysisReader_VHcc::setFatJetVariables(Jet& j1, Jet& j2, Jet& j3,
					     const std::vector<const xAOD::Jet*> &fatJets){

  int nJet = fatJets.size();

  const xAOD::Jet* jet1 = nullptr;
  const xAOD::Jet* jet2 = nullptr;
  const xAOD::Jet* jet3 = nullptr;

  if(nJet>=1) jet1 = fatJets.at(0);
  if(nJet>=2) jet2 = fatJets.at(1);
  if(nJet>=3) jet3 = fatJets.at(2);

  if(nJet >= 1) j1.vec = fatJets.at(0)->p4();
  if(nJet >= 2) j2.vec = fatJets.at(1)->p4();
  if(nJet >= 3) j3.vec = fatJets.at(2)->p4();

  if(nJet >= 1) j1.nBTags = Props::nBTagsAll.get(jet1);
  if(nJet >= 2) j2.nBTags = Props::nBTagsAll.get(jet2);
  if(nJet >= 3) j3.nBTags = Props::nBTagsAll.get(jet3);

  //mu-in-jet-corr
  if(nJet >= 1 && j1.nBTags > 0){
    EL_CHECK("AnalysisReader_VHcc::setFatJetVariables() ",getBJetEnergyCorrTLV(jet1,j1.vec_corr,true,m_fatJetCorrType));
  }
  else j1.vec_corr  =  j1.vec;
  if(nJet >= 2 && j2.nBTags > 0){
    EL_CHECK("AnalysisReader_VHcc::setFatJetVariables() ",getBJetEnergyCorrTLV(jet2,j2.vec_corr,true,m_fatJetCorrType));
  }
  else j2.vec_corr  =  j2.vec;
  if(nJet >= 3 && j3.nBTags > 0){
    EL_CHECK("AnalysisReader_VHcc::setFatJetVariables() ",getBJetEnergyCorrTLV(jet3,j3.vec_corr,true,m_fatJetCorrType));
  }
  else j3.vec_corr  =  j3.vec;

  //combMass begin

  if ( m_doCombMass ) {
    if ( m_CxAODTag=="CxAODTag26" ) {
      // XbbMu is done for Calo and TA before Combined so please just use Combined
      if(nJet >= 1) j1.vec_corr = j1.vec;
      if(nJet >= 2) j2.vec_corr = j2.vec;
      if(nJet >= 3) j3.vec_corr = j3.vec;
    }
    if ( m_CxAODTag=="CxAODTag28" ) {
      //xbb correction vector is saved so please do getBJetEnergyCorrTLV as usual
    }
  }

  //combMass end

  //mBB rescaling
  if(nJet >= 1) j1.vec_resc = j1.vec_corr; // will be set properly later
  if(nJet >= 2) j2.vec_resc = j2.vec_corr; // won't be rescaled
  if(nJet >= 3) j3.vec_resc = j3.vec_corr; // won't be rescaled

  if(nJet >= 1) j1.vec_gsc = j1.vec;
  if(nJet >= 2) j2.vec_gsc = j2.vec;
  if(nJet >= 3) j3.vec_gsc = j3.vec;

  if(nJet >= 1) j1.vec_onemu = j1.vec_corr;
  if(nJet >= 2) j2.vec_onemu = j2.vec_corr;
  if(nJet >= 3) j3.vec_onemu = j3.vec_corr;

  if(nJet >= 1) j1.vec_ptreco = j1.vec_corr;
  if(nJet >= 2) j2.vec_ptreco = j2.vec_corr;
  if(nJet >= 3) j3.vec_ptreco = j3.vec_corr;

  if(nJet >= 1) j1.vec_kf = j1.vec_corr;
  if(nJet >= 2) j2.vec_kf = j2.vec_corr;
  if(nJet >= 3) j3.vec_kf = j3.vec_corr;

  if(nJet >= 1) j1.isSL = false;
  if(nJet >= 2) j2.isSL = false;
  if(nJet >= 3) j3.isSL = false;

  if(nJet >= 1) j1.resolution  = 0.10;
  if(nJet >= 2) j2.resolution  = 0.10;
  if(nJet >= 3) j3.resolution  = 0.10;

  return EL::StatusCode::SUCCESS;

}//setFatJetVariables


void AnalysisReader_VHcc::setHiggsCandidate(Higgs& resolvedH, Higgs& mergedH, Jet j1, Jet j2, Jet fj1){

  //resolved
  resolvedH.vec       = j1.vec+j2.vec;
  resolvedH.vec_corr  = j1.vec_corr+j2.vec_corr;

  resolvedH.vec_gsc   = j1.vec_gsc+j2.vec_gsc;
  resolvedH.vec_onemu = j1.vec_onemu+j2.vec_onemu;
  resolvedH.vec_ptreco = j1.vec_ptreco+j2.vec_ptreco;
  resolvedH.vec_kf    = j1.vec_kf+j2.vec_kf;

  //merged
  mergedH.vec       = fj1.vec;
  mergedH.vec_corr  = fj1.vec_corr;

  mergedH.vec_gsc = fj1.vec_gsc;
  mergedH.vec_onemu = fj1.vec_onemu;
  mergedH.vec_ptreco = fj1.vec_ptreco;
  mergedH.vec_kf    = fj1.vec_kf;

}//setHiggsCandidate


TLorentzVector AnalysisReader_VHcc::defineMinDRLepBSyst(const Lepton &l1, const Lepton &l2, const Jet &j1, const Jet &j2){
  float minDR = 10000;
  TLorentzVector lbVec;
  if(j1.vec.DeltaR(l1.vec) < minDR){
    minDR = j1.vec.DeltaR(l1.vec);
    lbVec = j1.vec+l1.vec;
  }
  if(j1.vec.DeltaR(l2.vec) < minDR){
    minDR = j1.vec.DeltaR(l2.vec);
    lbVec = j1.vec+l2.vec;
  }
  if ( m_physicsMeta.regime == PhysicsMetadata::Regime::resolved ) {
    if(j2.vec.DeltaR(l1.vec) < minDR){
      minDR = j2.vec.DeltaR(l1.vec);
      lbVec = j2.vec+l1.vec;
    }
    if(j2.vec.DeltaR(l2.vec) < minDR){
      minDR = j2.vec.DeltaR(l2.vec);
      lbVec = j2.vec+l2.vec;
    }
  }
  return lbVec;
}

bool AnalysisReader_VHcc::checkTightLeptons(const Lepton &l1, const Lepton &l2){

  // Check that both leptons are above 25 GeV
  if ( l1.vec.Pt()/1.e3 < 25. || l2.vec.Pt()/1.e3 < 25. ) return false;

  // Check that at least one of them is ZHTight & has pt > 60
  if ( !( (l1.isZHTight && l1.vec.Pt()/1.e3 > 60.) ||
          (l2.isZHTight && l2.vec.Pt()/1.e3 > 60.) )   ) return false;

  return true;
}

EL::StatusCode AnalysisReader_VHcc::fill_jetHistos (std::vector<const xAOD::Jet*> signalJets, std::vector<const xAOD::Jet*> forwardJets) {

  if (!m_doReduceFillHistos) {
    m_histSvc->BookFillHist("NJets", 11, -0.5, 10.5, m_etree->get<int>("nJets"), m_weight);
    m_histSvc->BookFillHist("NSigJets", 11, -0.5, 10.5, m_etree->get<int>("nSigJets"), m_weight);
    m_histSvc->BookFillHist("NbJets", 11, -0.5, 10.5, m_etree->get<int>("nbJets"), m_weight);


    m_histSvc->BookFillHist("NFwdJets", 11, -0.5, 10.5, m_etree->get<int>("nFwdJets"), m_weight);
    for (const xAOD::Jet *jet : signalJets) {
      m_histSvc->BookFillHist("PtJets", 100,  0, 100, jet->pt() / 1e3, m_weight);
      m_histSvc->BookFillHist("EtaJets", 100, -5,   5, jet->eta(),    m_weight);
    }

    for (const xAOD::Jet *jet : forwardJets) {
      m_histSvc->BookFillHist("PtJets", 100,  0, 100, jet->pt() / 1e3, m_weight);
      m_histSvc->BookFillHist("EtaJets", 100, -5,   5, jet->eta(),    m_weight);
    }
  }

  return EL::StatusCode::SUCCESS;
} // fill_jetHistos


bool AnalysisReader_VHcc::isBlindedRegion(const unsigned long int eventFlag, bool isMerged){

return false; // Joe: this will definitely have to be fixed at some point, but DiLeptonCuts is undefined since it comes from VHbb2Lep

/*
  return (!m_isMC                                                 //only data
	  && (m_histNameSvc->get_description() == "SR")           //only SR
	  && ((m_histNameSvc->get_nTag() == 2)                    // 2 tag
	      || ((m_histNameSvc->get_nTag() == 1) && isMerged))  //also 1 tag in merged regime
	  && ( (passSpecificCuts(eventFlag, {DiLeptonCuts::mHCorrResolved}) && !isMerged) ||
	       (passSpecificCuts(eventFlag, {DiLeptonCuts::mHCorrMerged}) && isMerged) )            //mBB window
	  );
*/

}//isBlindedRegion

double AnalysisReader_VHcc::getSMSigVPTSlice(){
   bool isSMSig = (
         // qqZllH125
	 m_mcChannel == 341102 ||
	 // ggZllH125
	 m_mcChannel == 341094 ||
	 m_mcChannel == 341095 ||
	 m_mcChannel == 341096 ||
	 // qqZvvH125
	 m_mcChannel == 341101 ||
	 // ggZvvH125
	 m_mcChannel == 341097 ||
	 m_mcChannel == 341098 ||
	 m_mcChannel == 341099 ||
	 // qqWlvH125
	 m_mcChannel == 341100);
   double truthVpT=-1;
   if (!isSMSig) return truthVpT;

   for (const xAOD::TruthParticle *part : *m_truthParts) {
      if( part->status()==62 ){
      if(fabs(part->pdgId())==23 || fabs(part->pdgId())==24 ) // Z or W
	 truthVpT = TMath::Sqrt(part->px()*part->px() + part->py()*part->py());
      }
   }
   return truthVpT;
}

// for extract HTXS information
std::string AnalysisReader_VHcc::TranslateStage1Cat (int PF){
   // qq -> WH
   if(PF == 300) return "QQ2HLNU_FWDH";
   if(PF == 301) return "QQ2HLNU_PTV_0_150";
   if(PF == 302) return "QQ2HLNU_PTV_150_250_0J";
   if(PF == 303) return "QQ2HLNU_PTV_150_250_GE1J";
   if(PF == 304) return "QQ2HLNU_PTV_GT250";
   // qq -> ZH
   if(PF == 400) return "QQ2HLL_FWDH";
   if(PF == 401) return "QQ2HLL_PTV_0_150";
   if(PF == 402) return "QQ2HLL_PTV_150_250_0J";
   if(PF == 403) return "QQ2HLL_PTV_150_250_GE1J";
   if(PF == 404) return "QQ2HLL_PTV_GT250";
   // gg -> ZH
   if(PF == 500) return "GG2HLL_FWDH";
   if(PF == 501) return "GG2HLL_PTV_0_150";
   if(PF == 502) return "GG2HLL_PTV_GT150_0J";
   if(PF == 503) return "GG2HLL_PTV_GT150_GE1J";

   return "UNKNOWN";
}


// for 0lep boosted analysis
void AnalysisReader_VHcc::rescale_fatjet(double mBB,
				       TLorentzVector& fatJet) {
  double mbb_weight = 125.0 / (mBB*1e-3);
  fatJet *= mbb_weight;

}
EL::StatusCode AnalysisReader_VHcc::histFinalize () {
   EL_CHECK("finalize()", AnalysisReader::histFinalize());

   Info("histFinalize()", "About to write histograms.");
   m_histFastSvc.Write(wk());
   Info("histFinalize()", "Done writing histograms.");

  return EL::StatusCode::SUCCESS;
}

//Setup the CorrsAndSysts object m_corrsAndSyst for the current event
EL::StatusCode AnalysisReader_VHcc::SetupCorrsAndSyst(std::string currentVar, bool isCR = false){

  //Determine from m_histNameSvc->getFullHistName() th sample {ttbar, V+b,...} and number of tags {0tag, 1tag,...}
  //std::string EventClass = m_histNameSvc->getFullHistName("mVH"); //Leave entry blank as we only want sample & tag categorisation
  std::string EventClass = m_histNameSvc->get_sample();
  CAS::SysVar VarUp = CAS::Up;
  CAS::SysBin Bin = CAS::Any;

  if( m_histNameSvc -> get_sample() == "ttbar" ||  m_histNameSvc -> get_sample().find("stop") != std::string::npos ){
    m_evtType          = m_corrsAndSysts->GetEventType(m_histNameSvc -> get_sample());
    //Allocate a enum as to whether the event is a CR or SR.
    //Tehnically this is only needed for the 2-lepton channel under the new HVT/AZH PRSR analysis systematics
    if( isCR ){

      //New PRSR systematics sets enum to TopCR if 2-lepton channel event is a e-mu event.
      //Old HVT moriond 2016 nalysis will set TopCR if nAddBTags > 0 or e-mu event
      m_detailevtType    = m_corrsAndSysts->GetDetailEventType("TopCR");
      m_corrsAndSysts->GetSystFromName((TString)currentVar, m_Systematic, Bin, VarUp);

    }else if( !isCR ){

      //Otherwise ttbar event is a TopSR. May not be entirely true, but the price you pay for a
      //generalising code across 3 channels.
      //But be assured that the CorrsAndSysts package will assign the correct weight
      m_detailevtType    = m_corrsAndSysts->GetDetailEventType("TopSR");
      m_corrsAndSysts->GetSystFromName((TString)currentVar, m_Systematic, Bin, VarUp);

    }else{
      Info("SetupCorrsAndSysts()", "Invalid ttbar CR definition for TTbar and Stop systematic");
      return EL::StatusCode::FAILURE;
    }

  }else if( EventClass == "W" || EventClass == "Z" || EventClass == "Wv22" || EventClass == "Zv22"){

    if (EventClass == "Wv22") EventClass = "W";
    if (EventClass == "Zv22") EventClass = "Z";

    //Set event type, systematic name and void detaileventType.
    //SR vs CR determined by mBB
    if(          m_physicsMeta.b1Flav == 5 && m_physicsMeta.b2Flav == 5 )      EventClass += "b"; //bb
    else if (   (m_physicsMeta.b1Flav == 5 && m_physicsMeta.b2Flav == 4)
		|| (m_physicsMeta.b1Flav == 4 && m_physicsMeta.b2Flav == 5))   EventClass += "b"; //bc
    else if (    m_physicsMeta.b1Flav == 4 && m_physicsMeta.b2Flav == 4 )      EventClass += "c";    //cc
    else if (    m_physicsMeta.b1Flav == 5 || m_physicsMeta.b2Flav == 5 )      EventClass += "b";    //bl
    else if (    m_physicsMeta.b1Flav == 4 || m_physicsMeta.b2Flav == 4 )      EventClass += "c";    //cl
    else                                                                       EventClass += "l";    //ll
    if(m_debug) std::cout << " Event Class = " << EventClass << std::endl;
    m_evtType          = m_corrsAndSysts->GetEventType(EventClass);
    m_detailevtType    = CAS::NODETAILNAME;
    m_corrsAndSysts->GetSystFromName((TString)currentVar, m_Systematic, Bin, VarUp);

  }else{
    Info("AnalysisReader_VHcc1Lep::SetupCorrsAndSysts()", "Invalid sample, no systematics applied.");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;

}//End of SetupCorrsAndSysts()

// mVH is in GeV
void AnalysisReader_VHcc::fillVjetsSystslep(double Mbb, float mVH) {

  //Scale factor (weight)
  float weight_MG5_She = 0.1;
  if(m_debug) std::cout << "<fillVjetsSystslep()>    Adding MODEL___VJets systematics" << std::endl;

  //Load the correct systematic
  if( m_analysisType == "0lep" ){
    SetupCorrsAndSyst("SysVJets_0lep_shape_mVH");
    weight_MG5_She = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, 0, 0, 0, 0, 0, 0, 0, 0, m_detailevtType, m_Systematic, CAS::Up, CAS::Any);
  }else if( m_analysisType == "1lep" ){
    SetupCorrsAndSyst("SysVJets_1lep_shape_mVH");
    weight_MG5_She = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, 0, 0, 0, 0, 0, 0, 0, 0, m_detailevtType, m_Systematic, CAS::Up, CAS::Any);
  }else if( m_analysisType == "2lep" ){
    SetupCorrsAndSyst("SysVJets_2lep_shape_mVH");
    weight_MG5_She = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, 0, 0, 0, 0, 0, 0, 0, 0, m_detailevtType, m_Systematic, CAS::Up, CAS::Any);
  }

  //Get_Systematics() return scale+1
  weight_MG5_She = weight_MG5_She - 1;
  //if(m_debug) std::cout << "<fillVjetsSystslep()>    weight_M5G5_She : " << weight_MG5_She << std::endl;
  float min = 0.1;
  if (weight_MG5_She < min) weight_MG5_She = min;
  m_weightSysts.push_back({ "MODEL_Vjets_MadGraph__1up", weight_MG5_She });
}

//mVH in GeV
void AnalysisReader_VHcc::fillTTbarSystslep(int nTag, bool isCR, float mVH) {

  std::vector<std::string> variations;
  variations.push_back("Pow_aMC");
  variations.push_back("Her_Pyt");
  variations.push_back("rHi_rLo");

  //Determine from m_histNameSvc->getFullHistName() th sample {ttbar, V+b,...} and number of tags {0tag, 1tag,...}
  std::string EventClass = m_histNameSvc->getFullHistName(""); //Leave entry blank as we only want sample & tag categorisation
  std::string channel;
  if( m_analysisType == "0lep" ) { channel = "_0lep";}
  else if( m_analysisType == "1lep" ) { channel = "_1lep"; }
  else if( m_analysisType == "2lep" ) { channel = "_2lep"; }

  float weight = 0;
  //Scale factor (weight)
  //Load the correct systematic
  for(std::string shapeComp : variations)
    {
      SetupCorrsAndSyst("SysTTbar_shape_mVH_"+shapeComp+channel, isCR);
      //Subract 1 to account for relative ratio
      weight = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, 0, 0, 0, 0, 0, 0, 0, 0, nTag, m_detailevtType, m_Systematic, CAS::Up, CAS::Any) - 1;
      float min = 0.1;
      // cut rad in half
      //    if ( shapeComp == "rHi_rLo" ) weight = 0.5 * (weight - 1) + 1;
      if (weight < min) weight = min;

      if ( shapeComp == "Pow_aMC" ) { m_weightSysts.push_back({ "MODEL_TTbar_aMcAtNlo__1up", weight }); }
      else if (shapeComp == "Her_Pyt" ) { m_weightSysts.push_back({ "MODEL_TTbar_Herwig__1up", weight }); }
      else if ( shapeComp == "rHi_rLo" ) {
	m_weightSysts.push_back({ "MODEL_TTbar_rad__1up", (1+((weight-1)/(weight+1))) });
	m_weightSysts.push_back({ "MODEL_TTbar_rad__1down", (1-((weight-1)/(weight+1))) });
      }
    }
}// End of fillTTbarSystslep

//Fills m_weightSysts vector with a series of modelling systematics variations:
//      Powheg+Herwig++ Vs Powheg+Pythia6
//      aMC@NLO+Herwig++ Vs Powheg+Herwig++
//      Powheg+Pythia6 radHi & radLo
//mVH in GeV
void AnalysisReader_VHcc::fillTopSystslep_PRSR(std::string Regime, int nAddTag, float mBB, float mVH, bool isEMu) {

  bool isTTbar = m_histNameSvc -> get_sample() == "ttbar";
  bool isStop =  m_histNameSvc -> get_sample().find("stop") != std::string::npos;

  //Vector that stores the names of the relevant TTbar variations
  std::vector<std::string> variations;
  variations.push_back("Pow_aMC");
  variations.push_back("Her_Pyt");
  variations.push_back("rHi");
  variations.push_back("rLo");
  if ( m_histNameSvc -> get_sample().find("stop") != std::string::npos )
    variations.push_back("DS");

  //Determine from m_histNameSvc->getFullHistName() th sample {ttbar, V+b,...} and number of tags {0tag, 1tag,...}
  std::string EventClass = m_histNameSvc->getFullHistName(""); //Leave entry blank as we only want sample & tag categorisation

  std::string channel;
  if( m_analysisType == "0lep" ) { channel = "_0lep";}
  else if( m_analysisType == "1lep" ) { channel = "_1lep"; }
  else if( m_analysisType == "2lep" ) { channel = "_2lep"; }

  // single top systematics only for 1-lepton channel
  if (isStop && m_analysisType != "1lep")
    return;

  float weight = 0;
  //Scale factor (weight)

  //CAS::SysBin SysFormat = m_doBinnedSyst ? CAS::Bin0 : CAS::Any;
  float binned = m_doBinnedSyst ? 1 : 0;

  //Load the correct systematic
  for(std::string shapeComp : variations)
    {
      if ( isTTbar )
	SetupCorrsAndSyst("SysTTbar_" + Regime + "_shape_mVH_"+shapeComp+channel, isEMu);
      else if ( isStop )
	SetupCorrsAndSyst("SysStop_" + Regime + "_shape_mVH_"+shapeComp+channel, isEMu);

      //std::cout << " m_currentVar : " << "SysTTbar" + Regime + "_shape_mVH_"+shapeComp+channel << std::endl;
      //Subract 1 to account for relative ratio
      weight = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, mBB, binned, 0, 0, 0, 0, 0, 0, nAddTag, m_detailevtType, m_Systematic, CAS::Up, CAS::Any) - 1;
      float min = 0.1;
      //std::cout << " Variation : " << shapeComp << std::endl;
      //std::cout << " Weight : " << weight << std::endl;
      if (weight < min) weight = min;

      if ( shapeComp == "Pow_aMC" )     {
	if ( isTTbar )
	  m_weightSysts.push_back({ "MODEL_TTbar_aMcAtNlo__1up", weight });
	else if ( isStop )
	  m_weightSysts.push_back({ "MODEL_Stop_aMcAtNlo__1up", weight });
      }
      else if (shapeComp == "Her_Pyt" ) {
	if ( isTTbar )
	  m_weightSysts.push_back({ "MODEL_TTbar_Herwig__1up", weight });
	else if ( isStop )
	  m_weightSysts.push_back({ "MODEL_Stop_Herwig__1up", weight });
      }
      else if (shapeComp == "rHi" )     {
	if ( isTTbar )
	  m_weightSysts.push_back({ "MODEL_TTbar_rad__1up", weight });
	else if ( isStop )
	  m_weightSysts.push_back({ "MODEL_Stop_rad__1up", weight });
      }
      else if (shapeComp == "rLo" )     {
	if ( isTTbar )
	  m_weightSysts.push_back({ "MODEL_TTbar_rad__1down", weight });
	else if ( isStop )
	  m_weightSysts.push_back({ "MODEL_Stop_rad__1down", weight });
      }
      else if (shapeComp == "DS" )     {
	if ( m_histNameSvc -> get_sample().find("stop") != std::string::npos )
	  m_weightSysts.push_back({ "MODEL_Stop_DS__1up", weight });
      }

    }
}// End of fillTTbarSystslep

// mVH is in GeV
void AnalysisReader_VHcc::fillVjetsSystslep_PRSR(std::string Regime, double Mbb, float mVH, int nAddBTags) {

  //Scale factor (weight)
  float weight_VJets = 0.1;
  if(m_debug) std::cout << "<fillVjetsSystslep()>    Adding modelling V+jets systematics" << std::endl;

  //Read the event flavour and form the string to assign the correct CAS::SysWxy_1lep_<regime>_shape_mVH
  //enumerator
  if(m_debug){
    std::cout << "  ~~~~~~~~~~~~~~~  Regime: " << Regime << "  ~~~~~~~~~~~~~~~ " << std::endl;
    std::cout << " >> Jet 1 flavour = " << m_physicsMeta.b1Flav << std::endl;
    std::cout << " >> Jet 2 flavour = " << m_physicsMeta.b2Flav << std::endl;
  }
  std::string Flavour = "";
  std::string FlavCategory = "";
  if(          m_physicsMeta.b1Flav == 5 && m_physicsMeta.b2Flav == 5 ){
    Flavour = "bb";
    FlavCategory = "HF";
  }
  else if (   (m_physicsMeta.b1Flav == 5 && m_physicsMeta.b2Flav == 4)
	      || (m_physicsMeta.b1Flav == 4 && m_physicsMeta.b2Flav == 5)){
    Flavour = "bc";
    FlavCategory = "HF";
  }else if (    m_physicsMeta.b1Flav == 4 && m_physicsMeta.b2Flav == 4 ){
    Flavour = "cc";
    FlavCategory = "HF";
  }else if (    m_physicsMeta.b1Flav == 5 || m_physicsMeta.b2Flav == 5 ){
    Flavour = "bl";
    FlavCategory = "hl";
  }else if (    m_physicsMeta.b1Flav == 4 || m_physicsMeta.b2Flav == 4 ){
    Flavour = "cl";
    FlavCategory = "hl";
  }else{
    Flavour = "l";
    FlavCategory = "l";
  }
  if(m_debug) std::cout << " Event Flavour = " << Flavour << std::endl;


  //Get Sample prefix:
  std::string SamplePrefix = m_histNameSvc->get_sample();
  //Remove "v22" if present
  if( SamplePrefix.find("v22") != std::string::npos){
    SamplePrefix.erase(SamplePrefix.find("v22"), 3);
  }

  //Form the systematic string key:
  std::string SysKey = "Sys" + SamplePrefix + Flavour + "_" + m_analysisType + "_" + Regime + "_shape_mVH";
  if(m_debug) std::cout << " >> SysKey : " << SysKey << std::endl;
  if(m_debug) std::cout << " >> mVH : " << mVH << std::endl;
  if(m_debug) std::cout << " >> mBB : " << Mbb << std::endl;

  //CAS::SysBin SysFormat = m_doBinnedSyst ? CAS::Bin0 : CAS::Any;
  float SherpaSyst = m_doSherpaSyst ? 2 : 0;
  float binned = m_doBinnedSyst ? 1 : 0;

  //Load the correct systematic
  SetupCorrsAndSyst(SysKey);

  //########### Two options are available: #######################
  //############ 1) Sherpa-Vs-MG5 #########
  weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, binned, 0, 0, 0, 0, 0, 0, nAddBTags, m_detailevtType, m_Systematic, CAS::Up, CAS::Any, m_debug);

  //Check on the analysisType & event process
  //NOTE:::      1-lepton Channel -->>  If Z-boson process then set the weight to 2. This way the loaded scale factor = 1
  //             2-lepton Channel -->>  If W-boson process then set the weight to 2. This way the loaded scale factor = 1
  if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
    weight_VJets = 2;
  }


  //Get_Systematics() return scale+1
  weight_VJets = weight_VJets - 1;
  if(m_debug) std::cout << "<fillVjetsSystslep_PRSR()>    weight_M5G5_She : " << weight_VJets << std::endl;
  float min = 0.1;
  if (weight_VJets < min) weight_VJets = min;
  std::string SystName = "MODEL_V" + FlavCategory + "Jets_MadGraph__1up";
  std::string SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_MadGraph__1up";
  m_weightSysts.push_back({ SystName, weight_VJets });
  m_weightSysts.push_back({ SystName_ProcSpec, weight_VJets });
  //##############################################################

  //############### 2) Sherpa Scale Variations ###################
  if(m_doSherpaSyst){
    //Renormalisation '__1up'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::Renorm, m_Systematic, CAS::Up, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    //NOTE:::      1-lepton Channel -->>  If Z-boson process then set the weight to 2. This way the loaded scale factor = 1
    //             2-lepton Channel -->>  If W-boson process then set the weight to 2. This way the loaded scale factor = 1
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min; //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaRenorm__1up";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaRenorm__1up";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );

    //Renormalisation '__1down'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::Renorm, m_Systematic, CAS::Do, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min;
    //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaRenorm__1down";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaRenorm__1down";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );

    //Factorisation '__1up'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::Fac, m_Systematic, CAS::Up, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min;
    //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaFac__1up";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaFac__1up";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );

    //Factorisation '__1down'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::Fac, m_Systematic, CAS::Do, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min;
    //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaFac__1down";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaFac__1down";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );

    //AlphaPDF '__1up'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::AlphaPDF, m_Systematic, CAS::Up, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min;
    //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaAlphaPDF__1up";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaAlphaPDF__1up";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );

    //AlphaPDF '__1down'
    weight_VJets = 0.1;
    weight_VJets = m_corrsAndSysts->Get_SystematicWeight(m_evtType, mVH, Mbb, SherpaSyst, 0, 0, 0, 0, 0, 0, nAddBTags, CAS::AlphaPDF, m_Systematic, CAS::Do, CAS::Any, m_debug) - 1;
    //Check on the analysisType & event process
    if( (SamplePrefix == "W" && m_analysisType == "2lep") || (SamplePrefix == "Z" && m_analysisType == "1lep") ){
      weight_VJets = 1;
    }
    //Check for maximum
    if( weight_VJets < min) weight_VJets = min;
    //Compose the histogram name
    SystName = "MODEL_V" + FlavCategory + "Jets_SherpaAlphaPDF__1down";
    SystName_ProcSpec = "MODEL_" + SamplePrefix + FlavCategory + "Jets_SherpaAlphaPDF__1down";
    m_weightSysts.push_back( {SystName, weight_VJets} );
    m_weightSysts.push_back( {SystName_ProcSpec, weight_VJets} );
  }
  //##############################################################

}


bool AnalysisReader_VHcc::removeUnwantedLB ()
{

   // temporary method to fix the problem in the GRL
   // need to remove some LB
   // if function returns "true", event is rejected

   if ( m_eventInfo->runNumber() != 303079 || m_isMC )
      return false;

   if ( (m_eventInfo->lumiBlock() >= 200 && m_eventInfo->lumiBlock() <= 201)
     || (m_eventInfo->lumiBlock() >= 250 && m_eventInfo->lumiBlock() <= 251)
     || (m_eventInfo->lumiBlock() >= 285 && m_eventInfo->lumiBlock() <= 324)
     || (m_eventInfo->lumiBlock() >= 336 && m_eventInfo->lumiBlock() <= 404)
     || (m_eventInfo->lumiBlock() >= 407 && m_eventInfo->lumiBlock() <= 438) )
      return false;
   else
      return true;
}


//Void function that applies Sherpa 2.2.1 scale variations as a weighted systematic variation histogram
void AnalysisReader_VHcc::applySherpaVJet_EvtWeights(){

  //Check that the current variations isNominal
  if( m_currentVar != "Nominal" ) return;

  //Skip if not MC
  if( !Props::isMC.get(m_eventInfo) ) return;

  //Need to know sample DSID
  int DSID = m_eventInfo->mcChannelNumber();

  //If DSID matches Sherpa 2.2.1 sample then proceed with systematic weight application
  if( m_EvtWeightVar->isSherpaVJets221( DSID ) || m_EvtWeightVar->isSherpaZmumu221( DSID ) ){

    //---------------------------------------------------------
    //Check that the variations stored within csVariations vector, are present within the systematic variations stored within the event weight container
    for ( auto& csVar : m_csVariations ){

      //Check that the EvtWeightVariations object contains a relevant systematic
      if( m_EvtWeightVar->hasVariation(csVar) ){

	//Extract the alternative weight
	float AltWeight = m_EvtWeightVar->getWeightVariation(m_eventInfo, csVar);

	//HistSvc.cxx applies m_weightSystsVar entries as a multiplicative factor
	//Therefore insert ratio of AltWeight/NomWeight (Alternative MC weight/Nominal MC weight)
	m_weightSysts.push_back( {csVar + "__1up", AltWeight/Props::MCEventWeight.get(m_eventInfo)} );

      }   // Logic End --->>> Cross-check valid Sherpa scale variation with csVariations vector

    }    // Loop End --->>> csVariations config vector search end
    //---------------------------------------------------------

  }else{
    //No need to apply weight leave member function
    return;
  }   // Logic End --->>> Check for valid Sherpa V+Jet DSID number

}//applySherpaVJet_EvtWeights()


void AnalysisReader_VHcc::applySignal_EvtWeights(){

  //Check that the current variations isNominal
  if( m_currentVar != "Nominal" ) return;

  //Skip if not MC
  if( !Props::isMC.get(m_eventInfo) ) return;

  //Need to know sample DSID
  int DSID = m_eventInfo->mcChannelNumber();

  //If DSID matches MiNLO sample then proceed with systematic weight application
  if( !m_EvtWeightVar->isSignalMiNLO( DSID ) ) return;

  //---------------------------------------------------------
  //I am not going to hardcode 100 PDF variations in a config file .... once you call this method ... all the sys will be written
  auto sigVar=m_EvtWeightVar->getListOfVariations();
  for ( auto& csVar : sigVar ){

    //Check that the EvtWeightVariations object contains a relevant systematic
    if( m_EvtWeightVar->hasVariationSignal(csVar) ){

      //Extract the alternative weight
      float AltWeight = m_EvtWeightVar->getWeightVariation(m_eventInfo, csVar);

      //HistSvc.cxx applies m_weightSystsVar entries as a multiplicative factor
      //Therefore insert ratio of AltWeight/NomWeight (Alternative MC weight/Nominal MC weight)
      m_weightSysts.push_back( {csVar + "__1up", AltWeight/Props::MCEventWeight.get(m_eventInfo)} );

    }   // Logic End --->>> Cross-check valid Sherpa scale variation with csVariations vector

  }    // Loop End --->>> csVariations config vector search end
  //---------------------------------------------------------

}//applySignal_EvtWeights()

int AnalysisReader_VHcc::getTtbarDecayType(){

  //only for ttbar samples
  if(m_histNameSvc -> get_sample().find("ttbar") != std::string::npos){

    int ntr_el = 0;
    int ntr_mu = 0;
    int ntr_tau = 0;
    const xAOD::TruthParticleContainer *truthParts = m_truthParticleReader->getObjects("Nominal");

    for (const xAOD::TruthParticle *part : *truthParts) {
      if (part->status() != 3) continue;
      if (abs(part->pdgId()) == 11) ntr_el++;
      if (abs(part->pdgId()) == 13) ntr_mu++;
      if (abs(part->pdgId()) == 15) ntr_tau++;
    }

    if(ntr_tau==0){
      if(ntr_el+ntr_mu == 2) return 0; //ee or mumu (2 lep)
      if(ntr_el+ntr_mu == 1) return 1; // e / mu (1 lep)
    }
    else if(ntr_tau==1){
      if(ntr_el+ntr_mu == 0) return 2; //tau (0 or 1 lep)
      if(ntr_el+ntr_mu == 1) return 3; //e/mu + tau (1 or 2 lep)
    }
    else if(ntr_tau==2) return 4;      //tautau (0, 1 or 2 lep)
    else return 5;                     //0 lep
  }

  return -1;

}//getTtbarDecayType()


//combMass begin

EL::StatusCode AnalysisReader_VHcc::combMassTmp1 (bool& isCombMassSyst, std::string& varName){

  if ( m_CxAODTag=="CxAODTag26" ) {
    //all used fatjet syst was reculculated
    if ( std::find(m_combMassSyst.begin(), m_combMassSyst.end(), varName) != m_combMassSyst.end() ) {
      varName = "Nominal";
      isCombMassSyst = true;
    }
    else if (varName.compare("FATJET_JER__1up") == 0 || varName.compare("FATJET_JMR__1up") == 0 ) {
      varName = "Nominal";
    }
  } //Tag26

  if ( m_CxAODTag=="CxAODTag28" ) {
    //combined mass is done in the maker and only JER was decided to reculculate
    if ( varName.compare("FATJET_JER__1up") == 0 ) {
      varName = "Nominal";
    }
  } //Tag28

  return EL::StatusCode::SUCCESS;
} // combMassTmp1

EL::StatusCode AnalysisReader_VHcc::combMassTmp2 (bool& isCombMassSyst){

  xAOD::TStore * store = xAOD::TActiveStore::store();
  std::string containerName = m_fatJetReader->getContainerName() + "___" + m_currentVar + "_SC";
  bool doneSC=false;
  const xAOD::JetContainer* container = nullptr;
  if (store->contains<xAOD::JetContainer>(containerName.c_str())) {
    if (store->retrieve(container, containerName.c_str()).isSuccess()) {
      doneSC=true;
    } else {
      Warning("ObjectReader::getObjects()", ("Failed to retrieve container '" + containerName + " from active TStore'!").c_str());
    }
  }
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > fatJets_SC;
  if (doneSC) {
    fatJets_SC.first=(xAOD::JetContainer*)container;
    fatJets_SC.second=(xAOD::ShallowAuxContainer*)container->getConstStore();
  } else {
    fatJets_SC = xAOD::shallowCopyContainer( *m_fatJets );
    if (!store->record(fatJets_SC.first, containerName.c_str()).isSuccess()) {
      Warning("ObjectReader::getObjects()", ("Failed to record container '" + containerName + " to active TStore'!").c_str());
    }
    if (!store->record(fatJets_SC.second, (containerName + "Aux.").c_str()).isSuccess()) {
      Warning("ObjectReader::getObjects()", ("Failed to record aux container '" + containerName + " to active TStore'!").c_str());
    }
  }

  /*
  if ( m_CxAODTag=="CxAODTag26" ) {
    //all used fatjet syst was reculculated

    std::string combMassVar = "Nominal";
    if (isCombMassSyst) combMassVar = m_currentVar;
    CP_CHECK("combMassTmp2()", m_jesProviderHbb->applySystematicVariation(CP::SystematicSet(combMassVar)), m_debug);

    for (unsigned int iJet = 0; iJet < fatJets_SC.first->size(); ++iJet) {
      xAOD::Jet* jetSC = fatJets_SC.first->at(iJet);

      float calo_pt = jetSC->pt();
      float calo_mass = jetSC->m();
      float TA_mass = Props::TAmass.get(jetSC);
      if(TA_mass<0){
	TA_mass=0; //This protection was in JMSCorrection.cxx
      }
      float TA_pt = sqrt((jetSC->e() * jetSC->e()) - (TA_mass * TA_mass)) / cosh( jetSC->eta() );
      xAOD::JetFourMom_t calo_p4 = jetSC->jetP4();
      xAOD::JetFourMom_t TA_p4 = xAOD::JetFourMom_t(TA_pt, calo_p4.eta(), calo_p4.phi(), TA_mass);

      Jet jetStr;
      jetStr.vec = jetSC->p4();

      if ( m_doCombMassMuon ) {
	if ( jetSC->pt()>0 ) {
          EL_CHECK("AnalysisReader_VHcc::combMassTmp2() ",getBJetEnergyCorrTLV(jetSC,jetStr.vec_corr,true,m_fatJetCorrType));
	  //Muon in jet is done without checking btag since the prop was not yet set.
	  //But the effect is small like 1% in 0tag have XbbMu correction vector.
	  calo_pt = jetStr.vec_corr.Pt();
	  calo_mass = jetStr.vec_corr.M();
	  TA_mass = TA_mass*(jetStr.vec_corr.Pt()/jetStr.vec.Pt());
	  TA_pt = sqrt((jetStr.vec_corr.E() * jetStr.vec_corr.E()) - (TA_mass * TA_mass)) / cosh( jetStr.vec_corr.Eta() );
	  calo_p4 = xAOD::JetFourMom_t(jetStr.vec_corr.Pt(),jetStr.vec_corr.Eta(),jetStr.vec_corr.Phi(),jetStr.vec_corr.M());
	  TA_p4 = xAOD::JetFourMom_t(TA_pt, jetStr.vec_corr.Eta(), jetStr.vec_corr.Phi(), TA_mass);
	  //TA_mass = TA_p4.M();
	}
      }

	//EL_CHECK("combMassTmp2()", m_jetMassCorr->comb_mass_applyCalibration(*jetSC, calo_pt, calo_mass, TA_mass));
      static SG::AuxElement::Accessor<xAOD::JetFourMom_t> caloScale("JetJMSScaleMomentumCalo");
      static SG::AuxElement::Accessor<xAOD::JetFourMom_t> TAScale("JetJMSScaleMomentumTA");
      static SG::AuxElement::Accessor<xAOD::JetFourMom_t> combScale("JetJMSScaleMomentumCombQCD");
      caloScale.set(*jetSC, calo_p4);
      TAScale.set(*jetSC, TA_p4);
      combScale.set(*jetSC, jetSC->jetP4());
      CP_CHECK("combMassTmp2()", m_jesProviderHbb->applyCorrection(*jetSC, *m_eventInfo), m_debug);
      if (m_currentVar.compare("FATJET_JER__1up") == 0) {
	CP_CHECK("combMassTmp2()", applySmearingJER(*jetSC), m_debug);
      }
      else if (m_currentVar.compare("FATJET_JMR__1up") == 0) {
	CP_CHECK("combMassTmp2()", applySmearingJMR(*jetSC), m_debug);
      }
    }
  }//Tag26
  */

  if ( m_CxAODTag=="CxAODTag28" ) {
    //combined mass is done in the maker and only JER was decided to reculculate
    for (unsigned int iJet = 0; iJet < fatJets_SC.first->size(); ++iJet) {
      xAOD::Jet* jetSC = fatJets_SC.first->at(iJet);
      if (m_currentVar.compare("FATJET_JER__1up") == 0) {
	CP_CHECK("combMassTmp2()", applySmearingJER(*jetSC), m_debug);
      }
    }
  }//Tag28

  m_fatJets = fatJets_SC.first;

  return EL::StatusCode::SUCCESS;
}// combMassTmp2

CP::CorrectionCode AnalysisReader_VHcc::applySmearingJER(xAOD::Jet& jet)
{
  // seed similar to JERSmearingTool
  long int seed = 1.1e+5 * std::abs(jet.phi());
  double smear = 1;
  if ( m_CxAODTag=="CxAODTag26" ) {
    // smearing by 5%
    smear = getSmearFactor(0.05, seed);
  }
  if ( m_CxAODTag=="CxAODTag28" ) {
    // smearing by 2%
    smear = getSmearFactor(0.02, seed);
  }
  // energy smearing: scale pt only
  xAOD::JetFourMom_t p4 = jet.jetP4();
  jet.setJetP4(xAOD::JetFourMom_t(smear * p4.Pt(), p4.Eta(), p4.Phi(), p4.M()));
  return CP::CorrectionCode::Ok;
} // applySmearingJER

CP::CorrectionCode AnalysisReader_VHcc::applySmearingJMR(xAOD::Jet& jet)
{
  // seed similar to JERSmearingTool
  long int seed = 1.2e+5 * std::abs(jet.phi());
  // smearing by 10%
  double smear = getSmearFactor(0.10, seed);
  // mass smearing: scale mass only
  xAOD::JetFourMom_t p4 = jet.jetP4();
  jet.setJetP4(xAOD::JetFourMom_t(p4.Pt(), p4.Eta(), p4.Phi(), smear * p4.M()));
  return CP::CorrectionCode::Ok;
} // applySmearingJMR

double AnalysisReader_VHcc::getSmearFactor(double sigma, long int seed)
{
  // Set the seed
  m_rand.SetSeed(seed);
  // Calculate the smearing factor
  double smear = -1;
  // at least 50% change to get smear > 0 => runtime ok
  while (smear <= 0) {
    smear = m_rand.Gaus(1.0, sigma);
  }
  return smear;
} // getSmearFactor

//combMass end
