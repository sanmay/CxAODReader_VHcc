#include <type_traits>
#include <cfloat>

#include "CxAODReader/EasyTree.h"
#include "CxAODTools_VHbb/VHbb1lepEvtSelection.h"
#include "CxAODTools_VHbb/TriggerTool_VHbb1lep.h"

#include "CxAODReader_VHcc/AnalysisReader_VHcc.h"
#include <CxAODReader_VHcc/AnalysisReader_VHcc1Lep.h>

#define length(array) (sizeof(array) / sizeof(*(array)))

AnalysisReader_VHcc1Lep::AnalysisReader_VHcc1Lep () :
  AnalysisReader_VHcc()
{
}

AnalysisReader_VHcc1Lep::~AnalysisReader_VHcc1Lep ()
{
}

EL::StatusCode AnalysisReader_VHcc1Lep::initializeSelection ()
{

  AnalysisReader_VHcc::initializeSelection ();

  m_eventSelection = new VHbb1lepEvtSelection();
  //m_fillFunction   = std::bind(&AnalysisReader_VHcc1Lep::fill_1Lep, this);
  m_fillFunction   = std::bind(&AnalysisReader_VHcc1Lep::run_1Lep_analysis, this);
  if(m_model == Model::HVT) ((VHbb1lepEvtSelection*)m_eventSelection)->SetModel("HVT");
  ((VHbb1lepEvtSelection*)m_eventSelection)->SetAnalysisType(m_analysisType);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VHcc1Lep::initializeTools ()
{
  EL_CHECK("AnalysisReader_VHcc1Lep::initializeTools()", AnalysisReader_VHcc::initializeTools());

  m_triggerTool = new TriggerTool_VHbb1lep(*m_config);
  EL_CHECK("AnalysisReader_VHcc1Lep::initializeTools()", m_triggerTool -> initialize());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VHcc1Lep::run_1Lep_analysis ()
{

  if(m_debug) std::cout << " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " << std::endl;
  if(m_debug) std::cout << " >>>>> Starting run_1Lep_analysis " << std::endl;
  if(m_debug) std::cout << " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " << std::endl;

  // TEMPORARY FIX OF THE GRL AT READER LEVEL
  // ATTENTION ! ONLY APPLIED TO SM VH
  if ( m_model == Model::MVA || m_model == Model::SM ) {
    if ( !m_isMC && m_eventInfo->runNumber() == 303079 ) {
      bool doRemoveEvent = AnalysisReader_VHcc::removeUnwantedLB();
      if (doRemoveEvent) {
	//std::cout << "Going to remove event, run nb " << m_eventInfo->runNumber() << " LB nb " << m_eventInfo->lumiBlock() << std::endl;
	return EL::StatusCode::SUCCESS;
      }
    }
  }

  //*********************************************************//
  //                     READ CONFIGURATION                  //
  //*********************************************************//

  // set defaults
  bool doBJetEnergyCorr              = true;
  bool doMbbRescaling                = true; // is called mbbwindow by other channels
  bool doBlindingData                = true;
  bool doBlindingMC                  = false;
  bool ApplyFatJetMuOR               = false;
  bool mVHvsPtvCut                   = false;

  // easy tree options
  bool doMJEtree                     = false;

  // plotting options
  bool doInputPlots                  = true;
  bool doBasicPlots                  = true;
  bool doExtendedPlots               = false;
  bool doTLVPlots                    = false;
  bool doSplitLepFlavour             = false;
  bool doSplitLepCharge              = false;
  bool doSplitWhfCR                  = false;
  bool reduceHistograms              = false;

  // blinding window
  double mbbResolvedLowEdge          = 110e3;
  double mbbResolvedHighEdge         = 140e3;
  //double mbbResolvedLowEdge          = 0; //110e3;
  //double mbbResolvedHighEdge         = 100000000000e3; //140e3;
  double mbbMergedLowEdge            =  75e3;
  double mbbMergedHighEdge           = 145e3;
  double mbbMergedLowerBound         =  50e3;
  double mbbMergedUpperBound         =  200e3;

  // mbb rejection
  double mbbRestrictLowEdge = 50e3;
  double mbbRestrictHighEdge = 200e3;
  bool doMbbRejHist = false;

  bool writeMVATree                  = false;
  bool writeEasyTree                  = false;

  // misc options
  bool printEventWeightsForCutFlow   = false;
  bool doCutflow   = false;
  double dataOneOverSF = 1.0;
  bool doMergeCR = false;
  bool nominalOnly = false;

  double minElMET = 30e3;
  double minMuMET = 0e3;

  // for DL1 c-tagging
  float b_fraction = -99;
  float ctag_cutvalue = -99;

  m_config->getif<double>("minElMET", minElMET);
  m_config->getif<double>("minMuMET", minMuMET);

  // Run the reader in inverted isolation mode?
  // (for multijet estimate)
  bool doIsoInv = false;
  m_config->getif<bool>("doIsoInv", doIsoInv);
  // read in parameters from config and overwrite, if exist
  m_config->getif<bool>("doBJetEnergyCorr",            doBJetEnergyCorr);
  m_config->getif<bool>("doMbbRescaling",              doMbbRescaling);
  m_config->getif<bool>("doBlindingData",              doBlindingData);
  m_config->getif<bool>("doBlindingMC",                doBlindingMC);
  m_config->getif<bool>("doInputPlots",                doInputPlots);
  m_config->getif<bool>("doBasicPlots",                doBasicPlots);
  m_config->getif<bool>("doExtendedPlots",             doExtendedPlots);
  m_config->getif<bool>("doTLVPlots",                  doTLVPlots);
  m_config->getif<bool>("doSplitLepFlavour",           doSplitLepFlavour);
  m_config->getif<bool>("doSplitLepCharge",            doSplitLepCharge);
  m_config->getif<bool>("doSplitWhfCR",                doSplitWhfCR);
  m_config->getif<bool>("reduceHistograms",            reduceHistograms);
  m_config->getif<bool>("doMJEtree",                   doMJEtree);
  m_config->getif<bool>("printEventWeightsForCutFlow", printEventWeightsForCutFlow);
  m_config->getif<double>("mbbResolvedLowEdge",        mbbResolvedLowEdge);
  m_config->getif<double>("mbbResolvedHighEdge",       mbbResolvedHighEdge);
  m_config->getif<double>("mbbMergedLowEdge",          mbbMergedLowEdge);
  m_config->getif<double>("mbbMergedHighEdge",         mbbMergedHighEdge);
  m_config->getif<double>("mbbRestrictLowEdge",        mbbRestrictLowEdge);
  m_config->getif<double>("mbbRestrictHighEdge",       mbbRestrictHighEdge);
  m_config->getif<double>("mbbMergedLowerBound",       mbbMergedLowerBound);
  m_config->getif<double>("mbbMergedUpperBound",       mbbMergedUpperBound);
  m_config->getif<bool>("doMbbRejHist",                doMbbRejHist);
  m_config->getif<double>("dataOneOverSF",             dataOneOverSF);
  m_config->getif<bool>("ApplyFatJetMuOR",             ApplyFatJetMuOR);
  m_config->getif<bool>("mVHvsPtvCut",                 mVHvsPtvCut);
  m_config->getif<bool>("writeMVATree",                writeMVATree);
  m_config->getif<bool>("writeEasyTree",               writeEasyTree);
  m_config->getif<bool>("doMergeCR",                   doMergeCR);
  m_config->getif<bool>("doCutflow",                   doCutflow);
  m_config->getif<bool>("nominalOnly",                 nominalOnly);


  //*********************************************************//
  //                     EVENT INITIALISATION                //
  //*********************************************************//

  //Initialise lepton flavour category
  m_leptonFlavour = lepFlav::Combined;

  // initialize eventFlag - all cuts are initialized to false by default
  unsigned long eventFlagResolved = 0;
  unsigned long eventFlagMerged   = 0;

  ResultVHbb1lep selectionResult = ((VHbb1lepEvtSelection*)m_eventSelection)->result();

  // reset physics metadata
  m_physicsMeta         = PhysicsMetadata();
  m_physicsMeta.channel = PhysicsMetadata::Channel::OneLep;
  //Set the Analysis type for histograms
  if(m_model == Model::HVT){
    if(m_debug) std::cout << " >>>>> Setting HistNameSvc::AnalysisType::VHres" << std::endl;
    m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::VHres);
  }else if(m_model == Model::SM){
    m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::CUT);
  }else if(m_model == Model::MVA){
    m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::MVA);
  }
  if(m_debug) std::cout << " >>>>> Event Initialisation complete " << std::endl;

  //if(m_debug) std::cout << " >>>>> HistNameSvc m_variation : " << m_histNameSvc->get_variation() << std::endl;

  //*********************************************************//
  //      INIT ALL VARIABLES FROM THE START                  //
  //*********************************************************//

  const xAOD::Electron  *el                 = selectionResult.el;
  const xAOD::Muon      *mu                 = selectionResult.mu;
  if(doSplitLepFlavour){
    m_leptonFlavour = (el ? lepFlav::El : lepFlav::Mu);
  }
  const xAOD::MissingET *met                = selectionResult.met;
  std::vector<const xAOD::Jet*> signalJets  = selectionResult.signalJets;
  std::vector<const xAOD::Jet*> forwardJets = selectionResult.forwardJets;
  std::vector<const xAOD::Jet*> trackJets   = selectionResult.trackJets;
  std::vector<const xAOD::TauJet*> taus     = selectionResult.taus;
  std::vector<const xAOD::Jet*> selectedJets;
  std::vector<const xAOD::Jet*> associatedTrackJets;
  std::vector<const xAOD::Jet*> unassociatedTrackJets;


  int nSignalJet  = signalJets.size();
  int nForwardJet = forwardJets.size();
  int nJet        = nSignalJet + nForwardJet;
  setevent_nJets(signalJets, forwardJets);

  // **Definition** :  forward jets
  TLorentzVector fwdjet1Vec, fwdjet2Vec, fwdjet3Vec;
  if (nForwardJet >= 1) fwdjet1Vec = forwardJets.at(0)->p4();
  if (nForwardJet >= 2) fwdjet2Vec = forwardJets.at(1)->p4();
  if (nForwardJet >= 3) fwdjet3Vec = forwardJets.at(2)->p4();

  // **Definition** :  signal jets
  TLorentzVector sigjet1Vec, sigjet2Vec, sigjet3Vec;
  if (nSignalJet >= 1) sigjet1Vec = signalJets.at(0)->p4();
  if (nSignalJet >= 2) sigjet2Vec = signalJets.at(1)->p4();
  if (nSignalJet >= 3) sigjet3Vec = signalJets.at(2)->p4();

  // **Definition** : define jet pair/triplet for anti-QCD cut
  TLorentzVector j1VecPresel, j2VecPresel, j3VecPresel;
  if (nSignalJet >= 1) j1VecPresel = signalJets.at(0)->p4();
  if (nSignalJet >= 2) j2VecPresel = signalJets.at(1)->p4();
  if (nSignalJet >= 3) j3VecPresel = signalJets.at(2)->p4();
  else if (nForwardJet >= 1) j3VecPresel = forwardJets.at(0)->p4();


  if(m_debug) std::cout << " >>>>> Formed Jet 4-Vectors " << std::endl;

  // **Definition** : b-tagging**
  if(m_doTruthTagging){
    compute_TRF_tagging(signalJets);
  }
  else{
    compute_ctagging(b_fraction, ctag_cutvalue);
  }


  if(m_debug) std::cout << " >>>>> Calculated B-Jet Information " << std::endl;


  int tagcatExcl = -1;
  tagjet_selection(signalJets, forwardJets, selectedJets, tagcatExcl);
  m_physicsMeta.nTags = tagcatExcl;
  setevent_flavour(selectedJets); // default from resolved analysis; is overwritten later
  int nSelectedJet = selectedJets.size();

  // **Definition**: define b-jet system
  TLorentzVector HVecJet, j1VecSel, j2VecSel, j3VecSel, bbjVec;
  if (nSelectedJet >= 1) j1VecSel   = selectedJets.at(0)->p4();
  if (nSelectedJet >= 2) j2VecSel   = selectedJets.at(1)->p4();
  if (nSelectedJet >= 3) j3VecSel   = selectedJets.at(2)->p4();
  if (nSelectedJet >= 2) HVecJet    = j1VecSel + j2VecSel;
  if (nSelectedJet >= 3) bbjVec     = HVecJet + j3VecSel ;

  // **Definition**: b-jet corrected vectors
  TLorentzVector HVecJetCorr, j1VecCorr, j2VecCorr, j3VecCorr, bbjVecCorr;
  j1VecCorr = j1VecSel; // fall back
  j2VecCorr = j2VecSel;
  j3VecCorr = j3VecSel;

  if ((nSelectedJet >= 1) && BTagProps::isTagged.get(selectedJets.at(0))) {
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(selectedJets.at(0),j1VecCorr,false,m_jetCorrType));
  }
  if ((nSelectedJet >= 2) && BTagProps::isTagged.get(selectedJets.at(1))) {
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(selectedJets.at(1),j2VecCorr,false,m_jetCorrType));
  }
  if ((nSelectedJet >= 3) && BTagProps::isTagged.get(selectedJets.at(2))) {
    EL_CHECK("AnalysisReader_VHcc::setJetVariables() ",getBJetEnergyCorrTLV(selectedJets.at(2),j3VecCorr,false,m_jetCorrType));
  }
  if (nSelectedJet >= 2) HVecJetCorr = j1VecCorr + j2VecCorr;
  if (nSelectedJet >= 3) bbjVecCorr  = HVecJetCorr + j3VecCorr; // should use corrected 3rd jet?


  // **Definition**: mbb rescaling
  TLorentzVector j1VecRW, j2VecRW, HVecJetRW;
  j1VecRW = j1VecCorr; // baseline
  j2VecRW = j2VecCorr;
  HVecJetRW = HVecJetCorr;
  if (doMbbRescaling && (HVecJetCorr.M() > mbbResolvedLowEdge) && (HVecJetCorr.M() < mbbResolvedHighEdge)) {
    rescale_jets(HVecJetCorr.M(), j1VecRW, j2VecRW);
  }
  if (nSelectedJet >= 2) HVecJetRW = j1VecRW + j2VecRW;


  // **Definition** : met
  TLorentzVector metVec, metVecJetCorr, metVecJetRW, metVecFJCorr, metVecFJRW;
  if (met) {
    metVec.SetPtEtaPhiM(met->met(), 0, met->phi(), 0);
    if (nSelectedJet >= 2) metVecJetCorr = getMETCorrTLV(met, {&j1VecSel, &j2VecSel}, {&j1VecCorr, &j2VecCorr});
    if (nSelectedJet >= 2) metVecJetRW   = getMETCorrTLV(met, {&j1VecSel, &j2VecSel}, {&j1VecRW, &j2VecRW});
  }

  // **Definition** : lepton
  TLorentzVector lepVec;
  int nLooseEl = 0;
  int nSigEl   = 0;
  int nLooseMu = 0;
  int nSigMu   = 0;
  if (el)  {
    lepVec = el->p4();
    if (Props::isVHLooseElectron.get(el) == 1) nLooseEl = 1;
    if (Props::isWHSignalElectron.get(el) == 1) nSigEl = 1;
  }
  else if (mu) {
    lepVec = mu->p4();
    if (Props::isVHLooseMuon.get(mu) == 1) nLooseMu = 1;
    if (Props::isWHSignalMuon.get(mu) == 1) nSigMu = 1;
  }
  else {
    Error("run_1Lep_analysis()", "Missing lepton!");
    return EL::StatusCode::FAILURE;
  }

  // **Definition** : neutrino using W mass constraint
  TLorentzVector nuVec        = getNeutrinoTLV(metVec, lepVec, true);
  TLorentzVector nuVecJetCorr = getNeutrinoTLV(metVecJetCorr, lepVec, true);
  TLorentzVector nuVecJetRW   = getNeutrinoTLV(metVecJetRW, lepVec, true);
  TLorentzVector nuVecFJCorr  = getNeutrinoTLV(metVecFJCorr, lepVec, true);
  TLorentzVector nuVecFJRW    = getNeutrinoTLV(metVecFJRW, lepVec, true);

  // **Definition** : W
  TLorentzVector lepVecT;
  lepVecT.SetPtEtaPhiM(lepVec.Pt(), 0, lepVec.Phi(), 0);
  TLorentzVector WVecT       = lepVecT + metVec;
  TLorentzVector WVec        = lepVec + nuVec;
  TLorentzVector WVecJetCorr = lepVec + nuVecJetCorr;
  TLorentzVector WVecJetRW   = lepVec + nuVecJetRW;
  TLorentzVector WVecFJCorr  = lepVec + nuVecFJCorr;
  TLorentzVector WVecFJRW    = lepVec + nuVecFJRW;

  // **Definition** : VH resonance
  TLorentzVector VHVecJet,  VHVecJetCorr, VHVecJetRW;
  if (nSelectedJet >= 2) VHVecJet     = WVec + HVecJet;
  if (nSelectedJet >= 2) VHVecJetCorr = WVecJetCorr + HVecJetCorr;
  if (nSelectedJet >= 2) VHVecJetRW   = WVecJetRW + HVecJetRW;

  if(m_debug) std::cout << " >>>>> Formulated VH system 4-Vector" << std::endl;

  //*********************************************************//
  //                  RESOLVED BITFLAG / CUTFLOW             //
  //*********************************************************//

  // Joe: commenting the event selection for now, to be restored later!
  /*

  if(m_debug) std::cout << " >>>>> Resolved BitFlag" << std::endl;

  // C0: All events (CxAOD)
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::AllCxAOD);

  // C1: trigger
  double triggerSF = 1.;
  bool isTriggered =  pass1LepTrigger(triggerSF,selectionResult);
  if(m_debug) std::cout << " >>>>>>>>>>>>>>>>> isTriggered : " << isTriggered << std::endl;
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::Trigger, isTriggered);

  // C2: 1 loose lepton
  if(m_debug) std::cout << " >>>>>>>>>>>>>>>>> nLooseEl + nLooseMu : " << nLooseEl + nLooseMu << std::endl;
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::LooseLeptons, (nLooseEl + nLooseMu == 1));

  // C3: 1 signal lepton  (HVT Lepton defintion raised to 27GeV du to new 2016 trigger menu)
  // (SM VH(bb) Electron Channel raised to 27GeV, Muon Channel stay with 25GeV)
  // retrieve quantities on isolation and lepton quality to restore cuts
  // on reader level that were opened for the MJ CxAODs
  bool passPtLep27 = false;
  if(m_model == Model::HVT || ( el && !mu ))
  {
   if(lepVec.Pt() > 27e3) passPtLep27 = true;
  }
  else passPtLep27 = true;

  float caloIso = -1.0;
  float trackIso = -1.0;
  bool passNom = false;
  bool passIsoInv = false;
  bool passLepQuality = false;
  if (el) {
    caloIso    = Props::topoetcone20.get(el) / lepVec.Pt();
    trackIso   = Props::ptvarcone20.get(el) / lepVec.Pt();
    if(m_model == Model::HVT)
      passNom    = caloIso < 0.06 and trackIso < 0.06;
    else if (m_model == Model::SM || m_model == Model::MVA)
      passNom    = Props::topoetcone20.get(el) < 3.5e3;
    if(m_model == Model::HVT)
      passIsoInv = caloIso > 0.06 and trackIso < 0.06;
    else if (m_model == Model::SM || m_model == Model::MVA)
      passIsoInv = Props::topoetcone20.get(el) > 3.5e3;
    passLepQuality = Props::isTightLH.get(el) == 1;
  } else if (mu) {
    caloIso    = Props::ptvarcone30.get(mu) / lepVec.Pt();
    if(m_model == Model::HVT)
      passNom    = caloIso < 0.06;
    else if (m_model == Model::SM || m_model == Model::MVA){
      if(Props::ptcone20.exists(mu)) // ptcone20 is not available for CxAODs before Tag28.
	passNom = Props::ptcone20.get(mu) < 1.25e3;
      else
	passNom = caloIso < 0.06;
    }
    if(m_model == Model::HVT)
      passIsoInv = caloIso > 0.06;
    else if (m_model == Model::SM || m_model == Model::MVA){
      if(Props::ptcone20.exists(mu)) // ptcone20 is not available for CxAODs before Tag28.
	passIsoInv = Props::ptcone20.get(mu) > 1.25e3;
      else
	passIsoInv = caloIso > 0.06;
    }
    passLepQuality = Props::isWHSignalMuon.get(mu) == 1;
  }
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SignalLeptons,
             passPtLep27 and
	     passLepQuality and
	     (nSigEl + nSigMu == 1) and
	     ((!doIsoInv and passNom) or ((doIsoInv or m_doQCD) and passIsoInv)));

  // C4: njets
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::AtLeast2Jets, nJet >= 2);
  if(m_debug) std::cout << " >>>>>>>>>>>>>>>>> AtLeast2Jets : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::AtLeast2Jets }) << std::endl;

  // C5: nSignalJets
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::AtLeast2SigJets, nSignalJet >= 2 && nSignalJet <= 3);
  if(m_debug)   std::cout << " >>>>>>>>>>>>>>>>> AtLeast2SigJets : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::AtLeast2SigJets }) << std::endl;

  // C6: pt > 45
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::Pt45, j1VecSel.Pt() > 45e3);
  if(m_debug)   std::cout << " >>>>>>>>>>>>>>>>> Pt45 : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::Pt45 }) << std::endl;

  // C7, C8 MET mTW cut
  if(WVecT.Pt() > 150e3){
    if( nSigEl == 1 && nSigMu == 0 ) updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > minElMET);
    else if( nSigEl == 0 && nSigMu == 1 ) updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > minMuMET);

    updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW, (m_model == Model::HVT) ? (WVecT.M() > 0e3 && WVecT.M() < 300e3) : WVecT.M() > 0e3); // No mTW cut at high pTV
    if(m_debug)     std::cout << " >>>>>>>>>>>>>>>>> MET : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::MET }) << std::endl;
    if(m_debug)     std::cout << " >>>>>>>>>>>>>>>>> mTW : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::mTW }) << std::endl;
  }
  else if(WVecT.Pt() < 150e3){
    if( nSigEl == 1 && nSigMu == 0 ){
      if( nJet == 2 ) {
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > 30e3);
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW, WVecT.M() > 40e3); // 30 GeV mTW cut for low  WpT, 2jet electron channel
      }
      else if ( nJet == 3 ) {
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > 20e3);
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW, WVecT.M() > 20e3); // 20 GeV mTW cut for low  WpT, 3 jet electron channel
      }
      else{
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > 0e3);
        updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW, WVecT.M() > 0e3);
      }
    }
    else if( nSigEl == 0 && nSigMu == 1 ){
      updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::MET, metVec.Pt() > 0e3);
      updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW, WVecT.M() > 0e3); // cuts for muon channel are still to be finalised
    }
    else {
      Error("run_1lep()", "Invalid lepton type");
      return EL::StatusCode::FAILURE;
    }
  }

  // C9: 3rd b-jet veto
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::Veto3bjet, tagcatExcl < 3);
  if(m_debug)   std::cout << " >>>>>>>>>>>>>>>>> Veto3bjet : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::Veto3bjet }) << std::endl;

  // mbb rejection
  bool is_not_in_mbbRestrict = true;
  if( m_model == Model::HVT && !doMbbRejHist ){
    is_not_in_mbbRestrict = (HVecJetCorr.M() > mbbRestrictLowEdge) && (HVecJetCorr.M() < mbbRestrictHighEdge);
  }
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mbbRestrict, is_not_in_mbbRestrict);
  if(m_debug)   std::cout << " >>>>>>>>>>>>>>>>> mBBRestrict : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::mbbRestrict }) << std::endl;

  // mbb corrected
  bool is_inside_mbbCorr = true;
  if( m_model == Model::HVT ){
    is_inside_mbbCorr = ((HVecJetCorr.M() > mbbResolvedLowEdge) && (HVecJetCorr.M() < mbbResolvedHighEdge));
  }
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mbbCorr, is_inside_mbbCorr);
  if(m_debug)   std::cout << " >>>>>>>>>>>>>>>>> mBBCorr : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::mbbCorr }) << std::endl;

  // pTV
  if( m_model == Model::HVT && mVHvsPtvCut){
    //###############  mVH dependent pTV cut (optimised) ################
    //double low_Opt_grad = 249.88;    //(1+2-tag)   (log(x) version)
    //double low_Opt_inter = -1352.31;  //(1+2-tag)   (log(x) version)
    double low_Opt_grad = -326052;    //(1+2-tag)   (inv(x) version)
    double low_Opt_inter = 709.60;  //(1+2-tag)   (inv(x) version)
    double InvMassVH = VHVecJetCorr.M() / 1e3;
    //bool passLowerOpt = (WVecT.Pt() / 1e3) > (low_Opt_grad*log(InvMassVH)) + low_Opt_inter;  //(log(x) version)
    bool passLowerOpt = (WVecT.Pt() / 1e3) > (low_Opt_grad/(InvMassVH)) + low_Opt_inter;    //(inv(x) version)

    //Update the pTV flag for the resolved regime
    bool passed = WVecT.Pt()/1.e3 > 150 && passLowerOpt;
    updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, passed);
    //###################################################################
  }else{
    updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, WVecT.Pt()/1.e3 > 150);
  }
  if(m_debug)     std::cout << " >>>>>>>>>>>>>>>>> pTV : " << passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::pTV }) << std::endl;


  // dRBB cut in Cut-based
  if(WVecT.Pt() > 150e3 && WVecT.Pt() <= 200e3)updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::dRBB, j1VecSel.DeltaR(j2VecSel) < 1.8);
  else if(WVecT.Pt() > 200e3)updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::dRBB, j1VecSel.DeltaR(j2VecSel) < 1.2);

  // additional mTW cut in Cut-based
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW_add,  WVecT.M() < 120e3);


  // dphi
  double mindPhi1 = fabs(j1VecPresel.DeltaPhi(metVec));
  double mindPhi2 = fabs(j2VecPresel.DeltaPhi(metVec));
  double mindPhi3 = 1000;
  if (nJet >= 3) mindPhi3 = fabs(j3VecPresel.DeltaPhi(metVec));
  double mindPhi = std::min(mindPhi1, std::min(mindPhi2, mindPhi3));

  // for SR flags, require all cuts up to C10
  bool passResolvedSR = passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, {});
  bool passResolvedCR = !passResolvedSR && passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, {OneLeptonResolvedCuts_VHcc::mbbCorr});
  bool passCutBased = false;
  if(m_debug) std::cout << " ####################### passResolvedSR :" << passResolvedSR << std::endl;
  // May need to switch to passSPecificCuts() version instead of passAllCutsUpTo
  //passSpecificCuts(eventFlagResolved, {OneLeptonResolvedCuts_VHcc::AllCxAOD, OneLeptonResolvedCuts_VHcc::Trigger, OneLeptonResolvedCuts_VHcc::LooseLeptons,
  //                                     OneLeptonResolvedCuts_VHcc::SignalLeptons, OneLeptonResolvedCuts_VHcc::AtLeast2Jets, OneLeptonResolvedCuts_VHcc::AtLeast2SigJets, OneLeptonResolvedCuts_VHcc::Pt45, OneLeptonResolvedCuts_VHcc::MET,
  //                                     OneLeptonResolvedCuts_VHcc::mTW, OneLeptonResolvedCuts_VHcc::Veto3bjet, OneLeptonResolvedCuts_VHcc::mbbRestrict, OneLeptonResolvedCuts_VHcc::mbbCorr, OneLeptonResolvedCuts_VHcc::pTV});
  if(m_model == Model::SM) passResolvedSR = passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW_add, { OneLeptonResolvedCuts_VHcc::mbbRestrict });
  if(m_model == Model::MVA) passCutBased = passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW_add, { OneLeptonResolvedCuts_VHcc::mbbRestrict }); // Add mbb cutbased plot for SM MVA analysis to save running full analysis again
  // C10: SR_0tag_2jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_0tag_2jet , passResolvedSR && (tagcatExcl == 0) && (nJet == 2));
  // C11: SR_0tag_3jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_0tag_3jet , passResolvedSR && (tagcatExcl == 0) && (nJet == 3));
  // C12: SR_0tag_4pjet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_0tag_4pjet , passResolvedSR && (tagcatExcl == 0) && (nJet >= 4));
  // C13: SR_1tag_2jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_1tag_2jet , passResolvedSR && (tagcatExcl == 1) && (nJet == 2));
  // C14: SR_1tag_3jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_1tag_3jet , passResolvedSR && (tagcatExcl == 1) && (nJet == 3));
  // C15: SR_1tag_4pjet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_1tag_4pjet , passResolvedSR && (tagcatExcl == 1) && (nJet >= 4));
  // C16: SR_2tag_2jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_2tag_2jet , passResolvedSR && (tagcatExcl == 2) && (nJet == 2));
  // C17: SR_2tag_3jet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_2tag_3jet , passResolvedSR && (tagcatExcl == 2) && (nJet == 3));
  // C18: SR_2tag_4pjet
  updateFlag(eventFlagResolved, OneLeptonResolvedCuts_VHcc::SR_2tag_4pjet , passResolvedSR && (tagcatExcl == 2) && (nJet >= 4));

  */

  //*********************************************************//
  //                  MERGED BITFLAG / CUTFLOW               //
  //*********************************************************//

  // Joe: commenting the event selection for now, to be restored later!
  /*

  if(m_debug) std::cout << " >>>>> Merged BitFlag" << std::endl;

  // M0: All events (CxAOD)
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::AllCxAOD);

  // M1: trigger
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::Trigger, isTriggered);

  // M2: 1 signal lepton (HVT signal Lepton definition raised to 27GeV due to new trigger menu)
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::Leptons,
      passPtLep27 and
      passLepQuality and
      (nLooseEl + nLooseMu == 1) and
      (nSigEl + nSigMu == 1) and
      ((!doIsoInv and passNom) or ((doIsoInv or m_doQCD) and passIsoInv)));

  // M3: MET cut
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::MET, metVec.Pt() > 100e3);

  // M4: nFatjets
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::AtLeast1FatJet, nFatJet >= 1);

  // M5: corrected mass fatjet
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::mbbCorr, (fj1VecCorr.M() > mbbMergedLowEdge) && (fj1VecCorr.M() < mbbMergedHighEdge) );

  // M6: number of associated track jets
  if(m_model == Model::SM)  updateFlag(eventFlagMerged, OneLeptonMergedCuts::AtLeast2TrackJets, associatedTrackJets.size() >= 2);
  else if(m_model == Model::HVT) updateFlag(eventFlagMerged, OneLeptonMergedCuts::AtLeast2TrackJets, associatedTrackJets.size() >= 1);

  // M7: mTW cut
  updateFlag(eventFlagResolved, OneLeptonMergedCuts::mTW, (m_model == Model::HVT) ? (WVecT.M() > 0e3 && WVecT.M() < 300e3) : WVecT.M() > 0e3);

  // M8: pTV > 500
  if( m_analysisStrategy == "SimpleMerge500"){
    updateFlag(eventFlagMerged, OneLeptonMergedCuts::pTV, WVecT.Pt()/1.e3 > 500.);
  }else if( m_analysisStrategy == "Merged" || m_analysisStrategy == "PriorityResolved" || m_analysisStrategy == "PriorityResolvedSR" || m_analysisStrategy == "PriorityResolvedSRbtag" || m_analysisStrategy == "PriorityMerged" || m_analysisStrategy == "PriorityMergedSR" || m_analysisStrategy == "PriorityMergedSRbtag" ){

    if( fatJets.size() > 0 && mVHvsPtvCut){
      //############### mVH dependent pTV cut (optimised) ################
      double low_Opt_grad = 394.241;      //(1+2-tag)
      double low_Opt_inter = -2350.04;  //(1+2-tag)
      double InvMassVH = VHVecFatCorr.M() / 1e3;
      bool passLowerOpt = (WVecT.Pt() / 1e3) > (low_Opt_grad*log(InvMassVH)) + low_Opt_inter;
      //Update the pTV flag for the Merged regime
      updateFlag(eventFlagMerged, OneLeptonMergedCuts::pTV, passLowerOpt && WVecT.Pt()/1.e3 > 150.0);
      //############################
    }else{
      updateFlag(eventFlagMerged, OneLeptonMergedCuts::pTV, WVecT.Pt()/1.e3 > 150.0);
    }
  }

  // M9: Lower mBB bound for Merged CR
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::mbbRestrict, fj1VecCorr.M() > mbbMergedLowerBound && fj1VecCorr.M() < mbbMergedUpperBound);

  // for SR flags, require all cuts up to M7
  bool passMergedSR = passAllCutsUpTo(eventFlagMerged, OneLeptonMergedCuts::pTV, { });
  bool passMergedCR = !passMergedSR && passAllCutsUpTo(eventFlagMerged, OneLeptonMergedCuts::mbbRestrict, {OneLeptonMergedCuts::mbbCorr});
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::SR_0tag_1pfat0pjets,  passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 0)); // M9
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::SR_1tag_1pfat0pjets,  passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 1)); // M10
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::SR_2tag_1pfat0pjets,  passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 2)); // M11
  updateFlag(eventFlagMerged, OneLeptonMergedCuts::SR_3ptag_1pfat0pjets, passMergedSR && (Props::nBTags.get(fatJets.at(0)) >= 3)); // M12

  */

  //*********************************************************//
  //      DECISION TO USE MERGED OR RESOLVED ANALYSIS        //
  //*********************************************************//

  // Joe: commenting the event selection for now, to be restored later!
  /*

  bool passMerged   = (nFatJet                >= 1);
  bool passResolved = (m_physicsMeta.nSigJet  >= 2);

  if(m_debug) std::cout << " >>>>> Resolved/Merged Analysis Decision " << std::endl;
  if(m_debug) std::cout << "       passMerged : " << passMerged << std::endl;
  if(m_debug) std::cout << "       passResolved : " << passResolved << std::endl;
  if(m_debug) std::cout << "       m_analysisStrategy : " << m_analysisStrategy << std::endl;

  // Possibility to turn off merged analysis
  if ( m_analysisStrategy == "Resolved" ) {
    if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
  }
  else if ( m_analysisStrategy == "Merged" ) {
    if ( passMerged ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
  }
  else if ( m_analysisStrategy == "RecyclePtV" ) {
    // Give priority to merged above 500 GeV
    if ( WVecT.Pt()/1.e3 > 500. ) {
      if ( passMerged ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
      else if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    }
    else {
      if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
      else if ( passMerged )  m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    }
  }
  else if ( m_analysisStrategy == "SimpleMerge500" ) {
    // Only merged above 500 GeV
    if ( WVecT.Pt()/1.e3 > 500. ) {
      if ( passMerged ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    }
    else {
      if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    }
  }
  else if ( m_analysisStrategy == "PriorityResolved" ) {
    if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if ( passMerged ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
  }
  else if ( m_analysisStrategy == "PriorityResolvedSR" ){
    if ( passResolvedSR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedSR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
  }
  else if ( m_analysisStrategy == "PriorityResolvedSRbtag" ){
    if( passResolvedSR && (tagcatExcl == 2) ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 2) ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedSR && (tagcatExcl == 1) ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 1)) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
  }
  else if ( m_analysisStrategy == "PriorityMerged" ){
    if ( passMerged ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if ( passResolved ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
  }
  else if ( m_analysisStrategy == "PriorityMergedSR" ){
    if( passMergedSR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedSR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
  }
  else if ( m_analysisStrategy == "PriorityMergedSRbtag" ){
    if( passMergedSR && (Props::nBTags.get(fatJets.at(0)) == 2) ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedSR && (tagcatExcl == 2) ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if(passMergedSR &&(Props::nBTags.get(fatJets.at(0)) == 1) ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedSR && (tagcatExcl == 1) ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
    else if( passMergedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::merged;
    else if( passResolvedCR ) m_physicsMeta.regime = PhysicsMetadata::Regime::resolved;
  }

  bool isResolved = (m_physicsMeta.regime == PhysicsMetadata::Regime::resolved);
  bool isMerged   = (m_physicsMeta.regime == PhysicsMetadata::Regime::merged);

  if(m_debug) std::cout << " >>>>> Resolved/Merged Analysis Decision Complete" << std::endl;
  if(m_debug) std::cout << "       isMerged : " << isMerged << std::endl;
  if(m_debug) std::cout << "       isResolved : " << isResolved << std::endl;

  */

  //*********************************************************//
  //                    CALCULATE WEIGHTS                    //
  //*********************************************************//

  // Joe: commenting the event selection for now, to be restored later!
  /*

  // **Weight**: Lepton
  double leptonSF = 1.;
  if (m_isMC) {
    leptonSF = Props::leptonSF.get(m_eventInfo);
  }
  m_weight *= leptonSF;

  // **Weight** : Trigger
  //double triggerSF = 1.;
  //bool isTriggered =  pass1LepTrigger(triggerSF,selectionResult);
  //if (m_isMC) m_weight *= triggerSF; <- is done already inside pass1LepTrigger

  // **Weight** : JVT
  if (m_isMC) m_weight *= compute_JVTSF(signalJets);

  // **Weight** : b-tagging
  float btagWeight = 1.; // for later use
  //btagWeight is truth_tag_weight _with_ SFs if m_doTruthTagging (getEfficiency() includes SFs)
  if (m_isMC && isResolved) {
    // We set the bTagTool's jet author to the authorName argument passed to the computeBTagSFWeight now just in case
    btagWeight = computeBTagSFWeight(signalJets, m_jetReader->getContainerName());
    if(btagWeight <= 0) { Info("AnalysisReader_VHcc1Lep::run_1Lep_analysis()", "btagWeight = %f, be warned", btagWeight); }
  } else if ( m_isMC && isMerged ) {
    //Need to use the jets inside the fatjet and the unmatched trackjets
    std::vector<const xAOD::Jet*> trackJetsForBTagging = associatedTrackJets;
    trackJetsForBTagging.insert(trackJetsForBTagging.end(), unassociatedTrackJets.begin(), unassociatedTrackJets.end());
    btagWeight = computeBTagSFWeight(trackJetsForBTagging, m_trackJetReader->getContainerName());
    if(btagWeight <= 0) { Info("AnalysisReader_VHcc1Lep::run_1Lep_analysis()", "btagWeight = %f, be warned", btagWeight); }
  }
  m_weight  *= btagWeight;

  if (m_isMC && m_doTruthTagging) BTagProps::truthTagEventWeight.set(m_eventInfo,btagWeight);

  // **Weight** : Pileup
  float puWeight = 1.;
  if (m_isMC) {
    puWeight = Props::PileupweightRecalc.get(m_eventInfo);
    if (!m_config->get<bool>("applyPUWeight")) {
      m_weightSysts.push_back({"_withPU", (float) (puWeight)});
    }
  }

  // **Weight** : FakeFactor method for QCD
  if(m_doQCD) {
      m_histNameSvc->set_sample("multijet");
    if(el && nSigEl == 1) {
      EL_CHECK("run_1Lep_analysis()", applyFFweight(WVecT, metVec, el));
    }
    if (mu && nLooseMu == 1) {
      EL_CHECK("run_1Lep_analysis()", applyFFweight(WVecT, metVec, mu));
    }
    // applyFFweight might set event weight to zero in some cases,
    // like e.g. bad track isolation for electrons
    // (DBL_DIG: number of double digits, defined in <cfloat>)
    if (abs(m_weight) < (1. / (DBL_DIG - 1))) {
      if (m_debug) {
        Info("AnalysisReader_VHcc1Lep::run_1Lep_analysis()",
             "abs(m_weight) < 1e-%d after applyFFweight. Reject event.",
             (DBL_DIG - 1));
      }
      return EL::StatusCode::SUCCESS;
    }
  }
  // **Weight** : Template Method for SM VH(bb) multijet estimation.
  std::string samplename       =  m_histNameSvc->get_sample();
  if(doIsoInv && (m_model == Model::MVA || m_model == Model::SM)){
     if(el)
	   m_histNameSvc->set_sample("multijetEl");
     if(mu)
       m_histNameSvc->set_sample("multijetMu");
     if(m_isMC)m_weight *= -1;
  }

  if(m_debug) std::cout << " >>>>> Applied Weights" << std::endl;

  */

  //*********************************************************//
  //         EVENT CATEGORIZATION BASED ON BIT FLAG          //
  //*********************************************************//

  // Joe: commenting the event categorization for now, to be restored later!
  /*

  if(m_debug) std::cout << " >>>>> Event Categoristion Start" << std::endl;

  m_histNameSvc->set_description("");

  // leptons
  if (!m_doMergePtVBins) m_histNameSvc->set_pTV(WVecT.Pt());

  // here the difference is made between merged and resolved in histnames
  if ( isResolved ) {
    setevent_flavour(selectedJets);
    m_histNameSvc->set_nTag(tagcatExcl);
    m_histNameSvc->set_nJet(m_physicsMeta.nJets);
    //m_histNameSvc->set_nJet(m_physicsMeta.nJets);
    if( m_doMergeJetBins ) m_histNameSvc->set_nJet( m_physicsMeta.nJets >= 2 ? -2 : m_physicsMeta.nJets);
    else m_histNameSvc->set_nJet( m_physicsMeta.nJets );
    //m_histNameSvc->set_Resolved(isResolved);
  } else if (isMerged) {
    if (m_config->get<string>("truthLabeling") == "TrackJetCone") {
      setevent_flavour(associatedTrackJets);
    } else if (m_config->get<string>("truthLabeling") == "TrackJetGhostAssHadrons") {
      setevent_flavourGhost(associatedTrackJets);
    } else if (m_config->get<string>("truthLabeling") == "FatJetGhostAssHadrons") {
      setevent_flavourGhost(fatJets);
    } else if (m_config->get<string>("truthLabeling") == "TrackJetHybrid") {
      setevent_flavour(associatedTrackJets);
      // if there is no tag for the second track (b2Flav < 0) or there is no
      // second track, tag it with ghost associated hadrons
      // NB: if we do not want to resolve Wl into two tags, replace the if
      //     statement with
      //       " if ((m_physicsMeta.b1Flav == 5 || m_physicsMeta.b1Flav == 4) && m_physicsMeta.b2Flav < 0) "
      if (m_physicsMeta.b2Flav < 0) {
	setevent_flavourGhost(associatedTrackJets);
      }
    } else if (m_config->get<string>("truthLabeling") == "FatJetHybrid") {
      setevent_flavour(associatedTrackJets);
      // same argument with Wl as in the TrackJetHybrid case (see above)
      if (m_physicsMeta.b2Flav < 0) {
	setevent_flavourGhost(fatJets);
      }
    } else {
      Error("run_1Lep_analysis()", "truthLabeling '%s' is not implemented",
            m_config->get<string>("truthLabeling").c_str());
    } // end of if branches for different truthLabeling options

    m_histNameSvc->set_nTag(Props::nBTags.get(fatJets.at(0)));
    m_histNameSvc->set_nFatJet(nFatJet);
    m_histNameSvc->set_nBTagTrackJetUnmatched(
      Props::nAddBTags.get(fatJets.at(0)));
    m_physicsMeta.nAddBTrkJets = Props::nAddBTags.get(fatJets.at(0));
  } // end of if ( isMerged )

  m_histNameSvc->set_eventFlavour(m_physicsMeta.b1Flav, m_physicsMeta.b2Flav);

  // Region
  if ( isResolved ) {

    if ( passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, { OneLeptonResolvedCuts_VHcc::mbbCorr }) ) { // all signal cuts; ignore mbb

      if ( m_model==Model::MVA ){
	m_histNameSvc->set_description("SR");
	m_physicsMeta.region = PhysicsMetadata::Region::SR;
      }
      else if( m_model == Model::SM ){
	if ( passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::mTW_add, { OneLeptonResolvedCuts_VHcc::mbbCorr }) ) {
	  m_histNameSvc->set_description("SR");
	  m_physicsMeta.region = PhysicsMetadata::Region::SR;
	}
      }
      else if( m_model == Model::HVT ){
	if (passSpecificCuts(eventFlagResolved, { OneLeptonResolvedCuts_VHcc::mbbCorr })){
	  m_histNameSvc->set_description("SR");
	  m_physicsMeta.region = PhysicsMetadata::Region::SR;
	}
	else{
	  //Set the Control Region name
	  std::string CR_Name = (HVecJetCorr.M() < mbbResolvedLowEdge) && HVecJetCorr.M() > mbbRestrictLowEdge ? (doMergeCR ? "mBBcr" : "lowmBBcr")
	    :
	    ( HVecJetCorr.M() > mbbResolvedHighEdge && HVecJetCorr.M() < mbbRestrictHighEdge ? ( doMergeCR ? "mBBcr" : "highmBBcr") : "RejCRmbb");
	  if(m_debug) std::cout << "################################### CR_Name = " << CR_Name << std::endl;

	  //Set the PhysicsMetadata:: Merged side band definition (used later in model systematics definition)
	  (HVecJetCorr.M() < mbbResolvedLowEdge && HVecJetCorr.M() > mbbRestrictLowEdge) ? m_physicsMeta.mbbSideBandResolved = PhysicsMetadata::MbbSideBandResolved::Low
	    : ( HVecJetCorr.M() > mbbResolvedHighEdge && HVecJetCorr.M() < mbbRestrictHighEdge ?
		m_physicsMeta.mbbSideBandResolved = PhysicsMetadata::MbbSideBandResolved::High
		:
		m_physicsMeta.mbbSideBandResolved = PhysicsMetadata::MbbSideBandResolved::Outer );
	  m_physicsMeta.region = PhysicsMetadata::Region::mbbCR;
	  m_histNameSvc->set_description(CR_Name);
	}
      }

      if(m_debug) std::cout << " >>>>> EventCategory : " << m_histNameSvc->getFullHistName("") << std::endl;

    }else{
      //Skip the event
      if(!doCutflow) return EL::StatusCode::SUCCESS;
    }

  } else if ( isMerged ) {

    if ( passAllCutsUpTo(eventFlagMerged, OneLeptonMergedCuts::pTV, { OneLeptonMergedCuts::mbbCorr } ) ) { // all signal cuts; ignore mbb
      if (passSpecificCuts(eventFlagMerged, { OneLeptonMergedCuts::mbbCorr })){
	m_histNameSvc->set_description("SR");
	m_physicsMeta.region = PhysicsMetadata::Region::SR;
      }
      else if( passSpecificCuts(eventFlagMerged, {OneLeptonMergedCuts::mbbRestrict}) ){
        if( doBJetEnergyCorr ){
	  std::string CR_Name = doMergeCR ? "mBBcr" : ( fj1VecCorr.M() < mbbMergedLowEdge ? "lowmBBcr" : "highmBBcr" ) ;
          fj1VecCorr.M() < mbbMergedLowEdge ? m_physicsMeta.mbbSideBandMerged = PhysicsMetadata::MbbSideBandMerged::Low : m_physicsMeta.mbbSideBandMerged = PhysicsMetadata::MbbSideBandMerged::High;
	  m_physicsMeta.region = PhysicsMetadata::Region::mbbCR;
          m_histNameSvc->set_description(CR_Name);
        }else{
	  std::string CR_Name = doMergeCR ? "mBBcr" : ( fatjet1Vec.M() < mbbMergedLowEdge ? "lowmBBcr" : "highmBBcr" ) ;
	  fatjet1Vec.M() < mbbMergedLowEdge ? m_physicsMeta.mbbSideBandMerged = PhysicsMetadata::MbbSideBandMerged::Low : m_physicsMeta.mbbSideBandMerged = PhysicsMetadata::MbbSideBandMerged::High;
	  m_physicsMeta.region = PhysicsMetadata::Region::mbbCR;
          m_histNameSvc->set_description(CR_Name);
        }
      }
      if(m_debug) std::cout << " >>>>> EventCategory : " << m_histNameSvc->getFullHistName("mVH") << std::endl;
    }else{
      //Skip the event
      if(!doCutflow) return EL::StatusCode::SUCCESS;
    }

  }

  float Mtop        = calculateMtop(lepVec, metVec, j1VecCorr, j2VecCorr); //Moved these here to be used for W+hf CR definition
  float dYWH        = calculatedYWH(lepVec, metVec, j1VecCorr, j2VecCorr);

  if (m_histNameSvc->get_description() != "") {
    std::string descr = m_histNameSvc->get_description();
    if (doSplitLepCharge) {
      int charge = 0;
      if ( el ) charge = el -> charge();
      else charge = mu -> charge();
      if (charge < 0) descr += "_minus";
      else descr += "_plus";
    }
    if (doSplitWhfCR) {  //Splitting for W+hf control region, studied for SM VH analysis
      if (Mtop > 225e3 && HVecJetCorr.M() < 75e3 ){
	descr += "_WhfCR";
      }
      else{
	descr += "_WhfSR";
      }
    }
    m_histNameSvc->set_description(descr);
  }

  //if(m_debug) std::cout << " >>>>> EventCategory : " << m_histNameSvc->getFullHistName("mVH") << std::endl;

  // Check if event in blinded region
  bool isBlindingRegion = (isResolved && passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, {} ) && (m_histNameSvc->get_nTag() == 1))
    || (isResolved && passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, {} ) && (m_histNameSvc->get_nTag() == 2))
    || (isMerged   && passAllCutsUpTo(eventFlagMerged,   OneLeptonMergedCuts::pTV,   {} ) && (m_histNameSvc->get_nTag() == 1))
    || (isMerged   && passAllCutsUpTo(eventFlagMerged,   OneLeptonMergedCuts::pTV,   {} ) && (m_histNameSvc->get_nTag() == 2));

  m_doBlinding = isBlindingRegion && ((m_isMC && doBlindingMC) || (!m_isMC && doBlindingData));

  */

  //*********************************************************//
  //                  CORRS AND SYS                          //
  // This part needs some props from histnamesvc             //
  // -> don't call earlier                                   //
  // This part changes weights                               //
  // -> don't fill histograms before                         //
  //*********************************************************//

  // Joe: commenting the systematics for now, to be restored later!
  /*

  if (m_isMC && ((m_csCorrections.size() != 0) || (m_csVariations.size() != 0))){
    // apply CorrsAndSysts to m_weight and m_weightSysts (relies on m_histNameSvc):
    // added the MJ_TrigEff systematic from the template method on 23-Jan-2017
    // this should be moved to the TriggerTool_VHbb eventually
    if(isResolved) applyCS(WVecT.Pt(), metVec.Pt(), nJet, tagcatExcl, j1VecCorr, j2VecCorr);

    bool isNominal = false;
    if (m_currentVar == "Nominal") isNominal = true;

    // apply ttbar NNLO weight to m_weight or m_weightSysts:
    if(m_histNameSvc->get_sample() == "ttbar") applyTTbarNNLORW(isNominal);

  }

  if (doIsoInv && (m_csCorrections.size() != 0 || m_csVariations.size() != 0) ){
      if ( m_model == Model::HVT) {
       EL_CHECK("AnalysisReader_VHcc1Lep::applyCS_MJ()",applyCS_MJ(lepVec));
      }
      else if ( m_model == Model::MVA || m_model == Model::SM ) {
       if(el){
        TriggerInfo::DataPeriod period = TriggerInfo::getDataPeriodFromRun(m_eventInfo->runNumber());
        bool pass = false;
        if (period <=  TriggerInfo::DataPeriod::data15)
         pass = Props::passHLT_e24_lhmedium_L1EM20VH.get(m_eventInfo);
        else if (period >=  TriggerInfo::DataPeriod::data16A)
         pass = Props::passHLT_e26_lhtight_nod0_ivarloose.get(m_eventInfo);
        bool isTop = (samplename == "ttbar" || samplename == "stopWt" || samplename == "stopt" || samplename == "stops");
        bool isW   = (samplename == "W");
        EL_CHECK("AnalysisReader_VHcc1Lep::applyCS_SMVHMJEl()",applyCS_SMVHMJEl(el,pass,isTop,isW,nJet));
       }
       else if(mu){
        bool isTop = (samplename == "ttbar" || samplename == "stopWt" || samplename == "stopt" || samplename == "stops");
        bool isW   = (samplename == "W");
        EL_CHECK("AnalysisReader_VHcc1Lep::applyCS_SMVHMJMu()",applyCS_SMVHMJMu(mu,isTop,isW,nJet));
       }
      }
     }


  //-------------------------
  // TTbar & Stop systematics histos
  //--------------------------
  //NOTE :: mVH/1e3 !!!!!!!!!!!!!!!!!!!
  if ( (m_histNameSvc -> get_sample() == "ttbar" || m_histNameSvc -> get_sample().find("stop") != std::string::npos) && m_currentVar == "Nominal" && m_model == Model::HVT && (isResolved || isMerged) && !nominalOnly ) {
    double mBB = isResolved ? HVecJetCorr.M() : fj1VecCorr.M();
    float  mVH = isResolved ? (doMbbRescaling ? VHVecJetRW.M() : VHVecJetCorr.M()) : VHVecFatCorr.M();
    std::string Regime =  isResolved ? "Resolved" : "Boosted";
    float nAddBTags = isResolved ? 0 : Props::nAddBTags.get(fatJets.at(0));

    fillTopSystslep_PRSR( Regime, nAddBTags, mBB, mVH/1e3); //mVH= GeV, mBB = MeV

  }

  //-----------------------------
  // VJets Systematic Histograms
  //-----------------------------
  //NOTE :: mBB is in MeV because CorrsAndSysts uses MeV !!!!!!!!!!!!!!!!!!!
  if ( (m_histNameSvc -> get_sample() == "W"    || m_histNameSvc -> get_sample() == "Wv22" )
       && m_currentVar == "Nominal"
       && m_model == Model::HVT
       && (isResolved || isMerged)
       && !nominalOnly ){
    double mBB = isResolved ? HVecJetCorr.M() : fj1VecCorr.M();
    float  mVH = isResolved ? (doMbbRescaling ? VHVecJetRW.M() : VHVecJetCorr.M()) : VHVecFatCorr.M();
    //fillVjetsSystslep( mBB, mVH/1e3);
    std::string Regime =  isResolved ? "Resolved" : "Boosted";
    float nAddBTags = isResolved ? 0 : Props::nAddBTags.get(fatJets.at(0));
    fillVjetsSystslep_PRSR( Regime, mBB, mVH/1e3, nAddBTags); //mVH= GeV, mBB = MeV
  }

  //-----------------------------------------------
  // V+Jet Sherpa 2.2.1 Scale variations Histograms
  //-----------------------------------------------
  applySherpaVJet_EvtWeights();

  if(m_debug) std::cout << " >>>>> CorrsAndSysts Applied" << std::endl;

  */

  //*********************************************************//
  //                       MVA TREE                          //
  //*********************************************************//

  // Joe: commenting the MVA for now, to be restored later!
  /*

  if(m_debug) std::cout << " >>>>> MVA Tree" << std::endl;

  // reset MVA tree variables
  m_tree->Reset();
  m_tree->SetVariation(m_currentVar);

  if( m_model== Model::MVA || writeMVATree ){  //Only need the MVA tree for SM VH MVA analysis.  User can still generate tree when setting bool writeMVATree = true in config file

    m_tree->sample      = m_histNameSvc->getFullSample();
    m_tree->EventWeight = m_weight;
    m_tree->EventNumber = m_eventInfo->eventNumber();
    m_tree->nJ          = nJet;
    m_tree->nTags       = tagcatExcl;
    m_tree->dRBB        = j1VecSel.DeltaR(j2VecSel);
    m_tree->dPhiBB      = j1VecSel.DeltaPhi(j2VecSel);
    m_tree->dEtaBB      = fabs(j1VecSel.Eta() - j2VecSel.Eta());
    m_tree->mBB         = HVecJetCorr.M();
    m_tree->pTB1        = j1VecSel.Pt();
    m_tree->pTB2        = j2VecSel.Pt();
    m_tree->MET         = metVec.Pt();
    m_tree->mTW         = WVecT.M();
    m_tree->pTL         = lepVec.Pt();
    m_tree->etaL        = lepVec.Eta();
    m_tree->pTV         = WVecT.Pt();
    m_tree->dPhiVBB     = fabs(WVecT.DeltaPhi(HVecJet));
    m_tree->dPhiLBmin   = std::min(fabs(lepVec.DeltaPhi(j1VecSel)), fabs(lepVec.DeltaPhi(j2VecSel)));
    m_tree->dEtaWH      = fabs(WVec.Eta() - HVecJet.Eta());
    m_tree->Mtop        = Mtop;
    m_tree->dYWH        = dYWH;
    m_tree->dPhiLMET    = fabs(lepVec.DeltaPhi(metVec));

    if(HVecJetCorr.M()>175e3 && nJet==2)m_tree->mBB    = 175e3;
    else if(HVecJetCorr.M()>350e3 && nJet==3)m_tree->mBB    = 350e3;

    if(j1VecCorr.Pt()>350e3)m_tree->pTB1    = 350e3;
    if(j2VecCorr.Pt()>180e3)m_tree->pTB2    = 180e3;
    if(metVec.Pt()>320e3)m_tree->MET         = 320e3;
    if(WVecT.M()>215e3) m_tree->mTW = 215e3;
    if(WVecT.Pt()>450e3) m_tree->pTV         = 450e3;
    if(Mtop>1000e3)  m_tree->Mtop        = 1000e3;

    if (nJet >= 3) {
      if(j3VecSel.Pt()>300e3)m_tree->pTJ3 = 300e3; // should use corrected 3rd jet?
      else m_tree->pTJ3      = j3VecSel.Pt(); // should use corrected 3rd jet?
      if( bbjVecCorr.M()>1000e3)m_tree->mBBJ      = 1000e3;
      else m_tree->mBBJ      = bbjVecCorr.M();
    }

//  std::vector<double> values{m_tree->dRBB, m_tree->mBB, m_tree->dPhiVBB, m_tree->dPhiLBmin, m_tree->pTV, m_tree->pTB1, m_tree->pTB2, m_tree->mTW, m_tree->Mtop, m_tree->dEtaWH, m_tree->MET};
//  std::vector<float> bdt = m_mva->evaluateClassification(values);

    m_tree->ReadMVA();

  // fill MVA tree; makes sure nothing is overwritten later
    if (m_histNameSvc->get_isNominal() && (m_histNameSvc->get_description() != "") ) {
      m_tree->Fill();
    }

  }

  */

  //*********************************************************//
  //                       EASY TREE                         //
  //*********************************************************//

  // Joe: commenting most of this "easy tree" stuff, will keep something simple for testing
  /*

  if(m_debug) std::cout << " >>>>> EASY TREE" << std::endl;

  if( writeEasyTree ){

    // EasyTree reset
    m_etree->Reset();
    m_etree->SetVariation(m_currentVar);

    if ((nSelectedJet >= 2 || nFatJet >= 1)) {

      // event info
      m_etree->SetBranchAndValue<std::string>("Description", m_histNameSvc->get_description(), "");
      m_etree->SetBranchAndValue<std::string>("Sample", m_histNameSvc->getFullSample(), "");
      m_etree->SetBranchAndValue<std::string>("EventFlavor", m_histNameSvc->getEventFlavour(), "");
      m_etree->SetBranchAndValue<int>("EventNumber",   m_eventInfo->eventNumber(), -1);
      m_etree->SetBranchAndValue<float>("mu",          Props::averageInteractionsPerCrossingRecalc.get(m_eventInfo), -1.);

      // weights
      m_etree->SetBranchAndValue<float>("EventWeight", m_weight, -1.);
      m_etree->SetBranchAndValue<float>("PUWeight",    puWeight, -1.);
      m_etree->SetBranchAndValue<float>("BTagSF",      btagWeight, -1.);
      m_etree->SetBranchAndValue<float>("TriggerSF",   triggerSF , -1.);
      m_etree->SetBranchAndValue<float>("LeptonSF",    leptonSF,-1.);

      // objects counts
      m_etree->SetBranchAndValue<int>("nJets",         nJet, -1);
      m_etree->SetBranchAndValue<int>("nFats",         nFatJet, -1);
      m_etree->SetBranchAndValue<int>("nTaus",         taus.size(), -1);
      m_etree->SetBranchAndValue<int>("nTags",         m_physicsMeta.nTags, -1);
      m_etree->SetBranchAndValue<int>("nElectrons",    nSigEl, -1);
      m_etree->SetBranchAndValue<int>("nMuons",        nSigMu, -1);

      // event categorisation
      //     m_etree->SetBranchAndValue<int>("isMerged",      isMerged, -1);
      //     m_etree->SetBranchAndValue<int>("isResolved",    isResolved, -1);
      m_etree->SetBranchAndValue<int>("EventRegime",   static_cast<int>(m_physicsMeta.regime), -1);
      m_etree->SetBranchAndValue<unsigned long>("eventFlagResolved/l", eventFlagResolved, 0); // "/l" in the end to determine type
      m_etree->SetBranchAndValue<unsigned long>("eventFlagMerged/l", eventFlagMerged, 0);

      // general 1-lepton quantities
      m_etree->SetBranchAndValue<float>("lPt",         lepVec.Pt()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("lEta",        lepVec.Eta(), -10.);
      m_etree->SetBranchAndValue<float>("lPhi",        lepVec.Phi(), -10.);
      m_etree->SetBranchAndValue<float>("lM",          lepVec.M()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("WMt",         WVecT.M()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("WPt",         WVecT.Pt()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("WPhi",        WVecT.Phi(), -10);
      m_etree->SetBranchAndValue<float>("WM",          WVecT.M()/1e3, -1);
      m_etree->SetBranchAndValue<float>("met",         metVec.Pt()/1e3, -1.);
      float j1MV2c10 = -1.;
      if (nSelectedJet > 0) j1MV2c10 = Props::MV2c10.get(selectedJets.at(0));
      m_etree->SetBranchAndValue<float>("j1Pt",        j1VecCorr.Pt()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("j1Eta",       j1VecCorr.Eta(), -10.);
      m_etree->SetBranchAndValue<float>("j1Phi",       j1VecCorr.Phi(), -10.);
      m_etree->SetBranchAndValue<float>("j1M",         j1VecCorr.M()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("j1MV2c10",    j1MV2c10, -1.);
      m_etree->SetBranchAndValue<int>("j1Flav",        m_physicsMeta.b1Flav, -1);
      float j2MV2c10 = -1.;
      if (nSelectedJet > 1) j2MV2c10 = Props::MV2c10.get(selectedJets.at(1));
      m_etree->SetBranchAndValue<float>("j2Pt",        j2VecCorr.Pt()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("j2Eta",       j2VecCorr.Eta(), -10.);
      m_etree->SetBranchAndValue<float>("j2Phi",       j2VecCorr.Phi(), -10.);
      m_etree->SetBranchAndValue<float>("j2M",         j2VecCorr.M()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("j2MV2c10",    j2MV2c10, -1.);
      m_etree->SetBranchAndValue<int>("j2Flav",        m_physicsMeta.b2Flav, -1);
      m_etree->SetBranchAndValue<float>("jjPt",        HVecJetCorr.Pt()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("jjEta",       HVecJetCorr.Eta(), -10.);
      m_etree->SetBranchAndValue<float>("jjPhi",       HVecJetCorr.Phi(), -10.);
      m_etree->SetBranchAndValue<float>("jjM",         HVecJetCorr.M()/1e3, -1.);
      m_etree->SetBranchAndValue<float>("jjdR",        j1VecCorr.DeltaR(j2VecCorr), -1.);
      m_etree->SetBranchAndValue<float>("jjdPhi",      j1VecCorr.DeltaPhi(j2VecCorr), -10.);
      m_etree->SetBranchAndValue<float>("jjdEta",      fabs(j1VecCorr.Eta() - j2VecCorr.Eta()), -1.);
      m_etree->SetBranchAndValue<float>("WjjdPhi",     fabs(WVecT.DeltaPhi(HVecJetCorr)), -1.);
      m_etree->SetBranchAndValue<float>("ljmindPhi",   std::min(fabs(lepVec.DeltaPhi(j1VecCorr)), fabs(lepVec.DeltaPhi(j2VecCorr))), -1.);
      m_etree->SetBranchAndValue<float>("WjjM",        VHVecJetCorr.M()/1e3, -1.);

      // 3-jet events
      if (nSelectedJet > 2){
	m_etree->SetBranchAndValue<float>("j3Pt",        j3VecCorr.Pt()/1e3, -1.);
	m_etree->SetBranchAndValue<float>("j3Eta",       j3VecCorr.Eta(), -10.);
	m_etree->SetBranchAndValue<float>("j3Phi",       j3VecCorr.Phi(), -10.);
	m_etree->SetBranchAndValue<float>("j3M",         j3VecCorr.M()/1e3, -1.);
	m_etree->SetBranchAndValue<float>("jjjM",        bbjVecCorr.M()/1e3, -1.);
      }

      // fat jets
      if (nFatJet >= 1){
	int J1nTags = -1;
	if (nFatJet > 0) J1nTags = Props::nBTagsAll.get(fatJets.at(0));
	m_etree->SetBranchAndValue<float>("J1Pt",        fj1VecCorr.Pt()/1e3, -1.);
	m_etree->SetBranchAndValue<float>("J1Eta",       fj1VecCorr.Eta(), -10.);
	m_etree->SetBranchAndValue<float>("J1Phi",       fj1VecCorr.Phi(), -10.);
	m_etree->SetBranchAndValue<float>("J1M",         fj1VecCorr.M()/1e3, -1.);
	m_etree->SetBranchAndValue<int>("J1nTags",       J1nTags, -1);
	m_etree->SetBranchAndValue<float>("WJdPhi",      fabs(WVecT.DeltaPhi(fj1VecCorr)), -1.);
	m_etree->SetBranchAndValue<float>("WJM",         VHVecFatCorr.M()/1e3, -1.);
      }

      if (doMJEtree){ // MJ study variables - save if told to in config file
	float ptCone30 = -5;
	float ptCone20 = -5;
	float topoCone20 = -5;
	if (nSigMu == 1){
	  ptCone30 = Props::ptvarcone30.get(mu);
	  topoCone20 = Props::topoetcone20.get(mu);
	}
	else if (nSigEl == 1){
	  ptCone20 = Props::ptvarcone20.get(el);
	  topoCone20 = Props::topoetcone20.get(el);
	}

	m_etree->SetBranchAndValue<float>("topoetcone20", topoCone20, -5);
	m_etree->SetBranchAndValue<float>("ptvarcone30", ptCone30, -5);
	m_etree->SetBranchAndValue<float>("ptvarcone20", ptCone20, -5);
	m_etree->SetBranchAndValue<float>("lmetdPhi", fabs(lepVec.DeltaPhi(metVec)), -99);
	m_etree->SetBranchAndValue<double>("jjmindPhi", mindPhi, -99);
	m_etree->SetBranchAndValue<float>("jmetdPhi", fabs(j1VecCorr.DeltaPhi(metVec)), -99);
	m_etree->SetBranchAndValue<float>("jldPhi", fabs(j1VecCorr.DeltaPhi(lepVec)), -99);
      }

      if (m_histNameSvc->get_isNominal()
	  && (passAllCutsUpTo(eventFlagMerged, OneLeptonMergedCuts::pTV, { OneLeptonMergedCuts::pTV, OneLeptonMergedCuts::mbbCorr } )
	      || passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::pTV, { OneLeptonResolvedCuts_VHcc::pTV, OneLeptonResolvedCuts_VHcc::mbbCorr } ))
	  //         && passSpecificCuts(eventFlagMerged, { OneLeptonMergedCuts::AtLeast1FatJet, OneLeptonMergedCuts::AtLeast2TrackJets } )
	  //         && (Props::nBTags.get(fatJets.at(0)) >= 1)
	  ) {
	m_etree->Fill();
      }
    }
  }

  */

  if( writeEasyTree ){

    // EasyTree reset
    m_etree->Reset();
    m_etree->SetVariation(m_currentVar);

    if ((nSelectedJet >= 2)) {

      // event info
      m_etree->SetBranchAndValue<std::string>("Description", m_histNameSvc->get_description(), "");
      m_etree->SetBranchAndValue<std::string>("Sample", m_histNameSvc->getFullSample(), "");
      m_etree->SetBranchAndValue<std::string>("EventFlavor", m_histNameSvc->getEventFlavour(), "");
      m_etree->SetBranchAndValue<int>("EventNumber",   m_eventInfo->eventNumber(), -1);
      m_etree->SetBranchAndValue<float>("mu",          Props::averageInteractionsPerCrossingRecalc.get(m_eventInfo), -1.);

      // Joe: I removed the event selection, so lets just always fill this tree
      m_etree->Fill();
    }
  }

  //*********************************************************//
  //                    HISTOGRAMS                           //
  //*********************************************************//
  BookFillHist_VHcc1lep("pTV",     200,  0, 2000, WVecT.Pt() / 1e3, m_weight, SystLevel::None);
  // Joe: commenting most of the histogram booking, will keep something simple for testing
  /*

  if(m_debug) std::cout << " >>>>> Histogram Formation" << std::endl;

  // fill cutflow histogram
  if(doCutflow){
    fill_1lepResolvedCutFlow(eventFlagResolved);
    fill_1lepMergedCutFlow(eventFlagMerged);
  }

  // Quick veto of event before filling redundant histograms (for SM VH analysis)
  if((tagcatExcl < 1 || nJet > 3) && reduceHistograms){ //Added to reduce number of output histograms using reduceHistograms flag.  Just get > 0 tag and <  4 jet when true
     return EL::StatusCode::SUCCESS;
   }

  // Quick veto of events if they are in CR outter sidebands -->> Must change to bit mask check. Should be faster than string comparison
  if( m_histNameSvc->get_description() == "RejCRmbb" && !doMbbRejHist ){
    return EL::StatusCode::SUCCESS;
  }

  // print events for cutflow comparison
  if (m_isMC && printEventWeightsForCutFlow && passAllCutsUpTo(eventFlagResolved, OneLeptonResolvedCuts_VHcc::Pt45) && m_histNameSvc->get_isNominal()) {
    printf("Run: %7u  ,  Event: %8llu  -  PU = %5.3f  ,  Lepton = %5.3f  ,  Trigger = %5.3f ,  BTag = %5.3f\n",
           m_eventInfo->runNumber(), m_eventInfo->eventNumber(), puWeight, leptonSF, triggerSF, btagWeight);
  }  // weight comparison for cutflow challenge

  if ( (m_histNameSvc->get_description() != "") ) {


    if(m_doBootstrap) {
      //Filling histograms for bootstrap
      if ( !m_doBlinding && doInputPlots) {

        // =========== Resolved Regime Input Plots =============
	if (isResolved && doMbbRescaling) {
          m_histSvc->GenerateBootstrapWeights(m_weight, m_eventInfo->runNumber(), m_eventInfo->eventNumber(), m_eventInfo->mcChannelNumber()); // super super important !!!!!!!!
          m_histSvc->BookFillBootstrapHist("mBB", 100, 0, 500,  HVecJetRW.M() / 1e3, m_weight); // Note the different function name !!!!!!!!!!!!
          m_histSvc->BookFillBootstrapHist("mva", 1000, -1, 1,  m_tree->BDT,     m_weight); // mva discriminant
          m_histSvc->BookFillBootstrapHist("mvadiboson", 1000, -1, 1,  m_tree->BDT_WZ,     m_weight); // mva discriminant
        }
      }
    }
    else{

      if ( !m_doBlinding && doInputPlots) {

	//Determine if we need the _El_ & _Mu_ systematics for the mVH, mBB and MET distributions
	SystLevel ApplySysts = doIsoInv ? SystLevel::All : SystLevel::CombinedOnly;

	//================== Generic plots for basic input generation ===================
	BookFillHist_VHbb1lep("pTV",     200,  0, 2000, WVecT.Pt() / 1e3, m_weight, ApplySysts);
	if(doSplitLepFlavour && !doBasicPlots){
	  SystLevel ApplyMETSysts = doIsoInv ? SystLevel::All : SystLevel::None;
	  BookFillHist_VHbb1lep("MET",     100,  0,  1000, metVec.Pt() / 1e3, m_weight, ApplyMETSysts);
	}
	//===============================================================================

	// =========== Resolved Regime Inputs Plots ============
	if(isResolved){

	  //Form the generic TLV based on doMbbRescaling bool
	  TLorentzVector VHVecSelected   = doMbbRescaling ? VHVecJetRW : VHVecJetCorr;

	  //mBB & mVH is relevant
	  BookFillHist_VHbb1lep("mBB", 100, 0, 500,    HVecJetCorr.M() / 1e3, m_weight, ApplySysts);
	  if(passCutBased) BookFillHist_SMCut("mBB", 100, 0, 500,    HVecJetCorr.M() / 1e3, m_weight, ApplySysts); //Adding mBB cut based histogram to be plotted when running MVA code
	  if(m_model == Model::HVT) BookFillHist_VHbb1lep("mVH", 600, 0, 6000,   VHVecSelected.M() / 1e3, m_weight, ApplySysts);

	  //BDT
	  if(m_model == Model::MVA){
	    BookFillHist_VHbb1lep("mva", 1000, -1, 1,    m_tree->BDT, m_weight);
	    BookFillHist_VHbb1lep("mvadiboson", 1000, -1, 1,    m_tree->BDT_WZ, m_weight);
	    if(doIsoInv){
		m_histSvc->BookFillHist("mva_pTV",1000, -1., 1, 1000,0.,5000., m_tree->BDT, WVecT.Pt() / 1e3, m_weight);
        m_histSvc->BookFillHist("mva_mBB",1000, -1., 1, 1000,0.,5000., m_tree->BDT, HVecJetCorr.M() / 1e3, m_weight);
        m_histSvc->BookFillHist("mva_MET",1000, -1., 1, 1000,0.,5000., m_tree->BDT, metVec.Pt() / 1e3, m_weight);
        m_histSvc->BookFillHist("mva_mTW",1000, -1., 1, 1000,0.,5000., m_tree->BDT, WVecT.M() / 1e3, m_weight);
        m_histSvc->BookFillHist("mvadiboson_pTV",1000, -1., 1, 1000,0.,5000., m_tree->BDT_WZ, WVecT.Pt() / 1e3, m_weight);
        m_histSvc->BookFillHist("mvadiboson_mBB",1000, -1., 1, 1000,0.,5000., m_tree->BDT_WZ, HVecJetCorr.M() / 1e3, m_weight);
        m_histSvc->BookFillHist("mvadiboson_MET",1000, -1., 1, 1000,0.,5000., m_tree->BDT_WZ, metVec.Pt() / 1e3, m_weight);
        m_histSvc->BookFillHist("mvadiboson_mTW",1000, -1., 1, 1000,0.,5000., m_tree->BDT_WZ, WVecT.M() / 1e3, m_weight);
       }
	  }

	  //Where P is a digit representing the process
          //F is a unique integer ( F < 99 ) corresponding to each Stage1 phase-space region (bin)
	  if( m_model != Model::HVT){
	    int PF = 0;
	    if ( Props::HTXS_Stage1_Category_pTjet30.exists(m_eventInfo) ){
	      PF = Props::HTXS_Stage1_Category_pTjet30.get(m_eventInfo);
	    }
	    string Stage;
	    if ( PF != 0 ){
	      Stage = TranslateStage1Cat(PF);
	      string old_description = m_histNameSvc->get_description();
	      m_histNameSvc->set_description(string("SR"+Stage));
	      // exclusive ntags
	      m_histNameSvc->set_nTag(tagcatExcl); // b-tag
	      if ( tagcatExcl == 2 && m_physicsMeta.nJets < 4 ) {
		BookFillHist_VHbb1lep("mva", 1000, -1, 1,    m_tree->BDT, m_weight);
		BookFillHist_VHbb1lep("mvadiboson", 1000, -1, 1,    m_tree->BDT_WZ, m_weight);
	        BookFillHist_VHbb1lep("mBB", 100, 0, 500,    HVecJetRW.M() / 1e3, m_weight);
		if(passCutBased) BookFillHist_SMCut("mBB", 100, 0, 500,    HVecJetCorr.M() / 1e3, m_weight); //Adding mBB cut based histogram to be plotted when running MVA code
	      }
	      m_histNameSvc->set_description(old_description);
	    }
	  }

	  //Plot the ptvarcone and topetcone values for the isolation of the leptons
	  if(m_leptonFlavour == lepFlav::El){
	    BookFillHist_VHbb1lep("ptvarcone20_pT", 100,  0,  0.5, Props::ptvarcone20.get(el)/lepVec.Pt(), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("ptvarcone20", 100,  0,  5000, Props::ptvarcone20.get(el), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("topoetcone20_pT", 100,  0,  0.5, Props::topoetcone20.get(el)/lepVec.Pt(), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("topoetcone20", 100,  0,  5000, Props::topoetcone20.get(el), m_weight, SystLevel::None);
	  }
	  if(m_leptonFlavour == lepFlav::Mu){
	    BookFillHist_VHbb1lep("ptvarcone30_pT", 100,  0,  0.5, Props::ptvarcone30.get(mu)/lepVec.Pt(), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("ptvarcone30", 100,  0,  5000, Props::ptvarcone30.get(mu), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("topoetcone20_pT", 100,  0,  0.5, Props::topoetcone20.get(mu)/lepVec.Pt(), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("topoetcone20", 100,  0,  5000, Props::topoetcone20.get(mu), m_weight, SystLevel::None);
	  }
	}
	//======================================================

	//=========== Merged Regime Input Plots ============
	if (isMerged){

	  //Form the generic TLV based on domBBRescaling bool
	  TLorentzVector VHVecSelected   = doMbbRescaling && m_model != Model::HVT ? VHVecFatRW : VHVecFatCorr;
	  BookFillHist_VHbb1lep("mVH", 600, 0, 6000,   VHVecSelected.M() / 1e3, m_weight, SystLevel::CombinedOnly);
	  BookFillHist_VHbb1lep("mBB", 100, 0, 500,    fj1VecCorr.M() / 1e3, m_weight, SystLevel::CombinedOnly);

	}
	//===================================================

      }


      if (doBasicPlots) {

	//Delta Phi between lepton and MET
	double deltaPhi_LepMET = lepVec.DeltaPhi(metVec);
	BookFillHist_VHbb1lep("DeltaPhiLepMet",     160,  -4,  4, deltaPhi_LepMET, m_weight); // replace

	////Scalar sum of final state pT
	//Derive the Ht value
	double Ht = 0;
	for( auto SigJet : signalJets){
	  Ht += SigJet->p4().Pt();
	}
	for( auto ForwardJet : forwardJets){
	  Ht += ForwardJet->p4().Pt();
	}
	BookFillHist_VHbb1lep("Ht",     500,  0, 5000, Ht/1e3, m_weight);

	// event quantities
	BookFillHist_VHbb1lep("MET",     100,  0,  1000, metVec.Pt() / 1e3, m_weight); // replace
	BookFillHist_VHbb1lep("mTW",     100,  0,  500, WVecT.M() / 1e3, m_weight);

	//Lepton Vector quantities
	fill_TLV("lepVec",      lepVec      , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	//Lepton and MET
	m_histSvc->BookFillHist("dPhiLepMET",    200, -3.15, 3.15, lepVec.DeltaPhi(metVec), m_weight);

	//Jet quantities
	if(isResolved && !doExtendedPlots ){
	  fill_TLV("j1VecCorr",   j1VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("j2VecCorr",   j2VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nSelectedJet > 2){
	    fill_TLV("j3VecCorr",   j3VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  }
	}else if(isMerged && !doExtendedPlots){
	  fill_TLV("HVecFatCorr",  fj1VecCorr  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(associatedTrackJets.size() >= 1) fill_TLV("TrkJet1", associatedTrackJets.at(0)->p4(), m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(associatedTrackJets.size() >= 2) fill_TLV("TrkJet2", associatedTrackJets.at(1)->p4(), m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	}

	if (!doIsoInv) { // if doIsoInv these plots are part of doInputPlots
	  //Plot the ptvarcone and topetcome values for the isolation of the leptons
	  if(m_leptonFlavour == lepFlav::El){
	    BookFillHist_VHbb1lep("ptvarcone20_pT", 100,  0,  0.5, Props::ptvarcone20.get(el)/lepVec.Pt(), m_weight);
	    BookFillHist_VHbb1lep("ptvarcone20", 100,  0,  5000, Props::ptvarcone20.get(el), m_weight);
	    BookFillHist_VHbb1lep("topoetcone20_pT", 100,  0,  0.5, Props::topoetcone20.get(el)/lepVec.Pt(), m_weight);
	    BookFillHist_VHbb1lep("topoetcone20", 100,  0,  5000, Props::topoetcone20.get(el), m_weight);
	  }
	  if(m_leptonFlavour == lepFlav::Mu){
	    BookFillHist_VHbb1lep("ptvarcone30_pT", 100,  0,  0.5, Props::ptvarcone30.get(mu)/lepVec.Pt(), m_weight);
	    BookFillHist_VHbb1lep("ptvarcone30", 100,  0,  5000, Props::ptvarcone30.get(mu), m_weight);
	    BookFillHist_VHbb1lep("topoetcone20_pT", 100,  0,  0.5, Props::topoetcone20.get(mu)/lepVec.Pt(), m_weight);
	    BookFillHist_VHbb1lep("topoetcone20", 100,  0,  5000, Props::topoetcone20.get(mu), m_weight);
	  }

	}

	if( m_model == Model::MVA || m_model == Model::SM ){
	  BookFillHist_VHbb1lep("Mtop",     500,  0,  500, Mtop / 1e3, m_weight, SystLevel::None); // replace
	  BookFillHist_VHbb1lep("dYWH",     100,  0,  6, dYWH, m_weight, SystLevel::None);

	  if( !doExtendedPlots ){
	    BookFillHist_VHbb1lep("dRBB",      100, 0., 6.,   j1VecCorr.DeltaR(j2VecCorr), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("dPhiVBB",   100, 0., 3.15, fabs(WVecT.DeltaPhi(HVecJetCorr)), m_weight, SystLevel::None);
	    BookFillHist_VHbb1lep("dPhiLBmin", 100,    0.,   6.,   std::min(fabs(lepVec.DeltaPhi(j1VecCorr)), fabs(lepVec.DeltaPhi(j2VecCorr))), m_weight, SystLevel::None);
	    //3rd jet variables
	    if(nSelectedJet > 2){
	      BookFillHist_VHbb1lep("mBBJ", 100,  0,  500,  bbjVecCorr.M()/1e3, m_weight, SystLevel::None);
	    }
	  }

	}
      }

      if (doExtendedPlots) {

	//Plot the average number of  interactions per crossing for PU.
	{
	  float avgXing = Props::averageInteractionsPerCrossing.get(m_eventInfo);
	  if(!m_isMC && m_config->get<bool>("applyPUWeight")) {
	    avgXing /= dataOneOverSF;
	  }
	  BookFillHist_VHbb1lep("AverMu", 120, 0, 60, avgXing, m_weight);
	}


	//B-Tagging Variables for AntiKt4EMtopo dR = 0.4
	if (nSelectedJet >= 1){
	  BookFillHist_VHbb1lep("MV2c10B1", 100, -1, 1, Props::MV2c10.get(selectedJets.at(0)), m_weight);
	}
	if (nSelectedJet >= 2){
	  BookFillHist_VHbb1lep("MV2c10B2", 100, -1, 1, Props::MV2c10.get(selectedJets.at(1)), m_weight);
	}

	if(isMerged){
	  //1D histogram of MV2c00, MV2c10 & MV2c100 for each trackjet
	  if(associatedTrackJets.size() >= 1) {
	    BookFillHist_VHbb1lep("TrkMV2c10B1", 100, -1, 1,  Props::MV2c10.get(associatedTrackJets.at(0)), m_weight);
	  }
	  if(associatedTrackJets.size() >= 2){
	    BookFillHist_VHbb1lep("TrkMV2c10B2", 100, -1, 1,  Props::MV2c10.get(associatedTrackJets.at(1)), m_weight);
	  }
	}

	// W system
	//fill_TLV("lepVec",      lepVec      , m_weight);

	// dijet system (Higgs)
	if (isResolved) {
	  // jets
	  fill_TLV("j1VecCorr",   j1VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("j2VecCorr",   j2VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("HVecJetCorr",  HVecJetCorr , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  BookFillHist_VHbb1lep("dRBB",      100, 0., 6.,   j1VecCorr.DeltaR(j2VecCorr), m_weight);
	  BookFillHist_VHbb1lep("dPhiVBB",   100, 0., 3.15, fabs(WVecT.DeltaPhi(HVecJetCorr)), m_weight);

	  BookFillHist_VHbb1lep("dPhiBB",    200, -3.15, 3.15, j1VecCorr.DeltaPhi(j2VecCorr), m_weight);
	  BookFillHist_VHbb1lep("dEtaBB",    100,    0.,   6.,   fabs(j1VecCorr.Eta() - j2VecCorr.Eta()), m_weight);
	  BookFillHist_VHbb1lep("dPhiLBmin", 100,    0.,   6.,   std::min(fabs(lepVec.DeltaPhi(j1VecCorr)), fabs(lepVec.DeltaPhi(j2VecCorr))), m_weight);

	  if (nSelectedJet >= 1) BookFillHist_VHbb1lep("MV2c10B1", 100, -1, 1, Props::MV2c10.get(selectedJets.at(0)), m_weight);
	  if (nSelectedJet >= 2) BookFillHist_VHbb1lep("MV2c10B2", 100, -1, 1, Props::MV2c10.get(selectedJets.at(1)), m_weight);

	  //2D Histos
	  m_histSvc->BookFillHist("dPhiBB_dEtaBB",100, 0., 3.15, 100,0.,6., fabs(j1VecCorr.DeltaPhi(j2VecCorr)), fabs(j1VecCorr.Eta() - j2VecCorr.Eta()), m_weight);
	  m_histSvc->BookFillHist("dPhiBB_dRBB",100, 0., 3.15, 100,0.,6., fabs(j1VecCorr.DeltaPhi(j2VecCorr)), j1VecCorr.DeltaR(j2VecCorr), m_weight);
	  m_histSvc->BookFillHist("dEtaBB_dRBB",100, 0., 6., 100,0.,6.,fabs(j1VecCorr.Eta() - j2VecCorr.Eta()), j1VecCorr.DeltaR(j2VecCorr), m_weight);

	  //pTB1 and pTB2, redundant, this information is already stored by the lines above under a different name
	  //BookFillHist_VHbb1lep("pTB1", 100,  0,  500,  j1VecCorr.Pt()/1e3, m_weight);
	  //BookFillHist_VHbb1lep("pTB2", 100,  0,  500,  j2VecCorr.Pt()/1e3, m_weight);

	  //3rd jet variables
	  if(nSelectedJet > 2){
	    BookFillHist_VHbb1lep("pTJ3", 100,  0,  500,  j3VecCorr.Pt()/1e3, m_weight);
	    BookFillHist_VHbb1lep("mBBJ", 100,  0,  500,  bbjVecCorr.M()/1e3, m_weight);
	  }
	}
	// fat jet (Higgs)
	if (isMerged) {
	  fill_TLV("HVecFatCorr",  fj1VecCorr  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  BookFillHist_VHbb1lep("dPhiVFatCorr", 100, 0., 3.15,   fabs(WVecT.DeltaPhi(fj1VecCorr)), m_weight);
	  BookFillHist_VHbb1lep("dPhiLFatCorr", 100, 0., 3.15, fabs(lepVec.DeltaPhi(fj1VecCorr)), m_weight);
	}

	// W system
	fill_TLV("nuVec",       nuVec       , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	fill_TLV("WVec",        WVec        , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

      }

      if (doTLVPlots) {

	if(isResolved){
	  fill_TLV("fwdjet1Vec", fwdjet1Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("fwdjet2Vec", fwdjet2Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nForwardJet > 2) fill_TLV("fwdjet3Vec", fwdjet3Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("sigjet1Vec", sigjet1Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("sigjet2Vec", sigjet2Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nSignalJet > 2) fill_TLV("sigjet3Vec", sigjet3Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("j1VecSel",    j1VecSel    , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("j2VecSel",    j2VecSel    , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nSelectedJet > 2){
	    fill_TLV("j3VecSel",    j3VecSel    , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	    fill_TLV("bbjVec",      bbjVec      , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	    fill_TLV("j3VecCorr",   j3VecCorr   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	    fill_TLV("bbjVecCorr",  bbjVecCorr  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  }

	  fill_TLV("j1VecRW",     j1VecRW     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("j2VecRW",     j2VecRW     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("HVecJet",      HVecJet     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("HVecJetRW",    HVecJetRW   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("VHVecJet",     VHVecJet     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("VHVecJetCorr", VHVecJetCorr , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("VHVecJetRW",   VHVecJetRW   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	}

	if(isMerged){
	  fill_TLV("fatjet1Vec", fatjet1Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nFatJet > 1) fill_TLV("fatjet2Vec", fatjet2Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nFatJet > 2) fill_TLV("fatjet3Vec", fatjet3Vec, m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  if(nFatJet > 1) fill_TLV("fj2VecCorr",  fj2VecCorr  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  if(nFatJet > 2) fill_TLV("fj3VecCorr",  fj3VecCorr  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("fatJetRW",    fatJetRW    , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	  fill_TLV("VHVecFat",     VHVecFat     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("VHVecFatCorr", VHVecFatCorr , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	  fill_TLV("VHVecFatRW",   VHVecFatRW   , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	}

	fill_TLV("metVec",      metVec      , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	if(isResolved) fill_TLV("metVecJetRW", metVecJetRW , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	if(isMerged) fill_TLV("metVecFJRW",  metVecFJRW  , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

	fill_TLV("lepVecT",     lepVecT     , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);
	fill_TLV("WVecT",       WVecT       , m_weight, m_leptonFlavour == lepFlav::El, m_leptonFlavour == lepFlav::Mu);

      }
    }
  }

  if(m_debug) std::cout << " >>>>> Formed Histogram Inputs" << std::endl;

  */

  return EL::StatusCode::SUCCESS;
} // run_1Lep_analysis

EL::StatusCode AnalysisReader_VHcc1Lep::fill_1lepResolvedCutFlow (unsigned long int eventFlag) {

  //* Joe: This won't work for now because we've commented out all of the cuts...
  /*

  // systematics variations are skipped via histNameSvc
  std::string dir = "CutFlow/Nominal/";

  static std::string cuts[24] = { "AllCxAOD", "Trigger", "LooseLeptons", "SignalLeptons", "AtLeast2Jets",
                                  "AtLeast2SigJets", "Pt45", "MET", "mTW", "Veto3bjet", "mbbRestrict", "mbbCorr", "pTV", "dRBB", "mTW_add",
                                  "SR_0tag_2jet","SR_0tag_3jet", "SR_0tag_4pjet", "SR_1tag_2jet",
                                  "SR_1tag_3jet", "SR_1tag_4pjet", "SR_2tag_2jet", "SR_2tag_3jet",
                                  "SR_2tag_4pjet" };

  // Cuts to exclude from cutflow
  std::vector<unsigned long int> excludeCuts = { OneLeptonResolvedCuts_VHcc::dRBB, OneLeptonResolvedCuts_VHcc::mTW_add } ; //OneLeptonResolvedCuts_VHcc::tauVeto};

  // Loop over cuts
  for (unsigned long int i = OneLeptonResolvedCuts_VHcc::AllCxAOD; i <= OneLeptonResolvedCuts_VHcc::SR_2tag_4pjet; ++i) {
    // Skip excluded cuts
    if ( std::find(excludeCuts.begin(), excludeCuts.end(), i) != excludeCuts.end() ) continue;

    // all cuts up to Pt45 should be applied sequentially
    if ( i <= OneLeptonResolvedCuts_VHcc::pTV ) {
      if ( !passAllCutsUpTo(eventFlag, i, { } ) ) continue;
    } else {
      if ( !(passAllCutsUpTo(eventFlag, OneLeptonResolvedCuts_VHcc::pTV, {}) && passSpecificCuts(eventFlag, {i})) ) continue;
    }
    std::string label = cuts[i];
    m_histSvc->BookFillCutHist(dir + "CutsResolved", length(cuts), cuts, label, m_weight);
    m_histSvc->BookFillCutHist(dir + "CutsResolvedNoWeight", length(cuts), cuts, label, 1);
    //if(m_leptonFlavour == lepFlav::El){
    //  m_histSvc->BookFillCutHist(dir + "CutsResolved_el", length(cuts), cuts, label, m_weight);
    //  m_histSvc->BookFillCutHist(dir + "CutsResolvedNoWeight_el", length(cuts), cuts, label, 1);
    //}
    //if(m_leptonFlavour == lepFlav::Mu){
    //  m_histSvc->BookFillCutHist(dir + "CutsResolved_mu", length(cuts), cuts, label, m_weight);
    //  m_histSvc->BookFillCutHist(dir + "CutsResolvedNoWeight_mu", length(cuts), cuts, label, 1);
    //}
  }

  return EL::StatusCode::SUCCESS;
} // fill_1lepResolvedCutFlow

EL::StatusCode AnalysisReader_VHcc1Lep::fill_1lepMergedCutFlow (unsigned long int eventFlag) {
  // systematics variations are skipped via histNameSvc
  std::string dir = "CutFlow/Nominal/";

  static std::string cuts [12] = { "AllCxAOD", "Trigger", "Leptons", "MET", "AtLeast1FatJet",
                                   "mbbCorr", "AtLeast2TrackJets", "pTV",
                                   "SR_0tag_1pfat0pjets", "SR_1tag_1pfat0pjets",
                                   "SR_2tag_1pfat0pjets", "SR_3ptag_1pfat0pjets", };

  // Cuts to exclude from cutflow
  std::vector<unsigned long int> excludeCuts = { } ;

  // Loop over cuts
  for (unsigned long int i = OneLeptonMergedCuts::AllCxAOD; i <= OneLeptonMergedCuts::SR_3ptag_1pfat0pjets; ++i) {
    // Skip excluded cuts
    if ( std::find(excludeCuts.begin(), excludeCuts.end(), i) != excludeCuts.end() ) continue;

    if ( i <= OneLeptonMergedCuts::pTV ) {
      if ( !passAllCutsUpTo(eventFlag, i, { } ) ) continue;
    } else {
      if ( !(passAllCutsUpTo(eventFlag, OneLeptonMergedCuts::pTV, {}) && passSpecificCuts(eventFlag, {i})) ) continue;
    }
    std::string label = cuts[i];
    m_histSvc->BookFillCutHist(dir + "CutsMerged", length(cuts), cuts, label, m_weight);
  }

  */

  return EL::StatusCode::SUCCESS;
} // fill_1lepMergedCutFlow

bool AnalysisReader_VHcc1Lep::pass1LepTrigger(double& triggerSF_nominal, ResultVHbb1lep selectionResult) {
  const xAOD::Electron  *el = selectionResult.el;
  const xAOD::Muon      *mu = selectionResult.mu;
  const xAOD::MissingET *met = selectionResult.met;

  CP::SystematicSet sysSet;
  m_triggerTool -> applySystematicVariation(sysSet);
  m_triggerTool -> setEventInfo(m_eventInfo, m_randomRunNumber);
  m_triggerTool -> setElectrons({el});
  m_triggerTool -> setMuons({mu});
  m_triggerTool -> setMET(met);

  bool triggerDec = ((TriggerTool_VHbb1lep*) m_triggerTool) -> getDecisionAndSFwithMET(triggerSF_nominal);

  if (!triggerDec) return false;

  if (m_isMC) m_weight *= triggerSF_nominal;

  // handle systematics
  if (m_isMC && (m_currentVar == "Nominal")) {
    for (size_t i = 0; i < m_triggerSystList.size(); i++) {
      // not computing useless systematics
      if (el && (m_triggerSystList.at(i).find("MUON_EFF_Trig") != std::string::npos)){
	m_weightSysts.push_back({ m_triggerSystList.at(i), 1.0 });
        continue;
      }
      if (mu && (m_triggerSystList.at(i).find("EL_EFF_Trig") != std::string::npos)){
	m_weightSysts.push_back({ m_triggerSystList.at(i), 1.0 });
        continue;
      }

      // get decision + weight
      double triggerSF = 1.;
      CP::SystematicSet sysSet(m_triggerSystList.at(i));
      m_triggerTool -> applySystematicVariation(sysSet);
      ((TriggerTool_VHbb1lep*) m_triggerTool) -> getDecisionAndSFwithMET(triggerSF);

      if (triggerSF_nominal > 0) m_weightSysts.push_back({ m_triggerSystList.at(i), (float)(triggerSF / triggerSF_nominal) });
      else Error("pass1LepTrigger()", "Nominal trigger SF=0!, The systematics will not be generated.");
    }
  }
  return true;
} // pass1LepTrigger

float  AnalysisReader_VHcc1Lep::calculateMtop(
        const TLorentzVector &lepton,
        const TLorentzVector &MET,
        const TLorentzVector &b_jet1,
        const TLorentzVector &b_jet2) {

  //  bool NeuPz_im;
  float min_mtop,NeuPz_1,NeuPz_2;
  int METShift=1;
  TLorentzVector neu_tlv_1,neu_tlv_2;
  float mw = 80385;
  double tmp = mw*mw + 2.*lepton.Px()*MET.Px() + 2.*lepton.Py()*MET.Py();
  double METcorr = MET.Pt();
  if(tmp*tmp-pow(2.*lepton.Pt()*MET.Pt(),2)<0){
    //    NeuPz_im = true;
    if(METShift>0){
      METcorr = 0.5*mw*mw/(lepton.Pt()-lepton.Px()*cos(MET.Phi())-lepton.Py()*sin(MET.Phi()));
      double newtmp =mw*mw + 2.*lepton.Px()*METcorr*cos(MET.Phi()) + 2.*lepton.Py()*METcorr*sin(MET.Phi());
      NeuPz_1 = lepton.Pz()*newtmp/2./lepton.Pt()/lepton.Pt();
      NeuPz_2 = NeuPz_1;
    }
    else {
      NeuPz_1 = (lepton.Pz()*tmp)/2./lepton.Pt()/lepton.Pt();
      NeuPz_2 = NeuPz_1;
    }
  }
  else {
    //    NeuPz_im = false;
    NeuPz_1 = (lepton.Pz()*tmp + lepton.E()*sqrt(tmp*tmp - pow(2.*lepton.Pt()*MET.Pt(),2)))/2./lepton.Pt()/lepton.Pt();
    NeuPz_2 = (lepton.Pz()*tmp - lepton.E()*sqrt(tmp*tmp - pow(2.*lepton.Pt()*MET.Pt(),2)))/2./lepton.Pt()/lepton.Pt();
  }

  neu_tlv_1.SetPxPyPzE(METcorr*cos(MET.Phi()),METcorr*sin(MET.Phi()),NeuPz_1,sqrt(pow(METcorr,2) + pow(NeuPz_1,2)));
  neu_tlv_2.SetPxPyPzE(METcorr*cos(MET.Phi()),METcorr*sin(MET.Phi()),NeuPz_2,sqrt(pow(METcorr,2) + pow(NeuPz_2,2)));

  float mtop11 = (b_jet1 + lepton + neu_tlv_1).M();
  float mtop12 = (b_jet1 + lepton + neu_tlv_2).M();
  float mtop21 = (b_jet2 + lepton + neu_tlv_1).M();
  float mtop22 = (b_jet2 + lepton + neu_tlv_2).M();
  min_mtop = mtop11;
  if(min_mtop > mtop12)
    min_mtop = mtop12;
  if(min_mtop > mtop21)
    min_mtop = mtop21;
  if(min_mtop > mtop22)
    min_mtop = mtop22;

  return min_mtop;

}

float  AnalysisReader_VHcc1Lep:: calculatedYWH(
        const TLorentzVector &lepton,
        const TLorentzVector &MET,
        const TLorentzVector &b_jet1,
        const TLorentzVector &b_jet2) {

  // TODO the following looks mostly equivalent to AnalysisReader::getNeutrinoPz

  float NeuPz_1,NeuPz_2;
  float mw = 80385;
  TLorentzVector neu_tlv, Wz1, Wz2, Hz;
  double tmp = mw*mw + 2.*lepton.Px()*MET.Px() + 2.*lepton.Py()*MET.Py();

  if(tmp*tmp-pow(2.*lepton.Pt()*MET.Pt(),2)<0){
    NeuPz_1 = (lepton.Pz()*tmp)/2./lepton.Pt()/lepton.Pt();
    NeuPz_2 = NeuPz_1;
  }
  else {
    NeuPz_1 = (lepton.Pz()*tmp + lepton.E()*sqrt(tmp*tmp - pow(2.*lepton.Pt()*MET.Pt(),2)))/2./lepton.Pt()/lepton.Pt();
    NeuPz_2 = (lepton.Pz()*tmp - lepton.E()*sqrt(tmp*tmp - pow(2.*lepton.Pt()*MET.Pt(),2)))/2./lepton.Pt()/lepton.Pt();
  }

  Wz1.SetPxPyPzE(0, 0, lepton.Pz() + NeuPz_1, std::sqrt(mw*mw + std::pow(lepton.Pz() + NeuPz_1, 2)));
  Wz2.SetPxPyPzE(0, 0, lepton.Pz() + NeuPz_2, std::sqrt(mw*mw + std::pow(lepton.Pz() + NeuPz_2, 2)));
  Hz.SetPxPyPzE(0, 0, b_jet1.Pz() + b_jet2.Pz(), std::sqrt( (b_jet1 + b_jet2).M2() + std::pow(b_jet1.Pz() + b_jet2.Pz(), 2)));

  float dBeta1 = std::fabs(Wz1.Beta() - Hz.Beta());
  float dBeta2 = std::fabs(Wz1.Beta() - Hz.Beta());

  if(dBeta1 < dBeta2)
    neu_tlv.SetPxPyPzE(MET.Px(),MET.Py(),NeuPz_1,sqrt(pow(MET.Px(),2) + pow(MET.Py(),2) + pow(NeuPz_1,2)));
  else
    neu_tlv.SetPxPyPzE(MET.Px(),MET.Py(),NeuPz_2,sqrt(pow(MET.Px(),2) + pow(MET.Py(),2) + pow(NeuPz_2,2)));


  float dYWH = std::fabs( (lepton + neu_tlv).Rapidity() - (b_jet1 + b_jet2).Rapidity() );

  return dYWH;
}
EL::StatusCode AnalysisReader_VHcc1Lep::applyFFweight(
        const TLorentzVector& WVecT,
        const TLorentzVector& metVec,
        const xAOD::Electron* el) {
  if(!el) {
    return EL::StatusCode::FAILURE;
  }
  double ff_weight = 1.0;
  double iso = Props::topoetcone20.get(el);
  double ptcone = Props::ptvarcone20.get(el);
  double d0sigbl = Props::d0sigBL.get(el);
  bool mediumid = Props::isMediumLH.get(el);
  bool tightid = Props::isTightLH.get(el);
  m_FakeFactor_el->set_parameters(WVecT.Pt() / 1000., el->pt() / 1000., fabs(el->eta()),
          fabs(el->p4().DeltaPhi(metVec)), metVec.Pt() / 1000., iso / 1000., d0sigbl, mediumid, tightid);
  if(m_currentVar=="MJ_Mu_METstr")ff_weight = m_FakeFactor_el->get_weight("Nominal");
  else ff_weight = m_FakeFactor_el->get_weight(m_currentVar);
  if(!((ptcone/el->pt())<0.06)) m_weight = 0.0;
  m_weight *= ff_weight;
  if(m_isMC)m_weight *= -1;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VHcc1Lep::applyFFweight(
        const TLorentzVector& WVecT,
        const TLorentzVector& metVec,
        const xAOD::Muon* mu) {
  if (!mu) {
    return EL::StatusCode::FAILURE;
  }

  double ff_weight = 1.0;
  double iso = Props::ptvarcone30.get(mu);
  double d0sigbl = Props::d0sigBL.get(mu);
  bool mediumid = Props::isMedium.get(mu);
  bool tightid = Props::isTight.get(mu);
  m_FakeFactor_mu->set_parameters(WVecT.Pt() / 1000., mu->pt() / 1000., fabs(mu->eta()),
          fabs(mu->p4().DeltaPhi(metVec)), metVec.Pt() / 1000., iso / 1000., d0sigbl, mediumid, tightid);
  if(m_currentVar=="MJ_El_EWK")ff_weight = m_FakeFactor_mu->get_weight("MJ_El_EWK");
  else ff_weight = m_FakeFactor_mu->get_weight("Nominal");
  m_weight *= ff_weight;
  if(m_currentVar=="MJ_Mu_METstr")m_weight *= get_sys_metstrmu(WVecT.M()/1000.);
  if(WVecT.Pt()/1000. < 150 && iso/mu->pt() > 0.07) ff_weight=0;//for lowpTV, denominator is 0.06 <ptcone/pt < 0.07
  if(m_isMC)m_weight *= -1;

  return EL::StatusCode::SUCCESS;
}

float  AnalysisReader_VHcc1Lep:: get_sys_metstrmu(float mTW){

  return 0.513+0.00318*mTW;

}

EL::StatusCode AnalysisReader_VHcc1Lep::applyCS_SMVHMJEl(const xAOD::Electron* el, bool pass, bool isTop, bool isW, int nJet){
   if (!el) {
      return EL::StatusCode::FAILURE;
   }
   float WeightReduced = 1;
   float WeightTrigger = 1;
   float WeightSFsCR   = 1;
   for (auto s_csVariations : m_csVariations){
    if (s_csVariations == "MJReduced"){
     if(Props::topoetcone20.get(el) > 11e3)
      {
       WeightReduced = .0;
      }
     }
     else if (s_csVariations == "MJTrigger"){
      if(!pass) {
      WeightTrigger = .0;
      }
     }
     else if (s_csVariations == "MJSFsCR"){
      if(isTop) {
       if(nJet==2)       WeightSFsCR =  0.937;
       else if (nJet==3) WeightSFsCR =  0.928;
      }
      else if (isW) {
       if(nJet==2)       WeightSFsCR =  1.265;
       else if (nJet==3) WeightSFsCR =  1.316;
      }
     }
    bool nominalOnly = false;
    m_config->getif<bool>("nominalOnly",nominalOnly);
    if ((m_csVariations.size() != 0) && !nominalOnly){
     if (s_csVariations == "MJReduced"){
      m_weightSysts.push_back({(std::string)s_csVariations , WeightReduced});
     }
     else if (s_csVariations == "MJTrigger"){
      m_weightSysts.push_back({(std::string)s_csVariations , WeightTrigger});
     }
     else if (s_csVariations == "MJ2Tag"){
      m_weightSysts.push_back({(std::string)s_csVariations , 1});
     }
     else if (s_csVariations == "MJWOCorrection"){
      m_weightSysts.push_back({(std::string)s_csVariations , 1});
     }
     else if (s_csVariations == "MJSFsCR"){
      m_weightSysts.push_back({(std::string)s_csVariations , WeightSFsCR});
     }
     else{
      std::cout << "Unknown csVariation \""<< s_csVariations<< "\"" << std::endl;
     }
    }
   }
   return EL::StatusCode::SUCCESS;
}
EL::StatusCode AnalysisReader_VHcc1Lep::applyCS_SMVHMJMu(const xAOD::Muon* mu, bool isTop, bool isW, int nJet){
   if (!mu) {
      return EL::StatusCode::FAILURE;
   }
   float WeightReduced = 1;
   float WeightSFsCR   = 1;
   for (auto s_csVariations : m_csVariations){
    if (s_csVariations == "MJTrigger"){ //Only for el channel
     continue;
    }
    else if (s_csVariations == "MJReduced"){
     if(Props::ptcone20.get(mu) > 2.25e3){
       WeightReduced = .0;
     }
    }
    else if (s_csVariations == "MJSFsCR"){
      if(isTop) {
       if(nJet==2)       WeightSFsCR =  0.937;
       else if (nJet==3) WeightSFsCR =  0.928;
      }
      else if (isW) {
       if(nJet==2)       WeightSFsCR =  1.265;
       else if (nJet==3) WeightSFsCR =  1.316;
      }
    }
    bool nominalOnly = false;
    m_config->getif<bool>("nominalOnly",nominalOnly);
    if ((m_csVariations.size() != 0) && !nominalOnly){
     if (s_csVariations == "MJReduced"){
      m_weightSysts.push_back({(std::string)s_csVariations , WeightReduced});
     }
     else if (s_csVariations == "MJ2Tag"){
      m_weightSysts.push_back({(std::string)s_csVariations , 1});
     }
     else if (s_csVariations == "MJWOCorrection"){
      m_weightSysts.push_back({(std::string)s_csVariations , 1});
     }
     else if (s_csVariations == "MJSFsCR"){
      m_weightSysts.push_back({(std::string)s_csVariations , WeightSFsCR});
     }
     else{
      std::cout << "Unknown csVariation \""<< s_csVariations<< "\"" << std::endl;
     }
    }
   }
   return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VHcc1Lep::applyCS_MJ(const TLorentzVector &lepVec){
  // Apply a scale factor to account for trigger inefficiencies in the IsoInv region
  // Inefficiency only affects electron trigger

  // For the MJ_TrigEff uncertainty we have to determine in this function if
  // it's an electron or muon event.
  // Simplest thing to do is to check m_leptonFlavour, which is however only set
  // when running with doSplitLepFlavour option.
  // Currently, this is always the case! If this is changed later -> Error
  if (m_leptonFlavour == lepFlav::Combined) {
    Error("applyCS_MJ()",
          "applyCS_MJ not compatible with m_leptonFlavour == "
          "lepFlav::Combined (usually this means that doSplitLepFlavour is not "
          "set to true in the config).");
    return EL::StatusCode::FAILURE;
  }

  TriggerInfo::DataPeriod period = TriggerInfo::getDataPeriodFromRun(m_eventInfo->runNumber());
  double ScaleFactor = 1;
  double slope = 0;
  double offset = 1;
  for (auto s_csCorrections : m_csCorrections) {
    if (s_csCorrections == "MJ_TrigEff") {
      if (m_leptonFlavour != lepFlav::El) continue;
      if (period <=  TriggerInfo::DataPeriod::data15){
        slope = 0.000542;
        offset = 1.576;
      }
      else if (period >=  TriggerInfo::DataPeriod::data16A){
        slope = 0.0141;
        offset = 1.003;
      }
      ScaleFactor = slope * lepVec.Pt()/ 1e3 + offset;
      m_weight *= ScaleFactor;
    }
  }
  // Do MJ (IsoInv) related Systematics
  bool nominalOnly = false;
  m_config->getif<bool>("nominalOnly",nominalOnly);

  if ((m_csVariations.size() != 0) && !nominalOnly){
    for (auto s_csVariations : m_csVariations){
      if (s_csVariations == "MJ_TrigEff"){
        if (m_leptonFlavour != lepFlav::El) continue;
        double slope_corr = 0;
        double offset_corr = 1;
        if (period ==  TriggerInfo::DataPeriod::data15){
          slope_corr = 0.002331;
          offset_corr = 0.580;
        }
        else if (period ==  TriggerInfo::DataPeriod::data16A){
          slope_corr = 0.0032;
          offset_corr = 0.670;
        }
        double weightUp                    = 1;
        double weightDo                    = 1;

        weightUp = ((slope + slope_corr) * lepVec.Pt() / 1e3 + (offset - offset_corr) ) / ScaleFactor;
        weightDo = ((slope - slope_corr) * lepVec.Pt() / 1e3 + (offset + offset_corr) ) / ScaleFactor;

        if (fabs(weightUp - 1) > 1.e-6 || fabs(weightDo - 1) > 1.e-6) {
          m_weightSysts.push_back({(std::string)s_csVariations + "__1up",
                static_cast<float>(weightUp)});
          m_weightSysts.push_back({(std::string)s_csVariations + "__1down",
                static_cast<float>(weightDo)});
        }
      }
    }
  }
  return EL::StatusCode::SUCCESS;
}



//Interface member function designed to simply produce multiple histograms for lepton flavour
void AnalysisReader_VHcc1Lep::BookFillHist_VHcc1lep(const string& name, int nbinsx, float xlow, float xup, float value, float weight, SystLevel level, bool PrintSplitFlav){

  //Determine if we want to write systematics for the variable or not
  //HistSvc::doSysts::no = 0, HistSvc::doSysts::yes = 1
  HistSvc::doSysts ds_Split = HistSvc::doSysts::yes, ds_Combined = HistSvc::doSysts::yes;
  if( level == SystLevel::None ){
    ds_Split = HistSvc::doSysts::no;
    ds_Combined = HistSvc::doSysts::no;
  }
  else if( level == SystLevel::CombinedOnly ){
    ds_Split = HistSvc::doSysts::no;
    ds_Combined = HistSvc::doSysts::yes;
  }

  //Check the member variable for the "Nominal" string if we do not want systematics. I.e. do not print the systematic
  //versions of the histograms
  if( level == SystLevel::None && m_currentVar != "Nominal"){
    return;
  }

  //Fill the user defined histogram
  m_histSvc->BookFillHist(name,     nbinsx,  xlow, xup, value, weight, ds_Combined);
  //If set then fill the lepton flavour specific
  if( m_leptonFlavour == lepFlav::El && PrintSplitFlav) {
    m_histSvc->BookFillHist( "El_" + name,     nbinsx,  xlow, xup, value, weight, ds_Split);
  }else if( m_leptonFlavour == lepFlav::Mu && PrintSplitFlav){
    m_histSvc->BookFillHist( "Mu_" + name,     nbinsx,  xlow, xup, value, weight, ds_Split);
  }

}


//Plotting function to produced cut based histograms at same time as running inputs for MVA
void AnalysisReader_VHcc1Lep::BookFillHist_SMCut(const string& name, int nbinsx, float xlow, float xup, float value, float weight, SystLevel level, bool PrintSplitFlav){

  m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::CUT);  // Have to switch to cut based analysis to get right pTV splitting
  BookFillHist_VHcc1lep( name+"CutBased", nbinsx,  xlow, xup, value, weight, level, PrintSplitFlav);
  m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::MVA); // And now switch back to MVA (assuming that this function is only being called by MVA analysis

}
