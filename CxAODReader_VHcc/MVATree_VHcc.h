#ifndef CxAODReader_MVATree_VHcc_H
#define CxAODReader_MVATree_VHcc_H

#include "CxAODReader/MVATree.h"
#include "CxAODReader/StaticMVATree.h"

namespace CxAODReader_MVATree_VHcc_internal {
  template<class Tree> using STree = StaticMVATree<Tree,
                                                   bool, int, unsigned,
                                                   long, unsigned long,
                                                   float, double,
                                                   std::string>;
}

class MVATree_VHcc : public MVATree, public CxAODReader_MVATree_VHcc_internal::STree<MVATree_VHcc> {

friend CxAODReader_MVATree_VHcc_internal::STree<MVATree_VHcc>;

protected:

  std::string m_analysisType;

  virtual void AddBranch(TString name, Float_t* address);
  virtual void AddBranch(TString name, Int_t* address);
  virtual void AddBranch(TString name, ULong64_t * address);
  //virtual void AddBranch(TString name, std::string* address);
  template <class T>
  void AddBranch(TString name, T* address)
  {
    for (std::pair<std::string, TTree*> tree : m_treeMap) {
      tree.second -> Branch(name, address);
    }
  }
  virtual void SetBranches() override;
  virtual void TransformVars() override;
  float getBinnedMV1c(float MV1c);

public:

  MVATree_VHcc(bool persistent, bool readMVA, std::string analysisType, EL::Worker* wk, std::vector<std::string> variations, bool nominalOnly);
//  MVATree_VHcc(bool persistent, bool readMVA, std::string analysisType, EL::Worker* wk);//, std::vector<std::string> variations, bool nominalOnly);

  ~MVATree_VHcc() {}

  virtual void Reset() override;
  virtual void ReadMVA();
  void Fill();

  std::string sample;

  float EventWeight;
  float weight; // compatibility with Paul's MVA code
  unsigned long long EventNumber;
  int isOdd; // compatibility with Paul's MVA code
  int ChannelNumber;

  float mBB;
  float dRBB;
  float dEtaBB;
  float dEtaVBB;
  float dPhiVBB;
  float pTV;
  float pTB1;
  float pTB2;
  float pTBB;
  float yBB;
  float pTBBoverMET;
  float pTBBMETAsym;
  float pTJ3;
  float etaJ3; 
  float MV1cJ3; 
  float dRB1J3; 
  float dRB2J3; 
  float mindRBJ3;
  float maxdRBJ3;
  float mBBJ;
  float MV1cB1;
  float MV1cB2;
  float MET;
  float METSig;
  float METSig_soft;
  float METSig_hard;
  float METOverSqrtSumET;
  float METOverSqrtHT;
  float METDirectional;
  float METRho;
  float METVarL;
  float METVarL_soft;
  float METVarL_hard;
  float METVarT;
  float HT;
  int nSigJet; 
  int nForwardJet; 
  float mLL;
  int nJ;
  int nTags;
  int nTaus;
  float sumPt;
  float dPhiBB;
  float dPhiMETMPT;
  float dPhiMETdijet;
  float mindPhi;
  float dPhiLBmin;
  float MEff;
  float MEff3;
  float etaB1;
  float etaB2;
  float mTW;
  float MPT;
  float METHT;
  float pTL;
  float etaL;
  float Tau21;
  float Tau32;
  float mVH;
  float mVH1;
  float mVH2;
  float pTVHnorm;
  float Mtop;
  float dYWH;
  float dEtaWH;
  float dPhiLMET;
	
  float phiB1;
  float phiB2;
  float phiJ3;
  float mB1;
  float mB2;
  float mJ3;

  float costheta;

  float BDT;
  float BDT_WZ;
  float BDTCharles;
  
  std::map<std::string, float> BDTs;
};


#endif
