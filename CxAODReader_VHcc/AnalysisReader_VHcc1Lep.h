#ifndef CxAODReader_AnalysisReader_VHcc1Lep_H
#define CxAODReader_AnalysisReader_VHcc1Lep_H

#include "CxAODTools_VHbb/VHbb1lepEvtSelection.h"
#include "CxAODReader_VHcc/AnalysisReader_VHcc.h"

class AnalysisReader_VHcc1Lep : public AnalysisReader_VHcc {

protected:
  ////Used for MODEL systematics on V+Jets and TTbar
  //CAS::EventType m_evtType; //!
  //CAS::DetailEventType m_detailevtType; //!
  //CAS::Systematic m_Systematic; //!

  // --------- Level of systematics in histogram output ----------
  enum class SystLevel{
    None=0,
    CombinedOnly,
    All
  };
  //---------------------------------------------------------------

  //---------- Lepton Flavour info about event --------------
  //Enum for info encoding
  //Records the flavour of the lepton
  enum class lepFlav{
    Combined = 0,
      El,
      Mu
  };
  
  //Protected member that stored result
  lepFlav m_leptonFlavour;
  //---------------------------------------------------------

  //New Booking hisotgram for the 1-lepton analysis only. Hence protected. 
  void BookFillHist_VHcc1lep(const string& name, int nbinsx, float xlow, float xup, float value, float weight, SystLevel = SystLevel::All, bool PrintSplitFlav = true);
  void BookFillHist_SMCut(const string& name, int nbinsx, float xlow, float xup, float value, float weight, SystLevel = SystLevel::All, bool PrintSplitFlav = true);

public:

  AnalysisReader_VHcc1Lep ();
  ~AnalysisReader_VHcc1Lep ();

  virtual EL::StatusCode initializeSelection () override;
  virtual EL::StatusCode initializeTools () override;
  EL::StatusCode run_1Lep_analysis ();
  EL::StatusCode fill_1Lep ();
  //
  std::vector<const xAOD::Jet*> ApplyMuonFatJetOR(std::vector<const xAOD::Muon*> Muons, std::vector<const xAOD::Jet*> fatJets);
  //EL::StatusCode SetupCorrsAndSyst(std::string currentVar, bool isCR);
  //void fillTTbarSystslep(int nTag, bool isCR, float mVH);
  //void fillVjetsSystslep(double Mbb, float mVH);
  EL::StatusCode applyCS_MJ(const TLorentzVector &lepVec);
  EL::StatusCode applyCS_SMVHMJEl(const xAOD::Electron* el, bool pass, bool isTop, bool isW, int nJet);
  EL::StatusCode applyCS_SMVHMJMu(const xAOD::Muon* mu, bool isTop, bool isW, int nJet);
  EL::StatusCode fill_1lepResolvedCutFlow (unsigned long int eventFlag);
  EL::StatusCode fill_1lepMergedCutFlow (unsigned long int eventFlag);
  bool pass1LepTrigger(double& triggerSF_nominal, ResultVHbb1lep selectionResult);
  float calculateMtop(const TLorentzVector &lepton, const TLorentzVector &MET, const TLorentzVector &b_jet1, const TLorentzVector &b_jet2);
  float calculatedYWH(const TLorentzVector &lepton, const TLorentzVector &MET, const TLorentzVector &b_jet1, const TLorentzVector &b_jet2);
  float get_sys_metstrmu(float mTW);
  EL::StatusCode applyFFweight(const TLorentzVector& WVecT, const TLorentzVector& metVec, const xAOD::Electron* el);
  EL::StatusCode applyFFweight(const TLorentzVector& WVecT, const TLorentzVector& metVec, const xAOD::Muon* mu);
  // this is needed to distribute the algorithm to the workers
  ClassDefOverride(AnalysisReader_VHcc1Lep, 1);

};


namespace OneLeptonResolvedCuts_VHcc {

  enum Cuts {
    AllCxAOD = 0,               // C0
    Trigger,                    // C1
    LooseLeptons,               // C2
    SignalLeptons,              // C3
    AtLeast2Jets,               // C4
    AtLeast2SigJets,            // C5
    Pt45,                       // C6
    MET,                        // C7
    mTW,                        // C8
    Veto3bjet,                  // C9
    mbbRestrict,
    mbbCorr,
    pTV,
    dRBB,
    mTW_add,
    SR_0tag_2jet,               // C10
    SR_0tag_3jet, 		// C11
    SR_0tag_4pjet,		// C12
    SR_1tag_2jet, 		// C13
    SR_1tag_3jet, 		// C14
    SR_1tag_4pjet,		// C15
    SR_2tag_2jet, 		// C16
    SR_2tag_3jet, 		// C17
    SR_2tag_4pjet,		// C18

  };
}


//Replace Line // <<<--- SJ01 Revert Point
//    LooseLeptons,               // C2
//  SignalLeptons,              // c3
//    AtLeast2Jets,               // C4
//    AtLeast2SigJets,            // C5
//    Pt45,                       // C6
//    MET,                        // C7
//    mTW,                        // C8
//    BjetVeto3rd,                // C9
//    SR_0tag_2jet,              // C10
//    SR_0tag_3jet,               // C11
//    SR_0tag_4pjet,              // C12
//    SR_1tag_2jet,              // C13
//    SR_1tag_3jet,               // C14
//    SR_1tag_4pjet,              // C15
//    SR_2tag_2jet,              // C16
//    SR_2tag_3jet,               // C17
//    SR_2tag_4pjet,              // C18
//    SR_3ptag_23jet,             // C19
//    SR_3ptag_4jet,              // C20
//    SR_3ptag_5pjet,             // C21
//};
// }
//             // <<<--- SJ01 Revert Point

#endif // ifndef CxAODReader_AnalysisReader_VHbb1Lep_H
