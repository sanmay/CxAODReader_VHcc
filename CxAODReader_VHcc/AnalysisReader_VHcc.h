#ifndef CxAODReader_AnalysisReader_VHCC_H
#define CxAODReader_AnalysisReader_VHCC_H

#include "CxAODReader/AnalysisReader.h"
class EasyTree;
//class MVATree_VHcc;
//class MVATree_VBFHbb;
//class MVATree_BoostedVHcc;
class OSTree;
namespace TMVA {
class Reader;
}
namespace KF {
class KinematicFit;
}
class ITMVAApplicationTool;
#include "CxAODReader/HistFastSvc.h"
#include "CxAODReader_VHcc/VHResRegions.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools/OvCompat.h"

#include "CxAODReader_VHbb/AnalysisReader_VHbb.h"

//combMass begin

class JMSCorrection_lvqq; 
class JetUncertaintiesTool;

//combMass end

//PROPERTY( Props, int, nBTagsAll) //number of b-tagged track jets in leading fat jet
//PROPERTY( Props, int , isMatchedToLeadFatJet ) // ghost-matched between track-jet and leading fat-jet

namespace Ov {
class IAnalysisMain;
}




class AnalysisReader_VHcc : public AnalysisReader {
protected:

    virtual EL::StatusCode initializeSelection () override;
    virtual EL::StatusCode initializeCorrsAndSysts () override;
    virtual EL::StatusCode initializeVariations () override;
    virtual EL::StatusCode initializeTools () override;
    virtual EL::StatusCode finalizeTools () override;
    virtual EL::StatusCode initializeNNLORW (int DISD) override;
    EL::StatusCode SetupCorrsAndSyst(std::string currentVar, bool isCR);
    void fillTTbarSystslep(int nTag, bool isCR, float mVH);
    void fillTopSystslep_PRSR(std::string Regime, int nTag, float mBB, float mVH, bool isEMu = false);
    void fillVjetsSystslep(double Mbb, float mVH);
    void fillVjetsSystslep_PRSR(std::string Regime, double Mbb, float mVH, int nAddBTags = 0);

    // used for histfastsvc
    virtual EL::StatusCode histFinalize () override;

    EL::StatusCode fill_bTaggerHists (const xAOD::Jet *jet);
    EL::StatusCode fill_nJetHistos (const std::vector<const xAOD::Jet*> &jets,
                                    const string                  &jetType);
    EL::StatusCode fill_fatJetHistos (const std::vector<const xAOD::Jet*> &fatJets);
    EL::StatusCode fill_jetSelectedHistos ();
    EL::StatusCode compute_jetSelectedQuantities (const std::vector<const xAOD::Jet*> &selectedJets);
    EL::StatusCode fill_TLV(std::string name, const TLorentzVector &vec, double weight, bool isE = false , bool isMu = false);
    //EL::StatusCode fill_VBF0ph ();
    //EL::StatusCode fill_VBF1ph ();
    //EL::StatusCode fill_VBFIncl ();
    //EL::StatusCode fill_vbf1ph ();
    //EL::StatusCode fill_VBFInclCutFlow (std::string label);

    // ---
    float       compute_JVTSF(const std::vector<const xAOD::Jet*>& signalJets);
    void        compute_btagging ();
    void        compute_ctagging (float b_fraction, float discriminant_cut);
    void        compute_TRF_tagging (const std::vector<const xAOD::Jet*>& signalJets);
    void        compute_fatjetTags (const std::vector<const xAOD::Jet*> &trackJets, const std::vector<const xAOD::Jet*> &fatJets,
                                    std::vector<const xAOD::Jet*> *tagTrackJetsInLeadFJ, std::vector<const xAOD::Jet*> *trackJetsNotInLeadFJ);
    void        setevent_flavour (const std::vector<const xAOD::Jet*> &selectedJets);
    // set flavour using GhostMatching
    void
    setevent_flavourGhost(const std::vector<const xAOD::Jet *> &selectedJets);
    void        setevent_nJets   (const std::vector<const xAOD::Jet*> &signalJets,
                                  const std::vector<const xAOD::Jet*> &forwardJets);
    std::string determine_regime ();

    bool removeUnwantedLB (); //temporary fix of the GRL for ICHEP analyses



    void truthVariablesNNLO(float &truthPt,
                            float &topPt);  //used to get the top pt and ttbar pt, rather than the average top pt as is done in truthVariablesCS

    void truthVariablesCS(float &truthPt,
                          float &avgTopPt);

    EL::StatusCode applyTTbarNNLORW(bool isNominal);

    EL::StatusCode NNLORW(float ttbarPt, float topPt, bool isNominal);

    EL::StatusCode applyCS (float VpT,
                            float Mbb,
                            float truthPt,
                            float DeltaPhi,
                            float DeltaR,
                            float pTB1,
                            float pTB2,
                            float MET,
                            float avgTopPt,
                            int   njet,
                            int   ntag);

    EL::StatusCode applyCS (float VpT,
                            float MET,
                            float nJet,
                            float nTag,
                            const TLorentzVector& jet1,
                            const TLorentzVector& jet2);

    void tagjet_selection (
            std::vector<const xAOD::Jet*>  signalJets,
            std::vector<const xAOD::Jet*>  forwardJets,
            std::vector<const xAOD::Jet*> &selectedJets,
            int                           &tagcatExcl);


    //Multiple event weight application function
    //NOTE:: m_EvtWeightVar is defined and initialised in CxAODReader/AnalysisReader.cxx,
    //       this function simply applies variations that are defined within the config
    void applySherpaVJet_EvtWeights();
    void applySignal_EvtWeights();

    //returns if decay contains 0,1 or 2 leptons, tau treated separately
    //0: ee/mumu 1: e/mu 2: tau 3: e/mu+tau 4: tautau 5: 0 lep
    int getTtbarDecayType();

    // a function to check whether an event passes
    // all the cuts up to "cut" - possibility to exclude some cuts
    bool passAllCutsUpTo(const unsigned long int flag, const unsigned long int cut, const std::vector<unsigned long int> &excludeCuts={});

    // a function to check whether an event passes a specific set of cuts
    bool passSpecificCuts(const unsigned long int flag, const std::vector<unsigned long int> &cuts);

    // a function to create the bit mask
    unsigned long int bitmask(const unsigned long int cut, const std::vector<unsigned long int> &excludeCuts={});

    // a function to update the bit flag
    void updateFlag(unsigned long int &flag, const unsigned long int cutPosition, const unsigned long int passCut=1);


    void rescale_jets (double          mBB,
                       TLorentzVector &j1Vec,
                       TLorentzVector &j2Vec);

    void rescale_fatjet(double mBB,
                        TLorentzVector& fatjet);

    void rescale_leptons (double          mLL,
                          TLorentzVector &l1Vec,
                          TLorentzVector &l2Vec);

    bool checkTightLeptons(const Lepton &l1, const Lepton &l2);

    void computeHT(double &HTsmallR, double &HTlargeR,
                   const Lepton &l1, const Lepton &l2,
                   const std::vector<const xAOD::Jet*> &signalJets,
                   const std::vector<const xAOD::Jet*> &forwardJets,
                   const std::vector<const xAOD::Jet*> &fatJets);

    TLorentzVector getMETCorrTLV(const xAOD::MissingET *met, std::vector<const TLorentzVector *> removeObjects, std::vector<const TLorentzVector *> addObjects);

    EL::StatusCode setJetVariables(Jet& j1, Jet& j2, Jet& j3,
                                   const std::vector<const xAOD::Jet*> &selectedJets);

    EL::StatusCode setFatJetVariables(Jet& fj1, Jet& fj2, Jet& fj3,
                                      const std::vector<const xAOD::Jet*> &fatJets);

    void setHiggsCandidate(Higgs& resolvedH, Higgs& mergedH, Jet j1, Jet j2, Jet fj1);

    TLorentzVector defineMinDRLepBSyst(const Lepton &l1, const Lepton &l2, const Jet &j1, const Jet &j2);

    bool isBlindedRegion(const unsigned long int eventFlag, bool isMerged = false);

    EL::StatusCode fill_jetHistos (std::vector<const xAOD::Jet*> signalJets, std::vector<const xAOD::Jet*> forwardJets);

    double getSMSigVPTSlice ();
    std::string TranslateStage1Cat (int PF);
    // float computeBTagSFWeight (std::vector<const xAOD::Jet*> &signalJets);
    //
    std::string m_analysisType;           // !
    std::string m_analysisStrategy;       // !
    bool m_doTruthTagging;                // !
    bool m_doMbbWindow         = true;    // !
    bool m_doFillCR            = false;   // !
    bool m_doOnlyInputs        = false;   // !
    bool m_doBootstrap         = false;   // !
    bool m_doReduceFillHistos  = false;   // !
    //MVATree_VHcc   *m_tree     = nullptr; // !
    //MVATree_VBFHbb *m_tree_vbf = nullptr; // !
    EasyTree       *m_etree    = nullptr; // !
    OSTree         *m_OStree   = nullptr; // !
    PhysicsMetadata m_physicsMeta;  // !
    bool m_doBlinding          = false;   //!
    bool m_doMergeJetBins      = false;   //!
    bool m_doMergePtVBins      = false;   //!
    std::string m_jetCorrType;  //!
    std::string m_fatJetCorrType;  //!
    std::string m_tagStrategy; //!
    std::vector<std::string> m_csCorrections; //!
    std::vector<std::string> m_csVariations; //!

    //Merged Only V+Jets and TTbar systematics
    //Used for MODEL systematics on V+Jets and TTbar
    CAS::EventType m_evtType; //!
    CAS::DetailEventType m_detailevtType; //!
    CAS::Systematic m_Systematic; //!

    KF::KinematicFit *m_KF; //!

    Ov::IAnalysisMain *m_analysisContext; //!
    std::unique_ptr<ITMVAApplicationTool> m_mva; //!
    std::string m_mvaToolName; //!

    bool m_use2DbTagCut;

    // for boosted analysis
    bool m_doFillHistograms; //!
    bool m_useFastHistSvc; //!
    bool m_doMergeCR; //!
    bool m_doCutflow; //!
    bool m_doResolution; //!
    bool m_doBinnedSyst; //!
    bool m_doSherpaSyst; //!
    //MVATree_BoostedVHcc *m_boostedtree;       // !
    HistFastSvc<VHResRegions> m_histFastSvc; //!
    HistoVec m_fatJetHistos; //!

    Model m_model;

    static bool sort_pt(const xAOD::Jet* jetA, const xAOD::Jet* jetB) {
        return jetA->pt() > jetB->pt();
    }

public:

    AnalysisReader_VHcc ();

    virtual ~AnalysisReader_VHcc ();

    // this is needed to distribute the algorithm to the workers
    ClassDefOverride(AnalysisReader_VHcc, 1);

private:

    //combMass begin

    virtual EL::StatusCode combMassTmp1 (bool& isCombMassSyst, std::string& varName) override;
    virtual EL::StatusCode combMassTmp2 (bool& isCombMassSyst) override;

    double getSmearFactor(double sigma, long int seed);
    CP::CorrectionCode applySmearingJER(xAOD::Jet& jet);
    CP::CorrectionCode applySmearingJMR(xAOD::Jet& jet);

    JMSCorrection_lvqq *m_jetMassCorr; //!
    JetUncertaintiesTool* m_jesProviderHbb; //!
    std::vector<std::string> m_combMassSyst; //!
    TRandom3 m_rand; //!

    //combMass end
};


#endif // ifndef CxAODReader_AnalysisReader_VHCC_H
